module class_ION_input_output_normal
  use class_CBF_complex_basis_function_h
  use class_CN_contium_number
  use class_WKB_wkb_mathcing
!  use class_FW_function_writer
  implicit none

  type ION_Object
     type(CBF_Object) :: cbf           ! use for normal and step mode
     type(CN_Object)  :: energy_range  ! use for normal and asymmetry mode
     integer          :: skip_print_num
     logical          :: write_psiQ
     logical          :: write_wkbQ
     logical          :: write_basisQ
     logical          :: write_grad_hessQ
     logical          :: write_matrixQ
     type(CN_Object)  :: r_range
!     type(FW_Object)  :: func_write
  end type ION_Object

contains
  !===========Constructor===============
  function ION_new_from_read() result(res)
    use class_CHD_channel_and_dipole
    type(ION_Object) :: res
    character        :: in_mode
    real*8           :: mn, mx, de
    integer          :: i, pn, tmp, ll
    integer          :: basis_num, skip_num, loop_num, order, channel
    character        :: basis_type, wkb_type, form_ch
    real*8           :: r0, r1, rp, ip
    logical          :: l1, l2, l3 ! dummy
    complex*16, allocatable :: oe_array(:)
    logical, allocatable    :: optQ_array(:)
    integer, allocatable    :: pn_array(:), opt_info(:)
    type(CL2F_Object)       :: basis
    
    write(*,*) "Basis set(s: STO, g:GTO"
    read(*,*) basis_type
    write(*,*) basis_type
    write(*,*) "mathing way(w: WKB), matching point(r0), asymtptic point(r1), order"
    read(*,*) wkb_type, r0, r1, order
    write(*,*) wkb_type, r0, r1, order
    write(*,*) "T, T"
    read(*,*) l1, l2
    write(*,*) l1, l2
    write(*,*) "input basis_num, channel, form"
    read(*,*) basis_num, channel, form_ch
    write(*,*) basis_num, channel, form_ch
    write(*,*) "min, max, delta (photoelectron KE)"
    read(*,*) mn, mx, de
    write(*,*) mn, mx, de
    write(*,*) "skip num, loop num"
    read(*,*) skip_num, loop_num
    write(*,*) skip_num, loop_num
    write(*,*) "dymmy, psi, basis, dymmy, dymmy"
    read(*,*) l1, res % write_psiQ, res % write_basisQ, l2, l3
    write(*,*) l1, res % write_psiQ, res % write_basisQ, l2, l3
    
    write(*,*) "input real(zeta) imag(zeta) pn opt?"
    allocate(oe_array(basis_num))
    allocate(optQ_array(basis_num))
    allocate(pn_array(basis_num))
    allocate(opt_info(basis_num))

    do i = 1, basis_num
       read(*,*) rp, ip, pn, l1
       write(*,*) rp, ip, pn, l1
       oe_array(i) = cmplx(rp, ip)
       pn_array(i) = pn
       optQ_array(i) = l1
    end do

    res % cbf % channel_dipole = CHD_new_from_character(channel, form_ch)
    
    res % cbf % basis_set = LCCF_new(basis_num)
    do i = 1, basis_num
       basis = CL2F_new_normalize_from_char(pn_array(i), oe_array(i),  basis_type)
       call LCCF_set_CL2F_at_n(res % cbf % basis_set, basis, i)
    end do

    tmp = 1
    do i = 1, basis_num
       if(optQ_array(i)) then
          opt_info(i) = tmp
          tmp = tmp + 1
       else
          opt_info(0) = 0
       end if
    end do
    res % cbf % opt = OPT_new(opt_info, loop_num)
    ll = CHD_l_final_state(res % cbf % channel_dipole)
    write(*,*) "ll:", ll
    res % cbf % wkb = WKB_new(r0, r1, order, ll)
         
    
    res % energy_range = CN_new(mn, mx, de)
    res % skip_print_num = skip_num

    write(*,*) "----------------------"
    call CBF_display(res % cbf)
    write(*,*) "----------------------"
    
  end function ION_new_from_read
  function ION_new_from_read_zigzag() result(res)
    use class_CHD_channel_and_dipole
    type(ION_Object) :: res
    character        :: in_mode
    real*8           :: mn, mx, de
    integer          :: i, pn, tmp, ll
    integer, allocatable :: eff_zeta_start_indexes(:)
    integer          :: basis_num, skip_num, loop_num, order, channel, eff_num
    character        :: basis_type, wkb_type, form_ch
    real*8           :: r0, r1, rp, ip
    logical          :: l1, l2, l3 ! dummy
    complex*16, allocatable :: oe_array(:)
    logical, allocatable    :: optQ_array(:)
    integer, allocatable    :: pn_array(:), opt_info(:)
    type(CL2F_Object)       :: basis
    
    write(*,*) "Basis set(s: STO, g:GTO"
    read(*,*) basis_type
    write(*,*) basis_type
    write(*,*) "mathing way(w: WKB), matching point(r0), asymtptic point(r1), order"
    read(*,*) wkb_type, r0, r1, order
    write(*,*) wkb_type, r0, r1, order
    write(*,*) "min, max, delta (photoelectron KE)"
    read(*,*) mn, mx, de
    write(*,*) mn, mx, de
    write(*,*) "skip num, loop num"
    read(*,*) skip_num, loop_num
    write(*,*) skip_num, loop_num
    
    write(*,*) "input basis_num, channel, form, eff zeta num"
    read(*,*) basis_num, channel, form_ch, eff_num
    write(*,*) basis_num, channel, form_ch, eff_num
    
    allocate(oe_array(basis_num))
    allocate(optQ_array(basis_num))
    allocate(pn_array(basis_num))
    allocate(opt_info(basis_num))
    allocate(eff_zeta_start_indexes(eff_num))

    write(*,*) "eff zeta start index"
    do i = 1, eff_num
       read(*,*) eff_zeta_start_indexes(i)
    end do
    write(*,*) eff_zeta_start_indexes
    
    write(*,*) "input real(zeta) imag(zeta) pn opt?"
    do i = 1, basis_num
       read(*,*) rp, ip, pn, l1
       write(*,*) rp, ip, pn, l1
       oe_array(i) = cmplx(rp, ip)
       pn_array(i) = pn
       optQ_array(i) = l1
    end do

    res % cbf % channel_dipole = CHD_new_from_character(channel, form_ch)
    
    res % cbf % basis_set = LCCF_new(basis_num)
    do i = 1, basis_num
       basis = CL2F_new_normalize_from_char(pn_array(i), oe_array(i),  basis_type)
       call LCCF_set_CL2F_at_n(res % cbf % basis_set, basis, i)
    end do

    res % cbf % opt = OPT_new_from_start_index(&
         eff_zeta_start_indexes, basis_num ,loop_num)
    ll = CHD_l_final_state(res % cbf % channel_dipole)
    write(*,*) "ll:", ll
    res % cbf % wkb = WKB_new(r0, r1, order, ll)
         
    res % energy_range = CN_new(mn, mx, de)
    res % skip_print_num = skip_num

    write(*,*) "----------------------"
    call CBF_display(res % cbf)
    write(*,*) "----------------------"

  end function ION_new_from_read_zigzag
  function ION_new_from_read_general() result(res)
    use class_CHD_channel_and_dipole
    type(ION_Object) :: res
    character        :: in_mode
    real*8           :: mn, mx, de
    integer          :: i, pn, tmp, ll
    integer, allocatable :: eff_zeta_start_indexes(:)
    integer          :: basis_num, skip_num, loop_num, order, channel
    character        :: basis_type,  form_ch, wkb_type
    real*8           :: r0, r1, rp, ip, in_charge
    complex*16, allocatable :: oe_array(:)
    integer, allocatable    :: pn_array(:), opt_info_array(:)
    real*8                  :: eps_zeta, eps_grad
    type(CL2F_Object)       :: basis
    
    write(*,*) "Basis set(s: STO, g:GTO"
    read(*,*) basis_type
    write(*,*) basis_type
    write(*,*) "wkb_type, matching point(r0), asymtptic point(r1), order"
    read(*,*)  wkb_type, r0, r1, order
    write(*,*)  wkb_type, r0, r1, order
    write(*,*) "min, max, delta (photoelectron KE)"
    read(*,*) mn, mx, de
    write(*,*) mn, mx, de
    write(*,*) "skip num, loop num"
    read(*,*) skip_num, loop_num
    write(*,*) skip_num, loop_num
    
    write(*,*) "input basis_num, channel, form, charge"
    read(*,*) basis_num, channel, form_ch, in_charge
    write(*,*) basis_num, channel, form_ch, in_charge
    
    allocate(oe_array(basis_num))
    allocate(pn_array(basis_num))
    allocate(opt_info_array(basis_num))
    
    write(*,*) "input real(zeta) imag(zeta) pn opt_index(0:fix,1-n:opt)"
    do i = 1, basis_num
       read(*,*) rp, ip, pn_array(i), opt_info_array(i)
       oe_array(i) = cmplx(rp, ip)
       write(*,*) oe_array(i), pn_array(i), opt_info_array(i)
    end do

    write(*,*) "eps eps_zeta, eps_grad"
    read(*,*) eps_zeta, eps_grad
    write (*,*) eps_zeta, eps_grad

    write (*,*) "constructing channel and dipole..."
    res % cbf % channel_dipole = &
         CHD_new(channel, CHD_get_DP_from_character(form_ch), &
         in_charge, .true.)

    write (*,*) "constructing read write file info"
    call ION_read_write_file_info(res)

    write (*,*) "constructing basis set..."
    
    res % cbf % basis_set = LCCF_new(basis_num)
    do i = 1, basis_num
       basis = CL2F_new_normalize_from_char(pn_array(i), oe_array(i),  basis_type)
       call LCCF_set_CL2F_at_n(res % cbf % basis_set, basis, i)
    end do

    write (*,*) "constructing opt..."
    res % cbf % opt = OPT_new(opt_info_array, loop_num)
    res % cbf % opt % eps_grad = eps_grad
    res % cbf % opt % eps_val = eps_zeta

    write (*,*) "constructing wkb..."
    ll = CHD_l_final_state(res % cbf % channel_dipole)
    res % cbf % wkb = WKB_new(r0, r1, order, ll)

    write (*,*) "constructing energies range..."
    res % energy_range = CN_new(mn, mx, de)
    res % skip_print_num = skip_num

    write(*,*) "----------------------"
    call CBF_display(res % cbf)
    write(*,*) "----------------------"
  end function ION_new_from_read_general
  !===========read write data=================
  subroutine ION_read_write_file_info(this)
    type(ION_Object) :: this
    logical          :: file_existQ = .false.
    integer, parameter :: fileno = 133
    real*8             :: x0, x1, dx
    
    open(fileno, file = 'write.in.d', status = 'old',err = 999)

    file_existQ = .true.

    write (*,*) ""
    write (*,*) "reading write file info ...."
    write (*,*) ""
    
    write (*,*) "write psi?, write wkb?, write basis?"
    read(fileno,*) this % write_psiQ, this % write_wkbQ, this % write_basisQ
    write (*,*) this % write_psiQ, this % write_wkbQ, this % write_basisQ

    write (*,*) "x0, x1, dx"
    read(fileno,*) x0, x1, dx
    write (*,*) x0, x1, dx

    !    this % func_write = FW_new(2, x0, x1, dx)
    this % r_range = CN_new(x0, x1, dx)

    write (*,*) "write grad and hess ?"
    read(fileno, *) this % write_grad_hessQ
    write (*,*) this % write_grad_hessQ


    write (*,*) "write matrix?"
    read(fileno, *) this % write_matrixQ
    write (*,*) this % write_matrixQ

    close(fileno)

    ! file does not exist
999 if(.not. file_existQ) then
       this % write_psiQ = .false.
       this % write_basisQ = .false.
       this % write_wkbQ = .false.
       this % write_grad_hessQ = .false.
    end if

  end subroutine ION_read_write_file_info
  !===========Calculation===============
  subroutine ION_calc(this)
    type(ION_Object) :: this
    real*8           :: energy
    integer          :: i
    logical          :: convQ
    write (*,*) ""
    write (*,*) "START CALCULATION"
    write (*,*) ""    
    do while(CN_set_next(this % energy_range, energy, i))

       ! update energy and optimize.
       call CBF_set_energy(this % cbf, energy)
       convQ = CBF_optimize_orbital_exponent(this % cbf)

       ! show information if optimization is failed
       if(.not. convQ) then
          write(*,*) "--------Divergence------------"
          write(*,*) "energy: ", energy
          call CBF_display2(this % cbf)
          stop
       end if

       ! show information for success of optimization
       if(mod(i-1, this % skip_print_num) .eq. 0) then
          write(*,*) "--------Convergence-----------"
          write(*,*) "energy: ", energy
          call CBF_display2(this % cbf)
          if (this % cbf % channel_dipole % customQ) then
             call CBF_print_calc_res_from_wkb(this % cbf)
          else
             call CBF_print_calc_res_cross_section(this % cbf)
             call CBF_print_calc_res_phase(this % cbf)
          end if
       end if

       ! write wave function of first loop
       if (i .eq. 1) then
          
          if (this % write_psiQ) then
             call CBF_write_file_use_cn(&
                  this % cbf, 'psi.d', this % r_range)
          end if

          if (this % write_basisQ) then
             call CBF_write_one_basis(this % cbf, 'basis', 0.0d0, 100.0d0, 0.1d0)
          end if

          if (this % write_grad_hessQ) then
             call CBF_write_grad_and_hess(this % cbf, 'cbf')
          end if
          
          if (this % write_matrixQ) then
             call CBF_write_matrix(this % cbf, 'mvec.d')
          end if
          
       end if
    end do

    ! finalize
    call CBF_delete(this % cbf)
    
  end subroutine ION_calc
end module class_ION_input_output_normal
