module class_IOA_input_output_asym
  use class_CBF_complex_basis_function_h
  use class_CN_contium_number
  implicit none

  type IOA_Object
     type(CBF_Object) :: cbf_array(2) ! 1:plus channel, 2:minus channel
     type(CN_Object)  :: energy_controller
     integer          :: skip_num
  end type IOA_Object

contains
  !==========Constructor==============
  function IOA_new_from_read() result(res)
    use class_CHD_channel_and_dipole
    use class_WKB_wkb_mathcing
    type(IOA_Object) :: res
    integer          :: pm, i
    character        :: basis_type, matching_way, form, opt_info_char
    real*8           :: r0, r1, rp, ip, start_ene, end_ene, step_ene
    integer          :: basis_num, order, channel, pn, tmp, loop_num
    integer, allocatable :: opt_info(:)
    type(CL2F_Object)    :: basis

    do pm = 1, 2
       if(pm .eq. 1) then
          write(*,*) "-----------------plus channel----------------"
       else
          write(*,*) "-----------------minus channel----------------"
       end if
       write(*,*) "basis function (s :STO, g:GTO)"
       read(*,*) basis_type
       write(*,*) basis_type
       
       write(*,*) "Matching way(w,u,o,f), matching point, asymptotic point, order"
       read(*,*) matching_way, r0, r1, order
       write(*,*) matching_way, r0, r1, order
    
       write(*,*) "input num_basis, loopnum"
       read(*,*) basis_num, loop_num
       write(*,*) basis_num, loop_num
       
       write(*,*) "channel, dipole operator form "
       read(*,*) channel, form
       write(*,*) channel, form

       allocate(opt_info(basis_num))

       res % cbf_array(pm) % channel_dipole  = CHD_new_from_character(channel, form)
       res % cbf_array(pm) % basis_set = LCCF_new(basis_num)
       res % cbf_array(pm) % wkb = WKB_new(r0, r1, order, &
            CHD_l_final_state(res % cbf_array(pm) % channel_dipole))

       tmp = 1
       write(*,*) "input orbital exponent, principle number, opt info"
       do i = 1, basis_num
          read(*,*) rp, ip, pn, opt_info_char
          basis = CL2F_new_normalize_from_char(pn, dcmplx(rp, ip), basis_type)
          call LCCF_set_CL2F_at_n(res % cbf_array(pm) % basis_set, basis, i)

          if(opt_info_char .eq. "F") then
             opt_info(i) = 0
          else if(opt_info_char .eq. "T") then
             opt_info(i) = tmp
             tmp = tmp + 1
          else
             opt_info(i) = ichar(opt_info_char) - ichar('0')
          end if
       end do
       res % cbf_array(pm) % opt = OPT_new(opt_info, loop_num)

       deallocate(opt_info)
    end do

    write(*,*) "start ene, end ene, step ene"
    read(*,*) start_ene, end_ene, step_ene
    write(*,*) start_ene, end_ene, step_ene
    res % energy_controller = CN_new(start_ene, end_ene, step_ene)
    write(*,*) "step num"
    read(*,*)  res % skip_num
    write(*,*) res % skip_num

    do pm = 1, 2
       write(*,*) "----------------------"
       call CBF_display(res % cbf_array(pm))
       write(*,*) "----------------------"
    end do
    
  end function IOA_new_from_read
  !=========Calculation===============
  subroutine IOA_calc(this)
    type(IOA_Object) :: this
    real*8           :: energy
    integer          :: i, pm, l0
    logical          :: convQ_array(2)
    real*8           :: phase_array(2), moment_array(2)
    real*8           :: beta, exact, re
    do while(CN_set_next(this % energy_controller, energy, i))

       do pm = 1, 2
          call CBF_set_energy(this % cbf_array(pm), energy)
          convQ_array(pm) = CBF_optimize_orbital_exponent(this % cbf_array(pm))
       end do

       if( .not. (convQ_array(1) .and. convQ_array(2))) then
          write(*,*) "----Divergence-----"
          stop
       end if

       if(mod(i - 1, this % skip_num) .eq. 0) then
          write(*,*) "-------------------"
          write(*,*) "energy:", energy
          call CBF_print_calc_res_asymmetry_parameter( &
               this % cbf_array(1), this % cbf_array(2))
       end if
    end do
!          moment_array(j) = CBF_calc_regular_radial_transition_dipole_moment(this % cbf_array(j))
!          phase_array(j) = CBF_calc_phase_shift_WKB(this % cbf_array(j))
!       end do
!
!       if(convQ_array(1) .and. convQ_array(2)) then
!          l0 = CHD_l_initial_state(this % cbf_array(1) % channel_dipole)
!          beta = IOA_calc_beta_from_moment_and_phase(&
!               moment_array(1), moment_array(2), phase_array(1), phase_array(2), l0)
!          exact = IOA_calc_beta_exact(this, energy)
!          re = (beta - exact) / exact * 100
       
!          write(*,*) "------------------"
!          write(*,*) "energy: ", energy
!          write(*,*) "phase: ", phase_array
!          write(*,*) "moment: ", moment_array
!          write(*,*) "beta: ", beta
!          write(*,*) "exact:", exact
!          write(*,*) "re:", re
!       else
!          write(*,*) "---Divergence-----"
!          stop
!       end if
!    end do
  end subroutine IOA_calc
  function IOA_calc_beta_from_moment_and_phase(rdp, rdm, psp, psm, l) result(res)
    real*8, intent(in)  :: rdp, rdm, psp, psm
    integer, intent(in) :: l
    real*8              :: res, tmp0, tmp1, tmp2, tmp3

    tmp0 = l * (l - 1) * rdm * rdm
    tmp1 = (l + 1) * (l + 2) * rdp * rdp
    tmp2 = - 6 * l * (l + 1) * rdp * rdm * cos(psp - psm)
    tmp3 = (2 * l + 1) * (l * rdm * rdm + (l + 1) * rdp * rdp)
    res = (tmp0 + tmp1 + tmp2) / tmp3
  end function IOA_calc_beta_from_moment_and_phase
  function IOA_calc_beta_exact(this, energy) result(res)
    use class_CHD_channel_and_dipole
    type(IOA_Object)    :: this
    real*8, intent(in)  :: energy
    real*8              :: res, w

    if(this % cbf_array(2) % channel_dipole % channel .eq. CHD_CHA_2p_ks) then
       res = 4.0d0 / ( 6.0d0 * ( energy + 0.125) + 2.0d0)
    else if(this % cbf_array(2) % channel_dipole % channel.eq. CHD_CHA_3d_kp) then
       w = energy + 1.0d0 / 18.0d0
       res = (2.0d0 + 3.0d0 * w) / (1.0d0 + 15.0d0 * w)
    end if
  end function IOA_calc_beta_exact
end module class_IOA_input_output_asym
