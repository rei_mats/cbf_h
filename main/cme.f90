module complex_matrix_elements
  implicit none
  private
  integer                 :: basis_num       ! number of basis
  character               :: basis_type      ! s:STO, g:GTO
  complex*16, allocatable :: zeta_r(:)       ! exponents
  integer, allocatable    :: pn_r(:)         ! principle number of STO/GTO
  complex*16, allocatable :: c_r(:)          ! expand coefficient
  real*8                  :: charge          ! charge of nucleus

  !--------Normalizarion term------------------------------------------
  complex*16,allocatable :: n_r(:)           ! normalization constant
  complex*16,allocatable :: nr_r(:)          ! n_r' / n_r
  complex*16,allocatable :: nrr_r(:)         ! n_r'' / n_r
  
  !--------matrix element for calculating------------------------------
  complex*16,allocatable :: s_rs(:,:)        ! (chi_r | chi_s)
  complex*16,allocatable :: h_rs(:,:)        ! (chi_r | T + V | chi_s)
  complex*16,allocatable :: hmes_rs(:,:)     ! (chi_r | H -ES | chi_s)
  complex*16,allocatable :: hmes_rs_dt(:,:,:)
  complex*16,allocatable :: hmes_rs_dtdu(:,:,:,:)
  complex*16,allocatable :: m_r(:)           ! (chi_r | mu psi_0)
  complex*16,allocatable :: m_r_dt(:,:)      ! d/d(zeta_t) m_r
  complex*16,allocatable :: m_r_dtdu(:,:,:)  ! d/d(zeta_t zeta_u) m_r
  complex*16,allocatable :: g_rs(:,:)        ! Green's Function
  !---------variable using for custom channel dipole-------------------
  !---------c means custom---------------------------------------------
  complex*16,allocatable :: c_coe_i (:)
  complex*16,allocatable :: c_zeta_i (:)
  integer,   allocatable :: c_pn_i (:)
  integer                :: c_num
  integer                :: c_l_final
  logical                :: c_customQ

  public :: CME_new, CME_delete, CME_calc_matrix_elements,&
       CME_get_alpha, CME_get_grad, CME_get_hess, CME_print, &
       CME_testcase_1basis, CME_get_c_r, CME_testcase_3dkp_1basis, CME_get_n_r, &
       CME_get_m_r, CME_testcase_sto_1basis, CME_testcase_sto_2basis, CME_testcase_gto_1basis, &
       CME_calc_base_matrix_elements, CME_get_eigen_vals, CME_get_eigen_val_and_coe, &
       CME_get_overlap_mat_eigen_and_coe, CME_get_s_rs, &
       CME_get_hmes_rs, CME_get_hmes_rs_dt, CME_get_hmes_rs_dtdu, &
        CME_get_m_r_dt, CME_get_m_r_dtdu, CME_set_pn_at_n, CME_set_zeta_at_n, &
        CME_set_custom_driven_term

contains
 
  subroutine CME_new(num, in_basis_type, in_charge)
    integer, intent(in)   :: num
    character, intent(in) :: in_basis_type
    real*8, intent(in), optional  :: in_charge
    
    basis_num = num
    basis_type = in_basis_type
    charge = 1.0d0

    
    
    if(basis_type .eq. "s") then   !cSTO
       basis_type = in_basis_type
    else if(basis_type .eq. "g") then
       basis_type = in_basis_type
    else
       write(*,*) "bad basis_type in CME_new"
       stop
    end if
    if(present(in_charge)) then
       charge = in_charge
    else
       charge = 1.0d0
    end if
    
    if(allocated(zeta_r) .eqv. .false.) then
       allocate(zeta_r(num))
       allocate(pn_r(num))
       allocate(c_r(num))
       allocate(n_r(num))
       allocate(nr_r(num))
       allocate(nrr_r(num))
       allocate(m_r(num))
       allocate(m_r_dt(num,num))
       allocate(m_r_dtdu(num,num,num))
       allocate(h_rs(num,num))
       allocate(s_rs(num,num))
       allocate(hmes_rs(num,num))
       allocate(hmes_rs_dt(num,num,num))
       allocate(hmes_rs_dtdu(num,num,num,num))
       allocate(g_rs(num,num))

    end if
    c_customQ = .false.
  end subroutine CME_new
  subroutine CME_set_zeta_at_n(zeta, n)
    complex*16, intent(in) :: zeta
    integer,    intent(in) :: n
    zeta_r(n) = zeta
  end subroutine CME_set_zeta_at_n
  subroutine CME_set_pn_at_n(pn, n)
    integer,  intent(in) :: pn
    integer,  intent(in) :: n
    pn_r(n) = pn
  end subroutine CME_set_pn_at_n
  subroutine CME_delete
    deallocate(zeta_r)
    deallocate(pn_r)
    deallocate(c_r)
    deallocate(n_r)
    deallocate(nr_r)
    deallocate(nrr_r)
    deallocate(m_r)
    deallocate(m_r_dt)
    deallocate(m_r_dtdu)
    deallocate(h_rs)
    deallocate(s_rs)
    deallocate(hmes_rs)
    deallocate(hmes_rs_dt)
    deallocate(hmes_rs_dtdu)
    deallocate(g_rs)

    deallocate(c_coe_i)
    deallocate(c_pn_i)
    deallocate(c_zeta_i)
  end subroutine CME_delete
  subroutine CME_set_custom_driven_term(coe_i, pn_i, oe_i, l)
    complex*16, intent(in) :: coe_i(:), oe_i(:)
    integer, intent(in)    :: pn_i(:), l

    c_num = size(coe_i)
    allocate(c_coe_i(c_num))
    allocate(c_pn_i(c_num))
    allocate(c_zeta_i(c_num))

    c_coe_i(:)  = coe_i(:)
    c_zeta_i(:) = oe_i(:)
    c_pn_i(:)   = pn_i(:)

    c_l_final = l
    c_customQ = .true.
  end subroutine CME_set_custom_driven_term
  !----------matrix_elements---------------------------------------
  subroutine CME_calc_base_matrix_elements(channel,form, energy)
    use math
    use class_CHD_channel_and_dipole
    integer, intent(in)       :: channel
    integer,intent(in)        :: form
    real*8, intent(in)        :: energy
    integer                   :: r, s, pnr, pns, nn, t, u, l
    complex*16                :: zr, zs, ctmp, ctmp2, ctmp3, tmph, tmps
    real*8                    :: c
    type(CHD_Object)          :: chd

!    if(channel .eq. CHD_CHA_custom) then
!       l = c_l_final
!    else
       chd = CHD_new(channel, form)
       l = CHD_l_final_state(chd)
!    end if

    call CME_calc_normalization()
    call CME_calc_moment_matrix_elements(channel, form)

    c = dble(l * l + l)
    do r = 1, basis_num
       do s = 1, basis_num
          ctmp = CME_t_rs(r,s) + c / 2.0d0 * CME_rm_rs(r,s,-2) &
               - CME_rm_rs(r,s,-1) * charge
          ctmp2= energy * CME_rm_rs(r,s,0)
          h_rs(r,s) = ctmp
          s_rs(r,s) = CME_rm_rs(r,s,0)
          hmes_rs(r, s) = ctmp2 - ctmp
       end do
    end do
    g_rs = MATH_complex_matrix_inverse(hmes_rs, basis_num)

    c_r = MATH_complex_a_inverse_b(hmes_rs, m_r)
  end subroutine CME_calc_base_matrix_elements
  subroutine CME_calc_matrix_elements(channel ,form, energy)
    use math
    use class_CHD_channel_and_dipole
    integer, intent(in)       :: channel
    integer, intent(in)     :: form
    real*8, intent(in)       :: energy
    integer                   :: r, s, pnr, pns, nn, t, u, l
    complex*16                :: zr, zs, ctmp, ctmp2, ctmp3, tmph, tmps
    real*8                    :: c

!    if(channel .eq. CHD_CHA_custom) then
!       l = c_l_final
!    else
       l = CHD_l_final_state(CHD_new(channel, form))
!    end if
    c = dble(l * l + l)

    call CME_calc_base_matrix_elements(channel ,form, energy)

    do r = 1, basis_num
       do s = 1, basis_num
          do t = 1, basis_num
             tmph = CME_t_rs_dt(r,s,t) - CME_rm_rs_dt(r,s,t,-1) * charge
             tmph = tmph + c / 2.0d0 * CME_rm_rs_dt(r,s,t,-2)
             !             hmes_rs_dt(r,s,t) = tmph - energy * CME_rm_rs_dt(r,s,t,0)
             hmes_rs_dt(r,s,t) =  energy * CME_rm_rs_dt(r,s,t,0) - tmph
          end do
       end do
    end do
    do r = 1, basis_num
       do s = 1, basis_num
          do t = 1, basis_num
             do u = 1, basis_num
                tmph = CME_t_rs_dtdu(r,s,t,u) - CME_rm_rs_dtdu(r,s,t,u,-1) * charge
                tmph = tmph + c/2.0d0 * CME_rm_rs_dtdu(r,s,t,u,-2)
                !                hmes_rs_dtdu(r,s,t,u) = tmph - energy * CME_rm_rs_dtdu(r,s,t,u,0)
                hmes_rs_dtdu(r,s,t,u) = energy * CME_rm_rs_dtdu(r,s,t,u,0) - tmph
             end do
          end do
       end do
    end do

  end subroutine CME_calc_matrix_elements
  subroutine CME_calc_normalization
    use math
    integer    :: r, pnr
    complex*16 :: zr, ctmp

    do r = 1, basis_num
       zr = zeta_r(r)
       pnr = pn_r(r)
       if(basis_type .eq. "s") then
          ctmp = sqrt(2.0d0*zr) ** (2*pnr+1)
          n_r(r)  = ctmp / sqrt( dble(MATH_fac(2*pnr) ))
          nr_r(r) = (2.0d0*pnr+1.0d0)/(2.0d0 * zr)
          nrr_r(r)= (4.0d0*pnr*pnr-1.0d0)/(4.0d0 * zr * zr)
       else
          ctmp = (2.0d0 / pi) ** 0.25d0 * zr **(pnr / 2) * zr ** (0.5d0 * pnr + 0.25d0 - pnr / 2)
          n_r(r)  = ctmp * sqrt(2.0d0 ** (2 * pnr + 1) * 1 / MATH_facfac(2 * pnr - 1))
          nr_r(r) = (0.25d0 + pnr * 0.5d0) / zr
          nrr_r(r)= (-0.75d0 + pnr * 0.5d0) * (0.25d0 + pnr * 0.5d0) / (zr * zr)
       end if
    end do
  end subroutine CME_calc_normalization
  subroutine CME_calc_moment_matrix_elements(channel, form)
    use math
    use class_CHD_channel_and_dipole
!    use channel_master
    integer, intent(in)        :: channel
    integer, intent(in)        :: form
    complex*16                 :: tmp1, tmp2, zr, basis_exp
    complex*16                 :: term1, term2
    integer                    :: pnr, r, t, b, u, i
    complex*16, allocatable    :: mr_r(:), mr2_r(:)
    complex*16                 :: c_coe, c_zeta
    integer                    :: c_pn
    allocate(mr_r(basis_num))
    allocate(mr2_r(basis_num))
    
    b = CME_get_power_of_exp()

    do r = 1, basis_num
       pnr = pn_r(r)
       zr = zeta_r(r)
       if(c_customQ) then
          m_r(r) = (0.0d0, 0.0d0)
          mr_r(r) = (0.0d0, 0.0d0)
          mr2_r(r) = (0.0d0, 0.0d0)
          do i = 1, c_num
             c_coe = c_coe_i(i)
             c_zeta= c_zeta_i(i)
             c_pn  = c_pn_i(i)
             if(basis_type .eq. "s") then
                m_r(r)   = m_r(r)  + n_r(r) * c_coe * MATH_slater_integral(pnr + c_pn, zr + c_zeta)
                mr_r(r)  = mr_r(r) + n_r(r) * c_coe * MATH_slater_integral(pnr + c_pn + 1, zr + c_zeta)
                mr2_r(r) = mr2_r(r)+ n_r(r) * c_coe * MATH_slater_integral(pnr + c_pn + 2, zr + c_zeta)
             else if(basis_type .eq. "g") then
                m_r(r)   = m_r(r)  + n_r(r) * c_coe * MATH_sto_gto_integrate(pnr + c_pn, c_zeta, zr, 10)
                mr_r(r)  = mr_r(r) + n_r(r) * c_coe * MATH_sto_gto_integrate(pnr + c_pn + 2, c_zeta, zr, 10)
                mr2_r(r) = mr2_r(r)+ n_r(r) * c_coe * MATH_sto_gto_integrate(pnr + c_pn + 4, c_zeta, zr, 10)
             else
                write (*,*) "bad basis type. CME_calc_moment_matrix_elements"
                stop
          end if
       end do
       else if(form .eq. CHD_DP_velocity) then     
          if(channel .eq. CHD_CHA_1s_kp) then  !1s->kp
             tmp1 = -n_r(r) * 2.0d0 / sqrt(3.0d0)
             basis_exp = cmplx(1.0d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r) =  tmp1 * MATH_slater_integral(pnr + 1, basis_exp + zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr + 2, basis_exp + zr)
                mr2_r(r) =tmp1 * MATH_slater_integral(pnr + 3, basis_exp + zr)
             else
                m_r(r) =  tmp1 * MATH_sto_gto_integrate(pnr + 1, basis_exp, zr, 10)
                mr_r(r) = tmp1 * MATH_sto_gto_integrate(pnr + 3, basis_exp, zr, 10)
                mr2_r(r) =tmp1 * MATH_sto_gto_integrate(pnr + 5, basis_exp, zr, 10)
             end if
          else if( channel .eq. CHD_CHA_2p_ks) then
             basis_exp = cmplx(0.5d0, 0.0d0)
             if(basis_type .eq. "s") then
                tmp1 = MATH_slater_integral(pnr + 1, basis_exp + zr)
                tmp2 = MATH_slater_integral(pnr + 2, basis_exp + zr)
                m_r(r) = n_r(r) * sqrt(2.0d0) / 4.0d0 * (tmp1 - tmp2 / 6.0d0)
                tmp1 = MATH_slater_integral(pnr + 2, basis_exp + zr)
                tmp2 = MATH_slater_integral(pnr + 3, basis_exp + zr)
                mr_r(r) = n_r(r) * sqrt(2.0d0) / 4.0d0 * (tmp1 - tmp2 / 6.0d0)
                tmp1 = MATH_slater_integral(pnr + 3, basis_exp + zr)
                tmp2 = MATH_slater_integral(pnr + 4, basis_exp + zr)
                mr2_r(r) = n_r(r) * sqrt(2.0d0) / 4.0d0 * (tmp1 - tmp2 / 6.0d0)
             else
                tmp1 = MATH_sto_gto_integrate(pnr + 1,  basis_exp, zr, 10)
                tmp2 = MATH_sto_gto_integrate(pnr + 2,  basis_exp, zr, 10)
                m_r(r) = n_r(r) * sqrt(2.0d0) / 4.0d0 * (tmp1 - tmp2 / 6.0d0)
                tmp1 = MATH_sto_gto_integrate(pnr + 3,  basis_exp, zr, 10)
                tmp2 = MATH_sto_gto_integrate(pnr + 4,  basis_exp, zr, 10)
                mr_r(r) = n_r(r) * sqrt(2.0d0) / 4.0d0 * (tmp1 - tmp2 / 6.0d0)
                tmp1 = MATH_sto_gto_integrate(pnr + 5,  basis_exp, zr, 10)
                tmp2 = MATH_sto_gto_integrate(pnr + 6,  basis_exp, zr, 10)
                mr2_r(r) = n_r(r) * sqrt(2.0d0) / 4.0d0 * (tmp1 - tmp2 / 6.0d0)
             end if
          else if(channel .eq. CHD_CHA_2p_kd) then
             tmp1 = -1.0d0 / (6.0d0 * sqrt(10.0d0)) * n_r(r)
             basis_exp = cmplx(0.5d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r)  = tmp1 * MATH_slater_integral(pnr + 2, basis_exp + zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr + 3, basis_exp + zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr + 4, basis_exp + zr)
             else
                m_r(r)  = tmp1 * MATH_sto_gto_integrate(pnr + 2, basis_exp, zr, 10)
                mr_r(r) = tmp1 * MATH_sto_gto_integrate(pnr + 4, basis_exp, zr, 10)
                mr2_r(r)= tmp1 * MATH_sto_gto_integrate(pnr + 6, basis_exp, zr, 10)
             end if
          else if(channel .eq. CHD_CHA_3d_kp) then
             if(basis_type .eq. "s") then
                basis_exp = cmplx(1.0d0/3.0d0, 0.0d0)
                tmp1 = MATH_slater_integral(pnr + 2, basis_exp + zr) * 2.0d0
                tmp2 = MATH_slater_integral(pnr + 3, basis_exp + zr) * (-4.0d0 / 30.0d0)
                m_r(r) = 4.0d0 / (81*sqrt(18.0d0)) * n_r(r) * (tmp1 + tmp2)
                tmp1 = MATH_slater_integral(pnr + 3, basis_exp + zr) * 2.0d0
                tmp2 = MATH_slater_integral(pnr + 4, basis_exp + zr) * (-4.0d0 / 30.0d0 )
                mr_r(r) = 4.0d0 / (81*sqrt(18.0d0)) * n_r(r) * (tmp1 + tmp2)
                tmp1 = MATH_slater_integral(pnr + 4, basis_exp + zr) * 2.0d0
                tmp2 = MATH_slater_integral(pnr + 5, basis_exp + zr) * (-4.0d0 / 30.0d0 )
                mr2_r(r) = 4.0d0 / (81*sqrt(18.0d0)) * n_r(r) * (tmp1 + tmp2)
             else
                write(*,*) "mada junbi shitenai. 3d->kp channle GTO"
                stop
             end if
          else if(channel .eq. CHD_CHA_3d_kf) then
             tmp1 = -4.0d0 / (81*5*sqrt(42.0d0)) * n_r(r)
             basis_exp = cmplx(1.0d0 / 3.0d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r) = tmp1 * MATH_slater_integral(pnr + 3, basis_exp + zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr + 4, basis_exp + zr)
                mr2_r(r) = tmp1 * MATH_slater_integral(pnr + 5, basis_exp + zr)
             else
                m_r(r) = tmp1 * MATH_slater_integral(pnr + 3, basis_exp + zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr + 5, basis_exp + zr)
                mr2_r(r) = tmp1 * MATH_slater_integral(pnr + 7, basis_exp + zr)
             end if
          else
             write(*,*) "Bad channel in CME_calc_moment_matrix_elements"
             stop
          end if
       else if (form .eq. CHD_DP_accelate) then
          if(channel .eq. CHD_CHA_2p_ks) then
             tmp1 = n_r(r) * 1.0d0 / (6.0d0 * sqrt(2.0d0))
             basis_exp = (0.5d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r)  = tmp1 * MATH_slater_integral(pnr,     0.5d0 + zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr + 1, 0.5d0 + zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr + 2, 0.5d0 + zr)
             else
                write (*,*) "mada junbi site nai. in CME_calc_moment_matrxi_element."
                write (*,*) "bad basis"
                stop
             end if
          else if(channel .eq. CHD_CHA_1s_kp) then
             tmp1 = n_r(r) * 2.0d0 / sqrt(3.0d0)
             basis_exp = cmplx(1.0d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r)  = tmp1 * MATH_slater_integral(pnr-1, 1.0d0+zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+0, 1.0d0+zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+1, 1.0d0+zr)
             else
                write (*,*) "mada junbi sitenai. in CME_calc_moment_matrxi_element"
                write (*,*) "bad basis"
                stop
             end if
          else if(channel .eq. CHD_CHA_2p_kd) then
             tmp1 =  n_r(r) * 1.0d0 / (3.0d0 * sqrt(10.0d0))
             basis_exp = cmplx(0.5d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r) = tmp1 * MATH_slater_integral(pnr+0, 0.5d0+zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+1, 0.5d0+zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+2, 0.5d0+zr)
             else
                m_r(r) = tmp1 * MATH_sto_gto_integrate(pnr, basis_exp, zr, 10)
                mr_r(r) = tmp1 * MATH_sto_gto_integrate(pnr+2, basis_exp, zr, 10)
                mr2_r(r)= tmp1 * MATH_sto_gto_integrate(pnr+4, basis_exp, zr, 10)
             end if
          else if (channel .eq. CHD_CHA_3d_kp) then
             tmp1 = n_r(r) * 4.0d0 / (81*sqrt(30.0d0)) * sqrt(4.0d0/3.0d0) / sqrt(5.0d0)
             if(basis_type .eq. "s") then
                m_r(r) =  tmp1 * MATH_slater_integral(pnr+1, 1.0d0/3.0d0+zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+2, 1.0d0/3.0d0+zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+3, 1.0d0/3.0d0+zr)
             else
                write(*,*) "mada junbi shitenai in CME_calc_moment_matrix_elements. &
                     & 3d->kp. accel"
                stop
             end if
          else if (channel .eq. CHD_CHA_3d_kf) then
             tmp1 = n_r(r) * 4.0d0 / (81*sqrt(30.0d0)) * sqrt(4.0d0 / 3.0d0) &
                  * sqrt(27.0d0 / 140.0d0)
             if(basis_type .eq. "s") then
                m_r(r) =  tmp1 * MATH_slater_integral(pnr+1, 1.0d0 / 3.0d0 + zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+2, 1.0d0 / 3.0d0 + zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+3, 1.0d0 / 3.0d0 + zr)
             else
                write(*,*) "mada junbi shitenai in CME_calc_moment_matrix_elements. &
                     & 3d->kf. Length"
                stop
             end if
          else
             write (*,*) "mada junbi sitenai. in CME_calc_moment_matrxi_element"
             write (*,*) "bad channel"
             stop
          end if
       else if (form .eq. CHD_DP_length) then
          if(channel .eq. CHD_CHA_1s_kp) then        ! 1s -> kp
             tmp1 = n_r(r) * 2.0d0 / sqrt(3.0d0)
             basis_exp = cmplx(1.0d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r)  = tmp1 * MATH_slater_integral(pnr+2, 1.0d0+zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+3, 1.0d0+zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+4, 1.0d0+zr)
             else
                m_r(r)  = tmp1 * MATH_sto_gto_integrate(pnr+2, basis_exp, zr, 10)
                mr_r(r)  = tmp1 * MATH_sto_gto_integrate(pnr+4, basis_exp, zr, 10)
                mr2_r(r)  = tmp1 * MATH_sto_gto_integrate(pnr+6,basis_exp, zr, 10)
             end if
          else if(channel .eq. CHD_CHA_2p_ks) then   ! 2pz -> ks
             tmp1 =  n_r(r) * 1.0d0 / (3.0d0 * sqrt(8.0d0))
             basis_exp = cmplx(0.5d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r) = tmp1 * MATH_slater_integral(pnr+3, 0.5d0+zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+4, 0.5d0+zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+5, 0.5d0+zr)
             else
                m_r(r) = tmp1 * MATH_sto_gto_integrate(pnr+3, basis_exp, zr, 10)
                mr_r(r) = tmp1 * MATH_sto_gto_integrate(pnr+5, basis_exp, zr, 10)
                mr2_r(r)= tmp1 * MATH_sto_gto_integrate(pnr+7, basis_exp, zr, 10)
             end if
          else if(channel .eq. CHD_CHA_2p_kd) then        ! 2pz->kd
             tmp1 =  n_r(r) * 1.0d0 / (3.0d0 * sqrt(10.0d0))
             basis_exp = cmplx(0.5d0, 0.0d0)
             if(basis_type .eq. "s") then
                m_r(r) = tmp1 * MATH_slater_integral(pnr+3, 0.5d0+zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+4, 0.5d0+zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+5, 0.5d0+zr)
             else
                m_r(r) = tmp1 * MATH_sto_gto_integrate(pnr+3, basis_exp, zr, 10)
                mr_r(r) = tmp1 * MATH_sto_gto_integrate(pnr+5, basis_exp, zr, 10)
                mr2_r(r)= tmp1 * MATH_sto_gto_integrate(pnr+7, basis_exp, zr, 10)
             end if
          else if(channel .eq. CHD_CHA_3d_kp) then
             tmp1 = n_r(r) * 4.0d0 / (81*sqrt(30.0d0)) * sqrt(4.0d0/3.0d0) / sqrt(5.0d0)
             if(basis_type .eq. "s") then
                m_r(r) =  tmp1 * MATH_slater_integral(pnr+4, 1.0d0/3.0d0+zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+5, 1.0d0/3.0d0+zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+6, 1.0d0/3.0d0+zr)
             else
                write(*,*) "mada junbi shitenai in CME_calc_moment_matrix_elements. &
                     & 3d->kp. Length"
                stop
             end if
          else if(channel .eq. CHD_CHA_3d_kf) then
             tmp1 = n_r(r) * 4.0d0 / (81*sqrt(30.0d0)) * sqrt(4.0d0 / 3.0d0) &
                  * sqrt(27.0d0 / 140.0d0)
             if(basis_type .eq. "s") then
                m_r(r) =  tmp1 * MATH_slater_integral(pnr+4, 1.0d0 / 3.0d0 + zr)
                mr_r(r) = tmp1 * MATH_slater_integral(pnr+5, 1.0d0 / 3.0d0 + zr)
                mr2_r(r)= tmp1 * MATH_slater_integral(pnr+6, 1.0d0 / 3.0d0 + zr)
             else
                write(*,*) "mada junbi shitenai in CME_calc_moment_matrix_elements. &
                     & 3d->kf. Length"
                stop
             end if
          else if(channel .eq. 10) then
             !             tmp1 = 1.0d-9 * n_r(r)
             !             mum = 9
             !             m_r(r)  = tmp1 * MATH_fac(pnr + mum ) / ((zr + 0.3d0)**(pnr + mum + 1))
             !             mr_r(r) = tmp1 * MATH_fac(pnr + mum +1) / ((zr + 0.3d0)**(pnr + mum + 2))
             !             mr2_r(r)= tmp1 * MATH_fac(pnr + mum +2) / ((zr + 0.3d0)**(pnr + mum + 3))
             write(*,*) "mada junbi sitenai. in CME_calc_moment_matrix_elements."
             stop
             tmp1 = 1.0d0
             m_r(r)  = tmp1 * MATH_fac(pnr +1) &
                  / ((zr + 0.5d0)**(pnr + 1 + 1))
             mr_r(r) = tmp1 * MATH_fac(pnr +2) &
                  / ((zr + 0.5d0)**(pnr + 1 + 1 + 1))
             mr2_r(r)= tmp1 * MATH_fac(pnr +3) &
                  / ((zr + 0.5d0)**(pnr + 1 + 1 + 2))
          else if(channel .eq. CHD_CHA_2s_kp) then
             !             tmp1 = CHA_matrix_ele_cos(1,0,0,0) / sqrt(2.0d0) * n_r(r)
             tmp1 = 1.0d0 / sqrt(3.0d0 * 2.0d0) * n_r(r)
             
             term1 = MATH_slater_integral(pnr + 2, 0.5d0 + zr)
             term2 = MATH_slater_integral(pnr + 3, 0.5d0 + zr)
             m_r(r) = tmp1 * (term1 - 1.0d0 / 2.0d0 * term2)
             
             term1 = MATH_slater_integral(pnr + 3, 0.5d0 + zr)
             term2 = MATH_slater_integral(pnr + 4, 0.5d0 + zr)
             mr_r(r) = tmp1 * (term1 - 1.0d0 / 2.0d0 * term2)
             
             term1 = MATH_slater_integral(pnr + 4, 0.5d0 + zr)
             term2 = MATH_slater_integral(pnr + 5, 0.5d0 + zr)
             mr2_r(r) = tmp1 * (term1 - 1.0d0 / 2.0d0 * term2)
          else
             write(*,*) "bad channel. in CME_calc_moment_matrix_elements"
             stop
          end if
       end if
    end do
    
    do r = 1, basis_num
       do t = 1, basis_num
          if(r .eq. t) then
             m_r_dt(r, t) = nr_r(r) * m_r(r) - mr_r(r)
          else
             m_r_dt(r, t) = (0.0d0, 0.0d0)
          end if
          do u = 1, basis_num
             if( (r .eq. t) .and. (r .eq. u)) then
                tmp1 = nrr_r(r) * m_r(r)
                tmp2 = -2.0d0 * nr_r(r) * mr_r(r)
                m_r_dtdu(r,t,u) = tmp1 + tmp2 + mr2_r(r)
             else
                m_r_dtdu(r,t,u) = (0.0d0, 0.0d0)
             end if
          end do
       end do
    end do
  end subroutine CME_calc_moment_matrix_elements
  function CME_rm_rs(r,s,m) result(res)
    use math
    integer, intent(in) :: r, s, m
    complex*16          :: res, z, n
    integer             :: mm

    z = zeta_r(r) + zeta_r(s)
    n = n_r(r) * n_r(s)
    mm =  pn_r(r) + pn_r(s) + m 

    if(basis_type .eq. "s") then
       res = n * MATH_slater_integral(mm, z)
    else
       res = n * MATH_gauss_integral(mm, z)
    end if
  end function CME_rm_rs
  function CME_rm_rs_dt(r,s,t,m) result(res)
    use math
    integer, intent(in) :: r, s, t, m
    complex*16          :: res
    integer             :: b

    b = CME_get_power_of_exp()
    if( (r .eq. t) .and. (s .eq. t) ) then
       res = 2 * nr_r(r) * CME_rm_rs(r,s,m) - 2 * CME_rm_rs(r,s,m+b)
    else if(r .eq. t) then
       res = nr_r(r) * CME_rm_rs(r,s,m) - CME_rm_rs(r,s,m+b)
    else if(s .eq. t) then
       res = nr_r(s) * CME_rm_rs(r,s,m) - CME_rm_rs(r,s,m+b)
    else
       res = (0.0d0, 0.0d0)
    end if
  end function CME_rm_rs_dt
  function CME_rm_rs_dtdu(r,s,t,u,m) result(res)
    use math
    integer, intent(in) :: r, s, t, u, m
    integer             :: xr, xs, xt, xu
    complex*16          :: res, term1, term2, term3, term4
    integer             :: b
    b = CME_get_power_of_exp()
    if(r >= s) then
       xr = r;        xs = s
    else
       xr = s;        xs = r
    end if
    if(t >= u) then
       xt = t;        xu = u
    else
       xt = u;        xu = t
    end if
    if( (xr .eq. xs) .and. (xr .eq. xt) .and. (xr .eq. xu) ) then
       term1 = +2 * nrr_r(r)          * CME_rm_rs(r,r,m) 
       term2 = +2 * nr_r(r) * nr_r(r) * CME_rm_rs(r,r,m) 
       term3 = -8 * nr_r(r)           * CME_rm_rs(r,r,m + b)
       term4 = +4                     * CME_rm_rs(r,r,m + 2*b)
       res = term1 + term2 + term3 + term4
    else if( (xr .eq. xt) .and. (xr .eq. xu)) then
       term1 =  1 * nrr_r(xr) *   CME_rm_rs(xr,xs,m) 
       term2 = -2 * nr_r(xr)  * CME_rm_rs(xr,xs,m+b) 
       term3 =                  CME_rm_rs(xr,xs,m+2*b)
       res =  term1 + term2 + term3
    else if( (xs .eq. xt) .and. (xs .eq. xu)) then
       term1 =  1 * nrr_r(xs) *   CME_rm_rs(xr,xs,m) 
       term2 = -2 * nr_r(xs)  * CME_rm_rs(xr,xs,m+b) 
       term3 =                  CME_rm_rs(xr,xs,m+2*b)
       res =  term1 + term2 + term3
    else if( (xr .eq. xt) .and. (xs .eq. xu) ) then
       term1 =   nr_r(xr) * nr_r(xs)  * CME_rm_rs(xr,xs,m)
       term2 = -(nr_r(xr) + nr_r(xs)) * CME_rm_rs(xr,xs,m+b)
       term3 =  CME_rm_rs(xr,xs,m+2*b)
       res = (term1 + term2 + term3)
    else
       res = (0.0d0, 0.0d0)
    end if
    
  end function CME_rm_rs_dtdu
  function CME_t_rs(r, s) result(res)
    integer, intent(in) :: r, s
    complex*16 res, z, term1, term2, term3
    integer pn
    
    z = zeta_r(s)
    pn = pn_r(s)

    if(basis_type .eq. "s") then
       term1 = +pn * (pn - 1) * CME_rm_rs(r,s,-2) 
       term2 = -2 * pn * z *    CME_rm_rs(r,s,-1)
       term3 = +z * z  *        CME_rm_rs(r,s,0)
       res = -0.5d0 * (term1 + term2 + term3)
    else
       term1 = +pn * (pn - 1) * CME_rm_rs(r,s,-2) 
       term2 = -2*z *(2*pn+1) * CME_rm_rs(r,s,0)
       term3 = +4 * z * z     * CME_rm_rs(r,s,2)
       res = -0.5d0 * (term1 + term2 + term3)
    end if
  end function CME_t_rs
  function CME_t_rs_dt(r, s, t) result(res)
    integer, intent(in) :: r, s, t
    complex*16 res, z, term1, term2, term3, term4, term5
    integer pn
    
    z = zeta_r(s)
    pn = pn_r(s)

    if(basis_type .eq. "s") then !cSTO
       if(s .eq. t) then
          term1 = +pn * (pn - 1) * CME_rm_rs_dt(r,s,t,-2) 
          term2 = -2 * pn * z *    CME_rm_rs_dt(r,s,t,-1)
          term3 = +z * z  *        CME_rm_rs_dt(r,s,t,0)
          term4 = -2 * pn *        CME_rm_rs(r,s,-1)
          term5 = +2 * z  *        CME_rm_rs(r,s,0)
          res = -0.5d0 * (term1 + term2 + term3 + term4 + term5)
       else if(r .eq. t) then
          term1 = +pn * (pn - 1) * CME_rm_rs_dt(r,s,t,-2) 
          term2 = -2 * pn * z *    CME_rm_rs_dt(r,s,t,-1)
          term3 = +z * z  *        CME_rm_rs_dt(r,s,t,0)
          res = -0.5d0 * (term1 + term2 + term3)
       else
          res = (0.0d0, 0.0d0)
       end if
    else !cGTO
       if(s .eq. t) then
          term1 = +pn * (pn - 1) * CME_rm_rs_dt(r,s,t,-2) 
          term2 = -2*z*(2*pn+1)  * CME_rm_rs_dt(r,s,t,0)
          term3 = +4 * z * z     * CME_rm_rs_dt(r,s,t,2)
          term4 = -2*(2*pn+1)    * CME_rm_rs(r,s,0)
          term5 = +8 * z         * CME_rm_rs(r,s,2)
          res = -0.5d0 * (term1 + term2 + term3 + term4 + term5)
       else if(r .eq. t) then
          term1 = +pn * (pn - 1) * CME_rm_rs_dt(r,s,t,-2) 
          term2 = -2*z*(2*pn+1)  * CME_rm_rs_dt(r,s,t,0)
          term3 = +4 * z * z     * CME_rm_rs_dt(r,s,t,2)
          res = -0.5d0 * (term1 + term2 + term3)
       else
          res = (0.0d0, 0.0d0)
       end if
    end if
       
  end function CME_t_rs_dt
  function CME_t_rs_dtdu(r,s,t,u) result(res)
  integer, intent(in) :: r, s, t, u
  complex*16          :: res, term1, term2, term3, term4, term5, term6
  complex*16          :: z
  integer             :: pn
  z = zeta_r(s)
  pn = pn_r(s)
  if(basis_type .eq. "s") then 
     if( (s .eq. t) .and. (s .eq. u)) then
        term1 = +pn * (pn - 1) * CME_rm_rs_dtdu(r,s,t,u,-2) 
        term2 = -2 * pn * z *    CME_rm_rs_dtdu(r,s,t,u,-1)
        term3 = +z * z  *        CME_rm_rs_dtdu(r,s,t,u,0)
        term4 = -4*pn   *        CME_rm_rs_dt(r,s,t,-1)
        term5 = +4 * z  *        CME_rm_rs_dt(r,s,t,0)
        term6 = 2       *        CME_rm_rs(r,s,0)
        res = -0.5d0 * (term1 + term2 + term3 + term4 + term5 + term6)
     else if( ((t .eq. r) .and. (u .eq. s)) .or. ((t .eq. s) .and. (u .eq. r)) )then
        term1 = +pn * (pn - 1) * CME_rm_rs_dtdu(r,s,t,u,-2) 
        term2 = -2 * pn * z *    CME_rm_rs_dtdu(r,s,t,u,-1)
        term3 = +z * z  *        CME_rm_rs_dtdu(r,s,t,u,0)
        term4 = -2 * pn *        CME_rm_rs_dt(r,s,r,-1)
        term5 = +2 * z  *        CME_rm_rs_dt(r,s,r,0)
        res = -0.5d0 * (term1 + term2 + term3 + term4 + term5)
     else if( (r .eq. t) .and. (r .eq. u) ) then
        term1 = +pn * (pn - 1) * CME_rm_rs_dtdu(r,s,t,u,-2) 
        term2 = -2 * pn * z *    CME_rm_rs_dtdu(r,s,t,u,-1)
        term3 = +z * z  *        CME_rm_rs_dtdu(r,s,t,u,0)
        res = -0.5d0 * (term1 + term2 + term3)
     else
        res = (0.0d0, 0.0d0)
     end if
  else
     if( (s .eq. t) .and. (s .eq. u) ) then
        term1 = +pn * (pn - 1) * CME_rm_rs_dtdu(r,s,t,u,-2) 
        term2 = -2*z *(2*pn+1) * CME_rm_rs_dtdu(r,s,t,u,0)
        term3 = +4 * z * z  *    CME_rm_rs_dtdu(r,s,t,u,2)
        term4 = -4 * (2*pn+1)  * CME_rm_rs_dt(r,s,t,0)
        term5 = +16 * z  *       CME_rm_rs_dt(r,s,t,2)
        term6 = 8       *        CME_rm_rs(r,s,2)
        res = -0.5d0 * (term1 + term2 + term3 + term4 + term5 + term6)
     else if( ((t .eq. r) .and. (u .eq. s)) .or. ((t .eq. s) .and. (u .eq. r)) )then
        term1 = +pn * (pn - 1) * CME_rm_rs_dtdu(r,s,t,u,-2) 
        term2 = -2*z * (2*pn+1)* CME_rm_rs_dtdu(r,s,t,u,0)
        term3 = +4*z*z  *        CME_rm_rs_dtdu(r,s,t,u,2)
        term4 = -2*(2*pn+1) *    CME_rm_rs_dt(r,s,r,0)
        term5 = +8 * z  *        CME_rm_rs_dt(r,s,r,2)
        res = -0.5d0 * (term1 + term2 + term3 + term4 + term5)
     else if( (r .eq. t) .and. (r .eq. u) ) then
        term1 = +pn * (pn - 1) * CME_rm_rs_dtdu(r,s,t,u,-2) 
        term2 = -2*z * (2*pn+1)* CME_rm_rs_dtdu(r,s,t,u,0)
        term3 = +4*z*z  *        CME_rm_rs_dtdu(r,s,t,u,2)
        res = -0.5d0 * (term1 + term2 + term3)
     else
        res = (0.0d0, 0.0d0)
     end if
  end if
end function CME_t_rs_dtdu
  !----------other--------------------------------------------------
  function CME_get_power_of_exp() result(res)
    integer res
    if(basis_type .eq. "s") then
       res = 1
    else if(basis_type .eq. "g") then
       res = 2
    else
       write(*,*) "bad basis_type in CME_get_power_of_exp"
       stop
    end if
  end function CME_get_power_of_exp
  !----------get accessor--------------------------------------------
  function CME_get_alpha() result(res)
    complex*16              :: res, ctmp
    integer                 :: r
    ctmp = (0.0d0, 0.0d0)
    do r = 1, basis_num
       ctmp = ctmp + c_r(r) * m_r(r)
    end do
    res = ctmp
  end function CME_get_alpha
  function CME_get_grad() result(res)
    complex*16       :: ctmp1, ctmp2, res(basis_num)
    integer          :: r, s, t
    do t = 1, basis_num
       ctmp1 = (0.0d0, 0.0d0)
       ctmp2 = (0.0d0, 0.0d0)
       do r = 1, basis_num
          ctmp2 = ctmp2 + m_r_dt(r,t) * c_r(r)
          do s = 1, basis_num
             ctmp1 = ctmp1 + c_r(r) * hmes_rs_dt(r,s,t) * c_r(s)
          end do
       end do
       res(t) = 2.0d0 * ctmp2 - ctmp1
    end do
  end function CME_get_grad
  function CME_get_hess() result(res)
    use math
    complex*16   :: res(basis_num, basis_num), tmpvec(basis_num)
    complex*16   :: res1, res2, res3, res4, res5, res6, tmp
    integer      :: i, t, u
    do t = 1, basis_num
       do u = 1, basis_num
          
          tmp = (0.0d0, 0.0d0)
          do i = 1, basis_num
             tmp = tmp + 2.0d0 * m_r_dtdu(i,t,u) *  c_r(i)
          enddo
          res1 = tmp
          
!          tmpvec = matmul(g_rs(:,:), m_r_dt(:,u))
          tmpvec = MATH_complex_a_inverse_b(hmes_rs(:,:), m_r_dt(:,u))
          tmp = (0.0d0,0.0d0)
          do i = 1, basis_num
             tmp = tmp + 2.0d0 * m_r_dt(i,t) * tmpvec(i)
          end do
          res2 = tmp
          
          tmpvec = matmul(hmes_rs_dt(:,:,u),c_r(:))
          
          !          tmpvec = matmul(g_rs(:,:), tmpvec(:))
          tmpvec = MATH_complex_a_inverse_b( &
               hmes_rs(:,:), tmpvec(:))
          tmp = (0.0d0, 0.0d0)
          do i = 1, basis_num
             tmp = tmp -2.0d0 *  m_r_dt(i,t) * tmpvec(i)
          enddo
          res3   = tmp
          
          tmpvec = matmul(hmes_rs_dt(:,:,t), c_r(:))
          !          tmpvec = matmul(g_rs(:,:), tmpvec(:))
          tmpvec = MATH_complex_a_inverse_b(&
               hmes_rs(:,:), tmpvec(:))
          tmp = (0.0d0, 0.0d0)
          do i = 1, basis_num
             tmp = tmp -2.0d0 * m_r_dt(i,u) * tmpvec(i)
          enddo
          res4 = tmp
          
          tmpvec = matmul(hmes_rs_dt(:,:,t),c_r(:))
          !          tmpvec = matmul(g_rs, tmpvec)
          tmpvec = MATH_complex_a_inverse_b( &
               hmes_rs(:,:), tmpvec(:))
          tmpvec = matmul(hmes_rs_dt(:,:,u), tmpvec)
          tmp = 0.0d0
          do i = 1, basis_num
             tmp = tmp + 2.0d0 * c_r(i) * tmpvec(i)
          enddo
          res5 = tmp
          tmpvec = matmul(hmes_rs_dtdu(:,:,t,u), c_r)
          tmp = (0.0d0, 0.0d0)
          do i = 1, basis_num
             tmp = tmp + c_r(i) * tmpvec(i)
          enddo
          res6 = -tmp
          res(t,u) = res1 + res2 + res3 + res4 + res5 + res6
       end do
    end do
  end function CME_get_hess
  function CME_get_c_r() result(res)
    complex*16  :: res(basis_num)
    res = c_r
  end function CME_get_c_r
  function CME_get_n_r() result(res)
    complex*16 :: res(basis_num)
    res(:) = n_r(:)
  end function CME_get_n_r
  function CME_get_m_r() result(res)
    complex*16 :: res(basis_num)
    res(:) = m_r(:)
  end function CME_get_m_r
  function CME_get_m_r_dt() result(res)
    complex*16 :: res(basis_num, basis_num)
    res(:,:) = m_r_dt(:,:)
  end function CME_get_m_r_dt
  function CME_get_m_r_dtdu() result(res)
    complex*16 :: res(basis_num, basis_num, basis_num)
    res(:,:,:) = m_r_dtdu(:,:,:)
  end function CME_get_m_r_dtdu
  function CME_get_hmes_rs() result(res)
    complex*16 :: res(basis_num, basis_num)
    res(:,:) = hmes_rs(:,:)
  end function CME_get_hmes_rs
  function CME_get_hmes_rs_dt() result(res)
    complex*16 :: res(basis_num, basis_num, basis_num)
    res(:,:,:) = hmes_rs_dt(:,:,:)
  end function CME_get_hmes_rs_dt
  function CME_get_hmes_rs_dtdu() result(res)
    complex*16 :: res(basis_num, basis_num, basis_num,basis_num)
    res(:,:,:,:) = hmes_rs_dtdu(:,:,:,:)
  end function CME_get_hmes_rs_dtdu
  function CME_get_s_rs() result(res)
    complex*16  :: res(basis_num, basis_num)
    res(:,:) = s_rs(:,:)
  end function CME_get_s_rs
  function CME_get_eigen_vals() result(res)
    use Class_CGM_complex_givens_method
    type(CGM_Object) :: cgm
    complex*16 :: res(basis_num)
    cgm = CGM_new(h_rs, s_rs)
    call CGM_solve_eigenvalue(cgm)
    write(*,*) "h in CME_get_eigen_vals"
    write(*,*) h_rs(1:basis_num,1:basis_num)
    write(*,*) "s in CME_get_eigen_vals"
    write(*,*) s_rs(1:basis_num,1:basis_num)
    res = CGM_get_eigenvalues(cgm)        
  end function CME_get_eigen_vals
  subroutine CME_get_eigen_val_and_coe(eigs, coes)
    use Class_CGM_complex_givens_method
    type(CGM_Object) :: cgm
    complex*16, intent(out) :: eigs(basis_num)
    complex*16, intent(out) :: coes(basis_num, basis_num)
    complex*16              :: right(basis_num, basis_num)
    complex*16              :: left(basis_num, basis_num)
    complex*16              :: ctmp
    integer i, j, k, l
    cgm = CGM_new(h_rs, s_rs)
    call CGM_solve_eigenvalue(cgm)
!    write(*,*) "h in CME_get_eigen_vals"
!    write(*,*) h_rs(1:basis_num,1:basis_num)
!    write(*,*) "s in CME_get_eigen_vals"
    !    write(*,*) s_rs(1:basis_num,1:basis_num)
    eigs(:) = CGM_get_eigenvalues(cgm)
    coes(:,:) = CGM_get_coefficients(cgm)
!    do i = 1, basis_num
!       do j = 1, basis_num
!          ctmp = (0.0d0, 0.0d0)
!          do k = 1, basis_num
!             ctmp = ctmp + h_rs(i,k) * coes(k,j)
!          end do
!          right(i,j) = ctmp
!       end do
!    end do
!    do i = 1, basis_num
!       do j = 1, basis_num
!          ctmp = (0.0d0, 0.0d0)
!          do k = 1, basis_num
!             ctmp = ctmp + eigs(i) * s_rs(i,k) * coes(k,j)
!          end do
!          left(i,j) = ctmp
!       end do
!    end do
!    write(*,*) "eig"
!    write(*,*) eigs
!    write(*,*) "h_rs"
!    write(*,*) h_rs
!    write(*,*) "s_rs"
!    write(*,*) s_rs
!    write(*,*) "coes"
!    write(*,*) coes
!    write(*,*) "right"
!    write(*,*) right
!    write(*,*) "left"
!    write(*,*) left
!          
  end subroutine CME_get_eigen_val_and_coe
  subroutine CME_get_overlap_mat_eigen_and_coe(eigs, coes)
    use Class_CGM_complex_givens_method
    type(CGM_Object) :: cgm
    complex*16, intent(out) :: eigs(basis_num)
    complex*16, intent(out) :: coes(basis_num, basis_num)
    cgm = CGM_new(s_rs, s_rs)
    call CGM_solve_eigenvalue(cgm)
    eigs(:) = CGM_get_eigenvalues(cgm)
    coes(:,:) = CGM_get_coefficients(cgm)
  end subroutine CME_get_overlap_mat_eigen_and_coe
  !----------debug-------------------------------------------------
  subroutine CME_print
    complex*16, allocatable  :: grad(:), hess(:,:)
    complex*16               :: alpha
    write(*,*) "--------------CME print field data---------------"
    write(*,*) "basis_num : ", basis_num
    write(*,*) "zeta_r :"
    write(*,*) zeta_r
    write(*,*) "pn_r :"
    write(*,*) pn_r
    write(*,*) "c_r :"
    write(*,*) c_r
    write(*,*) "n_r"
    write(*,*) n_r(:)           ! normalization constant
    write(*,*) "nr_r"
    write(*,*) nr_r(:)          ! n_r' / n_r
    write(*,*) "nrr_r"
    write(*,*) nrr_r(:)         ! n_r'' / n_r
    write(*,*)  "m_r"
    write(*,*)  m_r(:)           ! (chi_r | mu psi_0)
    write(*,*)  "m_r_dt"
    write(*,*)  m_r_dt(:,:)      ! d/d(zeta_t) m_r
    write(*,*)  "m_r_dtdu"
    write(*,*)  m_r_dtdu(:,:,:)  ! d/d(zeta_t zeta_u) m_r
    write (*,*) "s_rs"
    write (*,*) s_rs(:,:)
    write (*,*) "h_rs"
    write (*,*) h_rs(:,:)
    write(*,*)  "hmes_rs"
    write(*,*)  hmes_rs(:,:)     ! (chi_r | T + V | chi_s)
    write(*,*)  "hmes_rs_dt"
    write(*,*)  hmes_rs_dt(:,:,:)
    write(*,*)  "hmes_rs_dtdu"
    write(*,*)  hmes_rs_dtdu(:,:,:,:)
    write(*,*)  "g_rs"
    write(*,*)  g_rs(:,:)        ! Green's Function
    allocate(grad(basis_num))
    allocate(hess(basis_num, basis_num))
    alpha = CME_get_alpha()
    grad = CME_get_grad()
    hess = CME_get_hess()
    write(*,*) "alpha"
    write(*,*) alpha
    write(*,*) "grad"
    write(*,*) grad
    write(*,*) "hess"
    write(*,*) hess
    write(*,*) "----------------------------------------------"
  end subroutine CME_print
  subroutine CME_testcase_1basis
    use class_CHD_channel_and_dipole
    complex*16 :: zeta1_r(1)
    integer    :: pn1_r(1), channel1
    integer  :: form1
    real*8     :: ene1
    zeta1_r(1) = (1.0d0, -0.1d0)
    pn1_r(1)   = 2
    channel1 = 1
    form1 = CHD_DP_length
    ene1 = 0.1d0
    call CME_new(1,"s")
    call CME_set_pn_at_n(pn1_r(1), 1)
    call CME_set_zeta_at_n(zeta1_r(1), 1)
    call CME_calc_matrix_elements(channel1, form1, ene1)
    call CME_print
    write(*,*) "alpha"
    write(*,*) CME_get_alpha()
    write(*,*) "grad"
    write(*,*) CME_get_grad()
    write(*,*) "hess"
    write(*,*) CME_get_hess()
  end subroutine CME_testcase_1basis
  subroutine CME_testcase_sto_1basis
    use class_CHD_channel_and_dipole
    complex*16 :: zeta1_r(1)
    integer    :: pn1_r(1), channel1, form1
    real*8     :: ene1, c
    zeta1_r(1) = (1.0d0, -0.1d0)
    pn1_r(1)   = 2
    channel1 = 1
    call CME_set_pn_at_n(pn1_r(1), 1)
    call CME_set_zeta_at_n(zeta1_r(1), 1)
    c = dble(1 * 1 + 1)
    form1 = CHD_DP_length
    ene1 = 0.1d0
    call CME_new(1,"s")
    call CME_calc_matrix_elements(channel1, form1, ene1)
    !    call CME_print
    write(*,*) "before CME_rm"
    write(*,*) "CME_rm_rs(0)", CME_rm_rs(1,1,0)
    write(*,*) "CME_rm_rs_dt(-1)", CME_rm_rs_dt(1,1,1,-1)
    write(*,*) "CME_rm_rs_dtdu(0)", CME_rm_rs_dtdu(1,1,1,1,0)
    write(*,*) "m_r(1)", m_r(1)
    write(*,*) "m_r_dt(1,1)", m_r_dt(1,1)
    write(*,*) "m_r_dtdu(1,1,1)", m_r_dtdu(1,1,1)
    write(*,*) "t_rs(1)+L^2", CME_t_rs(1,1) + c / 2.0d0 * CME_rm_rs(1,1,-2)
    write(*,*) "t_rs_dt(1)+L^2", CME_t_rs_dt(1,1,1)  + c / 2.0d0 * CME_rm_rs_dt(1,1,1,-2)
    write(*,*) "t_rs_dtdu(1)+L^2", CME_t_rs_dtdu(1,1,1,1) + c / 2.0d0 * CME_rm_rs_dtdu(1,1,1,1,-2)

    write(*,*) "alpha"
    write(*,*) CME_get_alpha()
    write(*,*) "grad"
    write(*,*) CME_get_grad()
    write(*,*) "hess"
    write(*,*) CME_get_hess()
  end subroutine CME_testcase_sto_1basis
  subroutine CME_testcase_sto_2basis
    complex*16 :: zeta1_r(2)
    integer    :: pn1_r(2), channel1, form1
    real*8     :: ene1, c
    complex*16,allocatable :: mat4(:,:,:,:)
    integer    :: r, s, t, u, i
    write (*,*) "this is old code. in CME_testcase_sto_2basis"
    stop
    zeta1_r(1) = (1.0d0, -0.1d0)
    zeta1_r(2) = (0.1d0, -1.0d0)
    pn1_r(1)   = 2
    pn1_r(2)   = 2
    channel1 = 1
    c = dble(1 * 1 + 1)
    form1 = 1
    ene1 = 0.1d0
    call CME_new(2,"s")
    do i = 1, 2
       call CME_set_zeta_at_n(zeta1_r(i), i)
       call CME_set_pn_at_n(pn1_r(i), i)
    end do
    call CME_calc_matrix_elements(channel1, form1, ene1)
    !    call CME_print
    write(*,*) "before CME_rm"
    write(*,*) "CME_rm_rs(1,2,0)", CME_rm_rs(1,2,0)
    write(*,*) "CME_rm_rs_dt(1,2,1,-1)", CME_rm_rs_dt(1,2,1,-1)
    write(*,*) "CME_rm_rs_dtdu(1,2,1,1,0)", CME_rm_rs_dtdu(1,2,1,1,0)
    write(*,*) "m_r(1)", m_r(1)
    write(*,*) "m_r_dt(1,1)", m_r_dt(1,1)
    write(*,*) "m_r_dtdu(1,1,1)", m_r_dtdu(1,1,1)
    write(*,*) "t_rs(1)+L^2", CME_t_rs(1,2) + c / 2.0d0 * CME_rm_rs(1,2,-2)
    write(*,*) "t_rs_dt(1)+L^2", CME_t_rs_dt(1,2,1)  + c / 2.0d0 * CME_rm_rs_dt(1,2,1,-2)
    write(*,*) "t_rs_dtdu(1)+L^2", CME_t_rs_dtdu(1,2,1,1) + c / 2.0d0 * CME_rm_rs_dtdu(1,2,1,1,-2)

    allocate(mat4(basis_num, basis_num, basis_num, basis_num))
    do r = 1, basis_num
       do s = 1, basis_num
          do t = 1, basis_num
             do u = 1, basis_num
                mat4(r,s,t,u) = CME_t_rs_dtdu(r,s,t,u) + c / 2.0d0 * CME_rm_rs_dtdu(r,s,t,u,-2)
             end do
          end do
       end do
    end do
    write(*,*) "t_rs_dtdu"
    write(*,*) mat4
    write(*,*) "this", mat4(2,1,1,2)


    write(*,*) "alpha"
    write(*,*) CME_get_alpha()
    write(*,*) "grad"
    write(*,*) CME_get_grad()
    write(*,*) "hess"
    write(*,*) CME_get_hess()
  end subroutine CME_testcase_sto_2basis
  subroutine CME_testcase_gto_1basis
    complex*16 :: zeta1_r(1), a00, a10, dzeta, am0
    integer    :: pn1_r(1), channel1, form1
    real*8     :: ene1, eps
    write (*,*) "this is old code. in CME_testcase_gto_1basis"
    stop
    zeta1_r(1) = (1.0d0, -0.1d0)
    pn1_r(1)   = 2
    channel1 = 1
    form1 = 1
    ene1 = 0.1d0
    eps = 0.00001d0
    dzeta = zeta1_r(1) / 1000.0d0
    call CME_new(1, "g")
    call CME_set_pn_at_n(pn1_r(1), 1)
    call CME_set_zeta_at_n(zeta1_r(1), 1)
    call CME_calc_matrix_elements(channel1, form1, ene1)
    
!    call CME_print()
    
    call CME_check_value("c_r", c_r(1), (0.364d0, 0.0790228d0), eps)
    call CME_check_value("m_r_dtdu", m_r_dtdu(1,1,1), (0.347663d0,&
         & 0.0291151d0), eps)
    call CME_check_value("m_r_dt", m_r_dt(1,1), (-0.33725d0, &
         &-0.0344809d0), eps)
    call CME_check_value("m_r", m_r(1), (0.501435d0, 0.03381974d0),&
         & eps)
    call CME_check_value("hmes", hmes_rs(1,1), (1.33483d0, &
         &-0.196874d0), eps)
    call CME_check_value("hmes_dt", hmes_rs_dt(1,1,1), (1.97006d0, &
         &-0.0264312d0), eps)
    call CME_check_value("hmes_dtdu", hmes_rs_dtdu(1,1,1,1),&
         & (0.26103d0, 0.0393196d0 ), eps)
    call CME_check_value("t_rs_dtdu", CME_t_rs_dtdu(1,1,1,1)&
         &+CME_rm_rs_dtdu(1,1,1,1,-2)+0.001d0, (0.0d0, 0.0d0)+0.001d0&
         &, eps)
    call CME_check_value("v_rs_dtdu", CME_rm_rs_dtdu(1,1,1,1,-1), (&
         &-0.261039d0, -0.0393196d0), eps)
    call CME_check_value("s_rs_dtdu", CME_rm_rs_dtdu(1,1,1,1,0)&
         &+0.001d0, (0.0d0, 0.0d0)+0.001d0, eps)
    call CME_check_value("CME_rm_rs_dtdu", CME_rm_rs_dtdu(1,1,1,1,0)&
         &+0.001d0, (0.0d0, 0.0d0)+0.001d0, eps)
    call CME_check_value("CME_s_rs_dt", CME_rm_rs_dt(1,1,1,0)+0.001d0&
         &, (0.0d0, 0.0d0)+0.001d0, eps)
    call CME_check_value("CME_rm_rs(0)", CME_rm_rs(1,1,0), (1.0d0,&
         & 0.0d0), eps)
    write(*,*) "CME_rm_rs_dt(2)", CME_rm_rs_dt(1,1,1,2)
    write(*,*) "CME_rm_rs_dtdu(2)", CME_rm_rs_dtdu(1,1,1,1,2)    
    write(*,*) "t_rs_dt", CME_t_rs_dt(1,1,1)+CME_rm_rs_dt(1,1,1,-2)
    write(*,*) "CME_rm_rs_dt(1,1,1,-2)", CME_rm_rs_dt(1,1,1,-2)
    
    write(*,*) "alpha"
    write(*,*) CME_get_alpha()
    write(*,*) "grad"
    write(*,*) CME_get_grad()
    write(*,*) "hess"
    write(*,*) CME_get_hess()
    a00 = CME_get_alpha()

    call CME_set_zeta_at_n(zeta1_r(1) + dzeta, 1)
    call CME_calc_matrix_elements(channel1, form1,&
         & ene1)
    a10 = CME_get_alpha()
    
    call CME_set_zeta_at_n(zeta1_r(1) - dzeta, 1)
    call CME_calc_matrix_elements(channel1, form1,&
         & ene1)
    am0 = CME_get_alpha()
    write(*,*) "numerical grad"
    write(*,*) (a10 - am0) / (2*dzeta)
    write(*,*) "numerical hess"
    write(*,*) (a10 + am0 - 2*a00) / (dzeta*dzeta)
    
    write(*,*) "zeta = 0.189696-0.230554I"
    call CME_set_zeta_at_n((0.189696d0, -0.230554d0), 1)
    call CME_calc_matrix_elements(channel1, form1,&
         & ene1)
    write(*,*) "alpha"
    write(*,*) CME_get_alpha()
    write(*,*) "grad"
    write(*,*) CME_get_grad()
  end subroutine CME_testcase_gto_1basis
  subroutine CME_check_value(title, calc, exact, eps)
    character(*), intent(in) :: title
    complex*16, intent(in)   :: calc, exact
    real*8, intent(in)       :: eps
    write(*,*) title, calc, exact, abs((calc - exact)/exact) < eps
  end subroutine CME_check_value
  subroutine CME_testcase_3dkp_1basis
    use class_CHD_channel_and_dipole
    complex*16 :: zeta1_r(1) 
    integer    :: pn1_r(1) 
    integer    :: channel1 = CHD_CHA_3d_kp, form1 = CHD_DP_length
    real*8     :: ene1 = 0.7d0
    zeta1_r(1) = (1.0d0, -1.0d0)
    pn1_r(1) = 2
    call CME_new(1,"s")
    call CME_set_pn_at_n(pn1_r(1), 1)
    call CME_set_zeta_at_n(zeta1_r(1), 1)
    call CME_calc_matrix_elements(channel1, form1, ene1)
    call CME_print
    write(*,*) "mu"
    write(*,*) m_r(1)
    write(*,*) "hmes"
    write(*,*) hmes_rs(1,1)
  end subroutine CME_testcase_3dkp_1basis
  subroutine CME_testcase_custom
    use class_CHD_channel_and_dipole
    call CME_new(1, "s")
    call CME_set_custom_driven_term( (/(2.0d0, 0.0d0)/), (/2/), (/(1.0d0, -0.5d0)/), 1)
    call CME_set_pn_at_n(1, 1)
    call CME_set_zeta_at_n( (1.0d0, -0.5d0), 1)
    call CME_calc_matrix_elements(CHD_CHA_1s_kp, CHD_DP_length, 0.5d0)
    call CME_print()
  end subroutine CME_testcase_custom
end module complex_matrix_elements
