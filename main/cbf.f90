module class_CBF_complex_basis_function_h
  use class_CHD_channel_and_dipole
  use class_LCCF_linear_comb_complex_l2_functions
  use class_OPT_optimization
  use class_WKB_wkb_mathcing
  use class_CN_contium_number
  implicit none

  type CBF_Object
     type(CHD_Object)  :: channel_dipole
     type(LCCF_Object) :: basis_set
     type(OPT_Object)  :: opt
     type(WKB_Object)  :: wkb
     real*8            :: photo_freq_w
  end type CBF_Object
 
contains
  !==========Constructor and Destructor==========
  function CBF_new_from_read_without_energy() result(res)
    type(CBF_Object) :: res
    integer          :: num, order
    real*8           :: r0, r1
    res % channel_dipole = CHD_new_from_read()
    res % basis_set      = LCCF_new_from_read()
    num = LCCF_get_num(res % basis_set)
    res % opt            = OPT_new_from_read(num)
    write(*,*) "r_match, r_asym, order"
    read(*,*) r0, r1, order
    write(*,*) r0, r1, order
    res % wkb = WKB_new(r0, r1, order, &
         CHD_l_final_state(res % channel_dipole))
    res % photo_freq_w = 0.0d0
    
  end function CBF_new_from_read_without_energy
  subroutine CBF_delete(this)
    use complex_matrix_elements
    type(CBF_Object) :: this
    call LCCF_delete(this % basis_set)
!    call CME_delete
  end subroutine CBF_delete
  !==========Setter=============================
  subroutine CBF_set_photon_freq_w(this, w)
    type(CBF_Object), intent(inout)   :: this
    real*8, intent(in)                :: w
    this % photo_freq_w = w
  end subroutine CBF_set_photon_freq_w
  subroutine CBF_set_energy(this, ene)
    type(CBF_Object), intent(inout)   :: this
    real*8, intent(in)                :: ene
    real*8                            :: ene0
    ene0 = CHD_energy_initial_state(this % channel_dipole)
    this % photo_freq_w = ene - ene0 
  end subroutine CBF_set_energy
  !==========Getter=============================

  !==========freq dep polarization==============
  function CBF_calc_freq_dep_polarization(this) result(res)
    use complex_matrix_elements
    type(CBF_Object), intent(inout) :: this
    complex*16 :: res, zeta
    integer    :: num, type, pn, ch, di, i
    type(CL2F_Object) :: basis
    real*8            :: ene
    type(LCCF_Object)  :: mu0
    
!    write(*,*) "danger. in CBF_calc_freq_dep_polarization"
!    stop
    num  = LCCF_get_num(this % basis_set)
    type = CL2F_get_function_type(LCCF_get_CL2F_at_n(this % basis_set, 1))
    
    if(type .eq. CL2F_CSTO) then
       call CME_new(num, "s", this % channel_dipole % charge)
    else
       call CME_new(num, "g", this % channel_dipole % charge)
    end if
    do i = 1, LCCF_get_num(this % basis_set)
       basis = this % basis_set % cl2f_array(i)
       zeta = CL2F_get_orbital_exponent(basis)
       pn   = CL2F_get_principle_number(basis)
       call CME_set_zeta_at_n(zeta, i)
       call CME_set_pn_at_n(pn, i)
    end do
    if (this % channel_dipole % customQ) then
       mu0 = this % channel_dipole % dipole_initial_state
       call CME_set_custom_driven_term( &
            LCCF_get_coefficients(mu0), &
            LCCF_get_pns(mu0), &
            LCCF_get_ors(mu0), &
            CHD_l_final_state(this % channel_dipole))
    end if
    
    ch = this % channel_dipole % channel
    di = this % channel_dipole % dipole
    ene = CHD_energy_final_state(this % channel_dipole, this % photo_freq_w)
    
    call CME_calc_base_matrix_elements(ch, di, ene)
    res = CME_get_alpha()
    call CME_delete
  end function CBF_calc_freq_dep_polarization
  function CBF_calc_partial_cross_section(this) result(res)
    use math
    type(CBF_Object), intent(inout) :: this
    complex*16 :: fdp
    real*8     :: res, tmp
    fdp = CBF_calc_freq_dep_polarization(this)
    tmp = CHD_partial_cross_section_in_au_from_fdp(&
         this % channel_dipole, fdp, this % photo_freq_w)
    if (this % channel_dipole % dipole .eq. CHD_DP_length) then
       res = -tmp
    else
       res = tmp
    end if
    
  end function CBF_calc_partial_cross_section
  subroutine CBF_calc_freq_dep_polarization_val_grad_hess(this, val, grad, hess)
    use complex_matrix_elements
    type(CBF_Object), intent(inout) :: this
    complex*16, intent(inout) :: val
    complex*16, intent(inout) :: grad(:)
    complex*16, intent(inout) :: hess(:,:)
    complex*16 :: zeta
    integer    :: num, type, pn, ch, di, i
    type(CL2F_Object) :: basis
    real*8            :: ene
    write(*,*) "danger. in CBF_calc_freq_dep_polarization_val_grad_hess"
    stop
    num  = LCCF_get_num(this % basis_set)
    type = CL2F_get_function_type(LCCF_get_CL2F_at_n(this % basis_set, 1))
    if(type .eq. CL2F_CSTO) then
       call CME_new(num, "s", this % channel_dipole % charge)
    else
       call CME_new(num, "g", this % channel_dipole % charge)
    end if
    do i = 1, LCCF_get_num(this % basis_set)
       basis = this % basis_set % cl2f_array(i)
       zeta = CL2F_get_orbital_exponent(basis)
       pn   = CL2F_get_principle_number(basis)
       call CME_set_zeta_at_n(zeta, i)
       call CME_set_pn_at_n(pn, i)
    end do
    ch = this % channel_dipole % channel
    di = this % channel_dipole % dipole
    ene = CHD_energy_final_state(this % channel_dipole, this % photo_freq_w)
    call CME_calc_matrix_elements(ch, di, ene)
    val  = CME_get_alpha()
    grad(:) = CME_get_grad()
    hess(:,:) = CME_get_hess()
    call CME_delete()
  end subroutine CBF_calc_freq_dep_polarization_val_grad_hess
  subroutine CBF_calc_eigenval_coes(this, eigen_vals, eigen_coes)
    use complex_matrix_elements
    type(CBF_Object), intent(inout) :: this
    complex*16, intent(out)   :: eigen_vals(:)
    complex*16, intent(out)   :: eigen_coes(:,:)
    complex*16 :: zeta
    integer    :: num, type, pn, ch, di, i
    type(CL2F_Object) :: basis
    real*8            :: ene
    write(*,*) "danger. in CBF_calc_eigenval_coes"
    stop
    num  = LCCF_get_num(this % basis_set)
    type = CL2F_get_function_type(LCCF_get_CL2F_at_n(this % basis_set, 1))
    if(type .eq. CL2F_CSTO) then
       call CME_new(num, "s", this % channel_dipole % charge)
    else
       call CME_new(num, "g", this % channel_dipole % charge)
    end if
    do i = 1, LCCF_get_num(this % basis_set)
       basis = this % basis_set % cl2f_array(i)
       zeta = CL2F_get_orbital_exponent(basis)
       pn   = CL2F_get_principle_number(basis)
       call CME_set_zeta_at_n(zeta, i)
       call CME_set_pn_at_n(pn, i)
    end do

    ch = this % channel_dipole % channel    
    di = this % channel_dipole % dipole
    ene = CHD_energy_final_state(this % channel_dipole, this % photo_freq_w)
    call CME_calc_base_matrix_elements(ch, di, ene)
    call CME_get_eigen_val_and_coe(eigen_vals, eigen_coes)
    call CME_delete()
  end subroutine CBF_calc_eigenval_coes
  function CBF_calc_regular_radial_transition_dipole_moment(this) result(res)
    type(CBF_Object) :: this
    complex*16 :: fdp
    real*8     :: res
    fdp = CBF_calc_freq_dep_polarization(this)
    res = CHD_regular_radial_transition_moment_from_fdp( &
         this % channel_dipole, fdp, this % photo_freq_w)
  end function CBF_calc_regular_radial_transition_dipole_moment
  !==========phase shift========================
  function CBF_calc_phase_shift_WKB(this) result(res)
    use math
    type(CBF_Object), intent(inout) :: this
    real*8             :: psi, psid, res, energy
    integer            :: form

    psi  = aimag(LCCF_value_at_r(this % basis_set, this % wkb % r_match))
    psid = aimag(LCCF_differentiate_at_r(this % basis_set, this % wkb % r_match))

    energy = CHD_energy_final_state( &
         this % channel_dipole, this % photo_freq_w)
    form = this % channel_dipole % dipole
    
    call WKB_set_energy(this % wkb, energy)
    call WKB_match(this % wkb, psi, psid)

    select case (form)
    case (CHD_DP_length)
       res = WKB_phase_shift(this % wkb, .true.)
    case (CHD_DP_velocity)
       res = WKB_phase_shift(this % wkb, .false.)
    case (CHD_DP_accelate)
       res = WKB_phase_shift(this % wkb, .true.)
    case default
       write (*,*) "bad form. in CBF_calc_phase_shift_WKB"
       stop
    end select

  end function CBF_calc_phase_shift_WKB
  function CBF_calc_asymmetry_parameter(cbf_p, cbf_m) result(res)
    type(CBF_Object), intent(inout) :: cbf_m, cbf_p
    real*8             :: res
    complex*16         :: fdp_p, fdp_m
!    fdp_p = CBF_calc_freq_dep_polarization(cbf_p)
!    fdp_m = CBF_calc_freq_dep_polarization(cbf_m)
!    res = CHD_asymmetry_parameter(&
!         cbf_p % channel_dipole, cbf_m % channel_dipole, &
    !         fdp_p, fdp_m, cbf_p % photo_freq_w)
    write(*,*) "mada junbi siteni. in CBF_calc_asymmetry_parameter"
    stop
    res = 0.0d0
  end function CBF_calc_asymmetry_parameter
  !==========cross section and phase shift======
  subroutine CBF_calc_cross_section_and_phase_shift_WKB(this, cross_section, phase_shift)
    use math
    type(CBF_Object), intent(inout) :: this

    real*8, intent(out)             :: cross_section, phase_shift
    type(CHD_Object)                :: tmp_chd
    type(LCCF_Object)               :: dipole_initial_state
    real*8                          :: amplitude
    real*8                          :: dipole, k
    complex*16                      :: alpha

    ! construct wkb and calc phase shift and amplitudelitude
    phase_shift = CBF_calc_phase_shift_WKB(this)
    amplitude = WKB_amplitude(this % wkb)
    
    ! construct physical channel_dipole and Frank-Condon state
    tmp_chd = CHD_new( &
         this % channel_dipole % channel, &
         this % channel_dipole % dipole)
    if(this % channel_dipole % customQ) then
       dipole_initial_state = this % channel_dipole &
            % dipole_initial_state
    else
       dipole_initial_state = CHD_get_dipole_init_state(tmp_chd)
    end if

    ! calculate dipole moment and freq-dep polarizability(real part is negrected)
    k = CHD_k_final_state(tmp_chd, this % photo_freq_w)
    dipole = aimag(LCCF_integrate_with_lccf( &
         dipole_initial_state, this % basis_set))
    alpha = Cmplx(0.0d0, (dipole / amplitude) ** 2 * 2.0d0 / k)

    ! calculate cross section
    cross_section = abs(CHD_partial_cross_section_in_au_from_fdp( &
         tmp_chd, &
         alpha, &
         this % photo_freq_w))
    
  end subroutine CBF_calc_cross_section_and_phase_shift_WKB
  !==========Optimization=======================
  function CBF_optimize_orbital_exponent(this) result(res)
    use complex_matrix_elements
    type(CBF_Object), intent(inout) :: this
    complex*16 :: grad(this % basis_set % num)
    complex*16 :: hess(this % basis_set % num, this % basis_set % num)
    complex*16 :: zeta, zeta_array(this % basis_set % num), freq_dep_polar
    integer    :: num, type, pn, ch, di, i
    type(CL2F_Object) :: basis
    real*8            :: ene
    logical           :: res, convQ
    complex*16        :: cs(this % basis_set % num)
    type(LCCF_Object)  :: mu0
    
    num = LCCF_get_num(this % basis_set)
    type = CL2F_get_function_type(LCCF_get_CL2F_at_n(this % basis_set, 1))
    
    ! initialize
    if(type .eq. CL2F_CSTO) then
       call CME_new(num, "s", this % channel_dipole % charge)
    else if(type .eq. CL2F_CGTO) then
       call CME_new(num, "g", this % channel_dipole % charge)       
    else
       write(*,*) "bad basis type. in CBF_optimize_orbital_exponent" 
       stop
    end if
    do i = 1, num
       basis = this % basis_set % cl2f_array(i)
       zeta = CL2F_get_orbital_exponent(basis)
       zeta_array(i) = zeta
       pn   = CL2F_get_principle_number(basis)
       call CME_set_zeta_at_n(zeta, i)
       call CME_set_pn_at_n(pn, i)
    end do

    if(this % channel_dipole % customQ) then
       mu0 = this % channel_dipole % dipole_initial_state
       call CME_set_custom_driven_term(&
            LCCF_get_coefficients(mu0), &
            LCCF_get_pns(mu0), &
            LCCF_get_ors(mu0),&
            CHD_l_final_state(this % channel_dipole))
    end if
    
    ch = this % channel_dipole % channel
    di = this % channel_dipole % dipole
    ene = CHD_energy_final_state(this % channel_dipole, this % photo_freq_w)

    call CME_calc_matrix_elements(ch, di, ene)
    grad(:) = CME_get_grad()
    hess(:,:) = CME_get_hess()

    ! optimization loop 
    do while(OPT_update_and_optimize_complex(&
         this % opt, convQ, zeta_array, grad, hess))
       do i = 1, num
          call CME_set_zeta_at_n(zeta_array(i), i)
       end do
       call CME_calc_matrix_elements(ch, di, ene)
       grad(:) = CME_get_grad()
       hess(:,:) = CME_get_hess()       
    end do

    ! set optimized data if convergence
    cs = CME_get_c_r()
    if(convQ) then
       do i = 1, num
          call LCCF_set_oe_at_n(this % basis_set, zeta_array(i), i)
          call LCCF_set_coefficient_at_n(this % basis_set, cs(i), i)
       end do
    end if

    ! Show
!    call CME_print()
    
    ! finalize
    call CME_delete()
    call OPT_reset_counter(this % opt)
    res = convQ
    
  end function CBF_optimize_orbital_exponent
  !==========print===============================
  subroutine CBF_display(this)
    type(CBF_Object), intent(in) :: this
    write(*,*) ""
    write(*,*) "*********CBF display***********"
    call LCCF_display(this % basis_set)
    call CHD_display(this % channel_dipole)
    call OPT_display(this % opt)
    call WKB_display(this % wkb)
    write(*,*) "w:", this % photo_freq_w
    write(*,*) "*******************************"
  end subroutine CBF_display
  subroutine CBF_display2(this)
    use class_CL2F_complex_l2_function
    type(CBF_Object), intent(inout) :: this
    integer i, num
    complex*16 zs(this % basis_set % num)
    complex*16 cs(this % basis_set % num)
    complex*16 ns(this % basis_set % num)
    type(CL2F_Object) :: basis

    num = LCCF_get_num(this % basis_set)

    do i = 1, num
       basis = LCCF_get_CL2F_at_n(this % basis_set, i)
       zs(i) = CL2F_get_orbital_exponent(basis)
       ns(i) = CL2F_get_coefficient(basis)
       cs(i) = LCCF_get_coefficient_at_n(this % basis_set, i)
    end do

    do i = 1, num
       write(*,'(A,I1,A,f15.10,f15.10)') "zeta_", i,":", real(zs(i)), aimag(zs(i))
    end do
    
    do i = 1, num
       write(*,'(A,I1,A,f15.10,f15.10)') "c_", i, ":", real(cs(i)), aimag(cs(i))
    end do
    
    do i = 1, num
       write(*,'(A,I1,A,f15.10,f15.10)') "N_", i, ":", real(ns(i)), aimag(ns(i))
    end do
  end subroutine CBF_display2
  subroutine CBF_print_calc_res_cross_section(this, represent_momentQ)
    use mod_PC_physical_constant
    type(CBF_Object), intent(inout) :: this
    logical, intent(in), optional   :: represent_momentQ
    real*8                          :: cs, st, re, rm
    complex*16                      :: fdp
    fdp = CBF_calc_freq_dep_polarization(this)
    cs =  CHD_partial_cross_section_in_au_from_fdp(&
         this % channel_dipole, fdp, this % photo_freq_w) &
         * PC_automb

    st = CHD_partital_cross_section_in_au(&
         this % channel_dipole, this % photo_freq_w) * PC_automb
    re = (cs - st) / st * 100
    write(*,*) "freq_dep_polar: ", real(fdp), "," , aimag(fdp)
    write(*,*) "Cross section(Mb): ", cs
    write(*,*) "Exact(Mb): ", st
    write(*,*) "R.E.(%): ", re
    if(present(represent_momentQ)) then
       if(represent_momentQ) then
          rm =  CHD_regular_radial_transition_moment_from_fdp( &
               this % channel_dipole, fdp, this % photo_freq_w)
          write(*,*) "radial moment(a.u.):", rm
       end if
    end if
  end subroutine CBF_print_calc_res_cross_section
  subroutine CBF_print_calc_res_from_wkb(this)
    use mod_PC_physical_constant
    type(CBF_Object), intent(inout) :: this
    real*8                          :: cs, st, re, ps, cs2, re2

    write (*,*) "this is CBF_print_calc_res_from_wkb"
    
    call CBF_calc_cross_section_and_phase_shift_WKB(this, cs, ps)
    st = CHD_partital_cross_section_in_au(&
         this % channel_dipole, this % photo_freq_w)
    st = st * PC_automb
    cs = cs * PC_automb
    re = (cs - st) / st * 100
    
    cs2= CBF_calc_partial_cross_section(this) * PC_automb
    re2= (cs2 - st) / st * 100

    write (*,*) "calculation result from matched WKB amplitude and phase"
    write (*,*) "Cross section dipole(Mb): ", cs
    write (*,*) "RE_dipole(%): ", re
    write (*,*) "Cross section polar(Mb): ", cs2
    write (*,*) "RE_polar(%): ", re2
    write (*,*) "Exact(Mb): ", st
    write (*,*) "phase_shift: ", ps
    
  end subroutine CBF_print_calc_res_from_wkb
  subroutine CBF_print_calc_res_phase(this)
    type(CBF_Object), intent(inout) :: this
    real*8  :: cps
    cps = CBF_calc_phase_shift_WKB(this)
    write(*,*) "phase_shift: ", cps
  end subroutine CBF_print_calc_res_phase
  subroutine CBF_print_calc_res_asymmetry_parameter(cbf_p, cbf_m)
    type(CBF_Object) :: cbf_p, cbf_m
    real*8     :: beta, phase_p, phase_m, exact, re
    complex*16 :: fdp_p, fdp_m
    fdp_p = CBF_calc_freq_dep_polarization(cbf_p)
    fdp_m = CBF_calc_freq_dep_polarization(cbf_m)
    phase_p = CBF_calc_phase_shift_WKB(cbf_p)
    phase_m = CBF_calc_phase_shift_WKB(cbf_m)
    
    beta = CHD_asymmetry_parameter(&
         cbf_p % channel_dipole, cbf_m % channel_dipole, &
         fdp_p, fdp_m, phase_p, phase_m, cbf_p % photo_freq_w)
    exact = CHD_asymmetry_parameter_exact( &
         cbf_p % channel_dipole, cbf_m % channel_dipole, cbf_p % photo_freq_w)
    re = (beta - exact) / exact * 100
    write(*,*) "phase_plus: ", phase_p
    write(*,*) "fdp_plus: ", fdp_p
    write(*,*) "phase_minus: ", phase_m
    write(*,*) "fdp_minus: ", fdp_m
    write(*,*) "beta: ", beta
    write(*,*) "exact:", exact
    write(*,*) "R.E.(%):", re
  end subroutine CBF_print_calc_res_asymmetry_parameter
  subroutine CBF_print_calc_eigen_probrem(this)
    type(CBF_Object) :: this
    complex*16       :: ev(this % basis_set % num) ! eigen values
    complex*16       :: ec(this % basis_set % num, this % basis_set % num) ! eigen coefficient
    integer :: i, num

    num = LCCF_get_num(this % basis_set)
    call CBF_calc_eigenval_coes(this, ev, ec)

    do i = 1, num
       write (*,'(A,I1,A,f15.10,f15.10)') "E_", i, ":", real(ev(i)), aimag(ev(i))       
    end do
  end subroutine CBF_print_calc_eigen_probrem
  !==========write file=========================
  subroutine CBF_write_file_use_cn (this, filename, r_range)
    type(CBF_Object)         :: this
    character(*), intent(in) :: filename
    type(CN_Object)          :: r_range
    integer                  :: fileno = 133
    real*8                   :: r
    integer                  :: i
    complex*16               :: psi
    
    open(fileno, file = filename)
    write(fileno, *) "# photo_freq_w:", this % photo_freq_w
    write(fileno, *) "# r, real(psi), imag(psi)"
    write (fileno,*) "# amplitude:", WKB_amplitude(this % wkb)
    call CN_reset_counter(r_range)
    do while(CN_set_next(r_range, r, i))
       psi = LCCF_value_at_r(this % basis_set, r)
       write (fileno,*) r, real(psi), aimag(psi)
    end do
    close(fileno)
    
  end subroutine CBF_write_file_use_cn
  subroutine CBF_write_file_use_writer(this, filename, writer)
    use class_FW_function_writer
    type(CBF_Object)         :: this
    type(FW_Object)          :: writer
    character(*), intent(in) :: filename
    real*8                   :: vals(2), r
    integer                  :: fileno = 133
    complex*16               :: psi
    
    call FW_open(writer, filename, fileno)
    write(fileno, *) "# photo_freq_w:", this % photo_freq_w
    write(fileno, *) "# r, real(psi), imag(psi)"
    write (fileno,*) "# amplitude:", WKB_amplitude(this % wkb)
    do while (FW_next(writer, r, vals))
       psi = LCCF_value_at_r(this % basis_set, r)
       vals(1) = real(psi)
       vals(2) = aimag(psi)
    end do
    call FW_close(writer)
    
  end subroutine CBF_write_file_use_writer
  subroutine CBF_write_file(this, filename, r0, r1, dr, in_with_wkbQ)
    type(CBF_Object)         :: this
    character(*), intent(in) :: filename
    real*8, intent(in)       :: r0, r1, dr
    logical, optional, intent(in) :: in_with_wkbQ
    logical                       :: with_wkbQ
    integer, parameter       :: fileno = 33
    real*8                   :: r
    complex*16               :: psi, wkb
    integer                  :: num, i

    if(present(in_with_wkbQ)) then
       with_wkbQ = in_with_wkbQ
    else
       with_wkbQ = .false.
    end if
    
    open(unit = fileno, file = filename, status = "replace")
    write(fileno, *) "# photo_freq_w:", this % photo_freq_w
    write(fileno, *) "# r, real(psi), imag(psi)"
    write (fileno,*) "# amp:", this % wkb % amp
    
    num = int((r1 - r0) / dr)

    do i = 0, num
       r = r0 + i * dr
       psi = LCCF_value_at_r(this % basis_set, r)

       if(with_wkbQ) then
          wkb = WKB_psi(this % wkb, r)
          write(fileno,*) r, real(psi), aimag(psi) &
               , real(wkb), aimag(wkb)
       else
          write(fileno,*) r, real(psi), aimag(psi)
       end if
       
    end do
    close(fileno)
  end subroutine CBF_write_file
  subroutine CBF_write_one_basis(this, filename_base, r0, r1, dr)
    use class_CL2F_complex_l2_function
    type(CBF_Object)         :: this
    character(*), intent(in) :: filename_base
    real*8, intent(in)       :: r0, r1, dr
    integer, parameter       :: fileno = 33
    real*8                   :: r
    complex*16               :: psi
    integer                  :: num, i, j
    type(CL2F_Object)        :: basis
    character(20)            :: filename
    num = int((r1 - r0) / dr)
    do j = 1, LCCF_get_num(this % basis_set)
       basis = LCCF_get_CL2F_at_n(this % basis_set, j)
       filename = filename_base // achar(j) // ".dat"
       open(unit = fileno, file = filename, status = "replace")
       write(fileno, *) "# photo_freq_w:", this % photo_freq_w
       do i = 0, num
          r = r0 + i * dr
          psi = CL2F_value_at_r(basis, r)
          write(fileno, *) r, real(psi), aimag(psi)
       end do
       close(fileno)
    end do
  end subroutine CBF_write_one_basis
  subroutine CBF_write_grad_and_hess(this, filename_base)
    use complex_matrix_elements
    type(CBF_Object)         :: this
    character(*), intent(in) :: filename_base
    integer                  :: num, type, i, j
    type(CL2F_Object)        :: basis
    complex*16               :: zeta
    integer                  :: pn
    complex*16 :: hess(this % basis_set % num, this % basis_set % num)
    complex*16 :: grad(this % basis_set % num)
    type(LCCF_Object)  :: mu0
    integer            :: fileno = 133

    num = LCCF_get_num(this % basis_set)
    type = CL2F_get_function_type(LCCF_get_CL2F_at_n(this % basis_set, 1))
    
    ! initialize
    if(type .eq. CL2F_CSTO) then
       call CME_new(num, "s", this % channel_dipole % charge)
    else if(type .eq. CL2F_CGTO) then
       call CME_new(num, "g", this % channel_dipole % charge)       
    else
       write(*,*) "bad basis type. in CBF_optimize_orbital_exponent" 
       stop
    end if
    do i = 1, num
       basis = this % basis_set % cl2f_array(i)
       call CME_set_zeta_at_n(CL2F_get_orbital_exponent(basis), i)
       call CME_set_pn_at_n(CL2F_get_principle_number(basis), i)
    end do

    if(this % channel_dipole % customQ) then
       mu0 = this % channel_dipole % dipole_initial_state
       call CME_set_custom_driven_term(&
            LCCF_get_coefficients(mu0), &
            LCCF_get_pns(mu0), &
            LCCF_get_ors(mu0), &
            CHD_l_final_state(this % channel_dipole))
    end if

    call CME_calc_matrix_elements(&
         this % channel_dipole % channel, &
         this % channel_dipole % dipole, &
         CHD_energy_final_state(this % channel_dipole, this % photo_freq_w))

    grad(:) = CME_get_grad()
    hess(:,:) = CME_get_hess()

    open(unit = fileno, file = filename_base // "_hess.d")
    do i = 1, num
       do j = 1, num
          write(fileno, *) i,",", j,",", real(hess(i,j)), ",", aimag(hess(i,j))
       end do
    end do
    close(fileno)

    open(unit = fileno, file = filename_base // "_grad.d")
    do i = 1, num
       if (i .eq. num) then
          write(fileno, fmt='(f10.7,",",f10.7)') &
               real(grad(i)), aimag(grad(i))
       else
          write(fileno, fmt='(f10.7,",",f10.7,",")', advance='no') &
               real(grad(i)), aimag(grad(i))
       end if
    end do
    close(fileno)
    
    ! finalize
    call CME_delete()
    
  end subroutine CBF_write_grad_and_hess
  subroutine CBF_write_matrix(this, filename)
    use complex_matrix_elements
    type(CBF_Object)         :: this
    character(*), intent(in) :: filename
    integer, parameter       :: fileno = 33
    integer                  :: i, j
    integer                  :: num, type, ch, di, pn
    real*8                   :: q, ene
    complex*16  :: m_r(this % basis_set % num), ele, zeta
    complex*16  :: hmes_r(this % basis_set % num, this % basis_set % num)
    complex*16  :: s_r(this % basis_set % num, this % basis_set % num)    
    type(CL2F_Object)        :: basis
    type(LCCF_Object)        :: mu0

    ! calculate matrix elements
    num = this % basis_set % num
    type = CL2F_get_function_type(LCCF_get_CL2F_at_n(&
         this % basis_set, 1))
    q = this % channel_dipole % charge

    if(type .eq. CL2F_CSTO) then
       call CME_new(num, "s", q)
    else
       call CME_new(num, "g", q)
    end if
    do i = 1, LCCF_get_num(this % basis_set)
       basis = this % basis_set % cl2f_array(i)
       zeta = CL2F_get_orbital_exponent(basis)
       pn   = CL2F_get_principle_number(basis)
       call CME_set_zeta_at_n(zeta, i)
       call CME_set_pn_at_n(pn, i)
    end do
    if (this % channel_dipole % customQ) then
       mu0 = this % channel_dipole % dipole_initial_state
       call CME_set_custom_driven_term( &
            LCCF_get_coefficients(mu0), &
            LCCF_get_pns(mu0), &
            LCCF_get_ors(mu0), &
            CHD_l_final_state(this % channel_dipole))
    end if
    ch = this % channel_dipole % channel
    di = this % channel_dipole % dipole
    ene = CHD_energy_final_state(this % channel_dipole, this % photo_freq_w)
    
    call CME_calc_base_matrix_elements(ch, di, ene)
    
    open(unit = fileno, file = filename, status = "replace")
    m_r(:)      = CME_get_m_r()
    hmes_r(:,:) = CME_get_hmes_rs()
    s_r(:,:)      = CME_get_s_rs()
    do i = 1, num
       ele = m_r(i)
       write (fileno,*) "m   ", 0, i, real(ele), aimag(ele)
    end do
    do i = 1, num
       do j = 1, num
          ele = hmes_r(i,j)
          write (fileno,*) "H-ES", i, j, real(ele), aimag(ele)
       end do
    end do
    do i = 1, num
       do j = 1, num
          ele = s_r(i,j)
          write (fileno,*) "S   ", i, j, real(ele), aimag(ele)
       end do
    end do    
    close(fileno)

    call CME_delete()
  end subroutine CBF_write_matrix
end module class_CBF_complex_basis_function_h



