module class_WKB_wkb_mathcing
  implicit none
  type WKB_Object
     integer :: order
     real*8  :: r_match
     real*8  :: r_asym
     logical :: exist_kQ
     logical :: calculate_doneQ
     integer :: l
     real*8  :: k
     real*8  :: amp
     real*8  :: in_delta

  end type WKB_Object
contains
  !=========Constructor==========
  function WKB_new(inr_match, inr_asym, inorder, inl, ink) result(res)
    real*8, intent(in)  :: inr_match, inr_asym
    integer, intent(in) :: inorder, inl
    real*8, optional, intent(in)  :: ink
    type(WKB_Object)    :: res
    res % order = inorder
    res % r_match = inr_match
    res % r_asym = inr_asym
    res % calculate_doneQ = .false.
    res % l = inl
    if(present(ink)) then
       res % k = ink
       res % exist_kQ = .true.
    else
       res % exist_kQ = .false.
    end if
    if((inorder > 1) .or. (inorder < 0)) then
       write(*,*) "bad order in WKB_new"
       stop
    end if
  end function WKB_new
  !=========Desutructor==========
  subroutine WKB_delete(this)
    type(WKB_Object)    :: this
    this % exist_kQ = .false.
    this % calculate_doneQ = .false.
  end subroutine WKB_delete
  !=========Setter===============
  subroutine WKB_set_energy(this, ene)
    type(WKB_Object)    :: this
    real*8, intent(in) :: ene
    this % k = sqrt(2 * ene)
    this % exist_kQ = .true.
  end subroutine WKB_set_energy
  !=========Display==============
  subroutine WKB_display(this)
    type(WKB_Object) :: this
    write(*,*) ""
    write(*,*) "----BEGIN WKB DISPLAY------"
    write(*,*) "order:", this % order
    write(*,*) "r match:", this % r_match
    write(*,*) "r asym:", this % r_asym
    write(*,*) "l:", this % l
    write(*,*) "k:", this % k
    write(*,*) "amp:", this % amp
    write(*,*) "delta:", this % in_delta
    write(*,*) "---------------------------"

  end subroutine WKB_display
  !=========Need calculated======
  function WKB_psi(this, r) result(res)
    type(WKB_Object)   :: this
    real*8, intent(in) :: r
    real*8             :: res, phase, z
    if(.not. this % calculate_doneQ) then
       write (*,*) "Pre calculation is nesesarry. in WKB_psi"
       stop
    end if
    phase = WKB_phi(this, r) - WKB_phi(this, this % r_match) + this % in_delta
    z = WKB_zeta(this, r)
    res = this % amp / sqrt(z) * sin(phase)

  end function WKB_psi
  function WKB_amplitude(this) result(res)
    type(WKB_Object)    :: this
    real*8              :: res
    if(this % calculate_doneQ) then
       res = this % amp / sqrt(this % k)
    else
       write (*,*) "pre calc is necessary. in WKB_amplitude"
       stop
    end if
  end function WKB_amplitude
  function WKB_phase_shift(this, is_plus) result(res)
    use math
    type(WKB_Object) :: this
    real*8           :: r
    logical, intent(in):: is_plus
    real*8             :: tmp, res, z, phase, phase0, k
    integer            :: l
    if(.not. this % calculate_doneQ) then
       write (*,*) "pre calculation is necessary. in WKB_phase_shift"
       stop
    end if

    k = this % k
    l = this % l
    r = this % r_asym
    phase = WKB_phi(this, r) - WKB_phi(this, this % r_match) + this % in_delta
    phase0 = k * r + 1 / k * log(2 * k * r) - l * pi / 2.0d0
    if(this % amp > 0.0d0) then
       if(is_plus) then
          tmp = mod(phase - phase0, 2.0d0 * pi)
       else
          tmp = mod(phase - phase0 + pi, 2.0d0 * pi)
       end if
    else
       if(is_plus) then
          tmp = mod(phase - phase0 + pi, 2.0d0 * pi)
       else
          tmp = mod(phase - phase0, 2.0d0 * pi)
       end if
    end if
    if(tmp > 0.0d0) then
       res = tmp - 2 * pi
    else
       res = tmp
    end if
!    write(*,*) "in WKB_phase_shift. WKB_phi(r)-WKB_phi(r_match): ", WKB_phi(r) - WKB_phi(r_match)
  end function WKB_phase_shift
  !=========Calculate============
  subroutine WKB_match(this, rad, radd)
    type(WKB_Object)   :: this
    real*8, intent(in) :: rad, radd
    real*8             :: z0, z0_p
    real*8             :: ratio, psi, psid
    if(.not. this % exist_kQ) then
       write (*,*) "k is necessary. in WKB_match"
       stop
    end if

    psi  = rad / this % r_match 
    psid = -rad / (this % r_match * this % r_match) + radd / this % r_match
    z0 = WKB_zeta(this, this % r_match)
    z0_p = WKB_zeta_prime(this, this % r_match)
    ratio = psid / psi

    this % in_delta = atan(z0 / (1.0d0/ this % r_match + ratio + 0.5d0 / z0 * z0_p))
    this % amp = this % r_match * sqrt(z0) * psi / sin(this % in_delta)

    this % calculate_doneQ = .true.
  end subroutine WKB_match
  function WKB_zeta(this,r) result(res)
    type(WKB_Object)   :: this
    real*8, intent(in) :: r
    real*8             :: res
    real*8             :: c, p, pd, pdd

    c = dble(this % l * this % l + this % l)
    p = sqrt(this % k * this % k - c / (r * r) + 2.0d0 / r)
    pd = 2 * c / (r * r * r) - 2 /(r * r)
    pdd=-6*c/(r**4) + 4/(r**3)
    if(this % order .eq. 0) then
       res = p
    else if(this % order .eq. 1) then
       res = 5 * pd * pd/ (32 * p ** 5) - pdd / (8 * p ** 3) + p
    end if
  end function WKB_zeta
  function WKB_zeta_prime(this, r) result(res)
    type(WKB_Object)   :: this
    real*8, intent(in) :: r
    real*8             :: res
    real*8             :: c, p, pd, pdd, tmp1, tmp2, pddd
    c = dble(this % l * this % l + this % l)
    p = sqrt(this % k * this % k - c / (r * r) + 2.0d0 / r)
    pd = 2 * c / (r * r * r) - 2 /(r * r)
    pdd=-6*c/(r**4) + 4/(r**3)
    pddd=24 * c / (r **5) - 12 / (r **4)
    tmp1 =-25.0d0 / 64.0d0 * pd * pd * pd / (p ** 7)
    tmp2 =  pdd * pd / (2 * p ** 5) - pddd / (8 * p ** 3) + 0.5d0 * pd / p
    if(this % order .eq. 0) then
       res = 0.5d0 / p * pd
    else if(this % order .eq. 1) then
       res =  tmp1 + tmp2
    end if
  end function WKB_zeta_prime
  function WKB_zeta_double_prime(this, r) result(res)
    type(WKB_Object)   :: this
    real*8, intent(in) :: r
    real*8             :: res
    real*8             :: c, p, pdd, tmp1, tmp2
    c = dble(this % l * this % l + this % l)
    p = sqrt(this % k * this % k - c / (r * r) + 2.0d0 / r)

    tmp1 = (2 * c / (r * r * r) - 2.0d0 / (r * r)) ** 2
    tmp2 = -6 * c / (r * r * r * r) + 4 / (r * r * r)
    if(this % order .eq. 0) then
       res = tmp1 / ( 4 * p * p * p) + tmp2 / (2 * p)
    else if(this % order .eq. 1) then
       write(*,*) "mada junbisitenai. in WKB_zeta_double_prime"
       stop
    end if
    
  end function WKB_zeta_double_prime
  recursive function WKB_phi(this, r) result(res)
    type(WKB_Object)   :: this
    real*8, intent(in) :: r
    real*8             :: res, x, c
    real*8             :: term1, term2, term3, k
    complex*16         :: i = (0.0d0, 1.0d0)
    integer            :: l
    l = this % l
    k = this % k
    c = dble(l * l + l)
    x = sqrt(-c + 2.0d0 * r + k * k * r * r)
    if(this % order .eq. 0) then
       if(l .eq. 0) then
          res = x + 1 / k * log(2 * (1 + k * k * r + k * x))
       else
          res = x - sqrt(c) * atan( (r-c) /  ( sqrt(c) * x ))+ &
               1 / k * log(2.0d0 * (1 + k * k * r + k * x))
       end if
    else if(this % order .eq. 1) then
       if(l .eq. 0) then
          term1 = 0.25d0 / ( sqrt(k * k + 2.0d0 / r) ** 3.0 * r * r)
          term2 = x - 0.04166666d0 * sqrt(k * k + 2.0d0 / r) * (3 + 6 * k * k * r + 2 * k ** 4 * r * r) / ( (2 + k * k * r) ** 2)
          term3 = 1.0d0 / k * log(2*(1 + k * k * r + k * x))
          res = term1 + term2 + term3
       else
          term1 = - 0.25d0 * (c - r) / (x * x * x)- 1 / (x ** 5)
          term2 = -0.125d0 /(x ** 3) * (4 * c * c * k * k- r * (3 + k * k * r) ** 2 + c * (5-6 * k * k * r - 3 * k ** 4 * r * r)) &
               / (3 + 3 * c * k * k)
          term3 = real( -0.125d0 * i * x ** 5 * log( 2*(i * (r - c) / sqrt(c) + x) / r) / (sqrt(c) * x ** 5) )
          this % order = 0
          res =  WKB_phi(this, r) + term1 + term2 + term3
          this % order = 1
       end if
    end if
  end function WKB_phi
end module class_WKB_wkb_mathcing
