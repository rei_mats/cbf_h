program main
  !  use class_CBF_complex_basis_function_h
  use class_ION_input_output_normal
  use class_IOA_input_output_asym
  character :: mode
  type(ION_Object) :: io
  type(IOA_Object) :: ioa
  write(*,*) "c:normal, i:zigzag, g:general, a:asym"
  read(*,*) mode
  write(*,*) mode

  if(mode .eq. "c") then
     io = ION_new_from_read()
     call ION_calc(io)
  else if(mode .eq. "i") then
     io = ION_new_from_read_zigzag()
     call ION_calc(io)
  else if(mode .eq. "g") then
     io = ION_new_from_read_general()
     call ION_calc(io)
  else if(mode .eq. "a") then
     ioa= IOA_new_from_read()
     call IOA_calc(ioa)
  end if


!  type(CBF_Object) :: cbf
!  logical          :: convQ
!  real*8           :: cross_section
!  ! read
!  cbf = CBF_new_from_read_without_energy()
!  write(*,*) "energy"
!  read(*,*) ene
!  write(*,*) ene
!  call CBF_set_energy(cbf, ene)
!
!  ! opt
!  convQ = CBF_optimize_orbital_exponent(cbf)
!
!  ! print  
!  call CBF_display(cbf)
!  call CBF_print_calc_res_cross_section(cbf)
!  write(*,*) convQ
end program main
