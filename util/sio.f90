module module_SIO_simple_inout
  implicit none
  public
contains
  !----------reader-----------------------
  subroutine SIO_read_and_substitute_real(x, message)
    real*8       :: x
    character(*) :: message
    write (*,*) message
    read(*,*) x
    write (*,*) x
  end subroutine SIO_read_and_substitute_real
  subroutine SIO_read_and_substitute_complex(x, message)
    complex*16, intent(out) :: x
    character(*)            :: message
    write (*,*) message
    read(*,*) x
    write (*,*) x
  end subroutine SIO_read_and_substitute_complex
  subroutine SIO_read_and_substitute_character(x, message)
    character, intent(out) :: x
    character(*)           :: message
    write (*,*) message
    read(*,*) x
    write (*,*) x
  end subroutine SIO_read_and_substitute_character
  subroutine SIO_read_and_substitute_logical(x, message)
    logical, intent(out) :: x
    character(*)           :: message
    write (*,*) message
    read(*,*) x
    write (*,*) x
  end subroutine SIO_read_and_substitute_logical
  subroutine SIO_read_and_substitute_integer(x, message)
    integer, intent(out) :: x
    character(*)           :: message
    write (*,*) message
    read(*,*) x
    write (*,*) x
  end subroutine SIO_read_and_substitute_integer
  !----------tester----------------------
  subroutine SIO_test_value_double(calc, exact, name_of_test_value, error)
    real*8, intent(in)       :: calc, exact, error
    character(*), intent(in) :: name_of_test_value
    write(*,*) "Test for " // name_of_test_value // ": "
    if(abs(calc - exact) < error) then
       write (*,*) "SUCCESS!  :",calc, exact, calc-exact
    else
       write (*,*) "FAILED!  :",calc, exact, calc-exact
    end if
  end subroutine SIO_test_value_double
  subroutine SIO_test_value_complex(calc, exact, name_of_test_value, error)
    complex*16, intent(in)   :: calc, exact
    character(*), intent(in) :: name_of_test_value
    real*8, intent(in)       :: error
    write(*,*) "Test for " // name_of_test_value // ": "
    if(abs(calc - exact) < error) then
       write (*,*) "SUCCESS! :" , calc, exact, calc-exact
    else
       write (*,*) "FAILED! :" , calc, exact, calc-exact
    end if
  end subroutine SIO_test_value_complex
  subroutine SIO_testcase
    real*8    :: x
    character :: ch
    complex*16  :: cx
    logical     :: lo
    call SIO_read_and_substitute(x, "input real number")
    write (*,*) "x:", x
    call SIO_read_and_substitute(ch, "input character")
    write (*,*) "ch:", ch
    call SIO_read_and_substitute(cx, "input complex number")
    write (*,*) "cx:", cx
    call SIO_read_and_substitute(lo, "input logical")
    write (*,*) "lo:", lo
  end subroutine SIO_testcase
end module module_SIO_simple_inout



