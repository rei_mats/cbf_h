module class_OPT_optimization
  use math
  implicit none

  logical :: OPT_debugQ = .false.

  type OPT_Object
     integer :: loop_max
     integer :: counter
     real*8  :: eps_grad
     real*8  :: eps_val
     integer, allocatable :: opt_info_array(:) !0:fix, 1~N:same
     integer :: full_val_num
     integer :: eff_val_num
  end type OPT_Object

contains
  !=========Constructor==========
  function OPT_new(opt_info_array, loop_max) result(res)
    integer, intent(in) :: opt_info_array(:)
    integer, intent(in) :: loop_max
    type(OPT_Object)    :: res
    integer             :: sz

    sz = size(opt_info_array)
    res % loop_max = loop_max
    res % counter = 0
    allocate(res % opt_info_array(sz))
    res % opt_info_array(:) = opt_info_array(:)
    res % full_val_num = size(opt_info_array)
    res % eff_val_num  = maxval(opt_info_array)
    res % eps_grad = 1.0d-5
    res % eps_val = 1.0d-7
  end function OPT_new
  function OPT_new_from_start_index(same_start_indexes, val_num, loop_max) result(res)
    integer, intent(in) :: same_start_indexes(:)
    integer, intent(in) :: loop_max, val_num
    integer, allocatable:: same_end_indexes(:)
    type(OPT_Object)    :: res
    integer             :: i, eff_val_num, is, ie

    res % loop_max = loop_max
    res % counter  = 0
    res % eps_grad = 1.0d-5
    res % eps_val = 1.0d-7
    
    allocate(res % opt_info_array(val_num))
    res % full_val_num = val_num
    
    res % eff_val_num  = size(same_start_indexes)

    eff_val_num = size(same_start_indexes)
    allocate(same_end_indexes(eff_val_num))
    
    do i = 1, eff_val_num - 1
       same_end_indexes(i) = same_start_indexes(i + 1) - 1
    end do
    same_end_indexes(eff_val_num) = val_num
    
    do i = 1, eff_val_num
       is = same_start_indexes(i)
       ie = same_end_indexes(i)
       res % opt_info_array(is:ie) = i
    end do
    
  end function OPT_new_from_start_index
  function OPT_new_from_read(num) result(res)
    integer, intent(in), optional :: num
    integer             :: numm
    type(OPT_Object)    :: res
    integer i
    
    if(.not. present(num)) then
       write(*,*) "number of variables"
       read(*,*) numm
       write(*,*) numm
    else
       numm = num
    end if

    write(*,*) "loop num"
    read(*,*) res % loop_max
    write(*,*) res % loop_max
    res % counter = 0
    res % eps_grad = 1.0d-5
    res % eps_val = 1.0d-7
    
    write(*,*) "optimization index(0:const, 1~num:opt)"
    allocate(res % opt_info_array(numm))
    do i = 1, numm
       read(*,*) res % opt_info_array(i)
    end do
    write(*,*) res % opt_info_array
    res % full_val_num = numm
    res % eff_val_num  = maxval(res % opt_info_array)
    
  end function OPT_new_from_read
  !=========Destructor===========
  subroutine OPT_delete(this)
    type(OPT_Object) :: this
    deallocate(this % opt_info_array)
  end subroutine OPT_delete
  !=========Optimization loop====
  function OPT_update_and_optimize_complex(this, convQ, array, grad, hess) result(res)
    use math
    type(OPT_Object)       :: this
    complex*16, intent(inout) :: array(this % full_val_num)
    logical, intent(out)      :: convQ
    complex*16, intent(in) :: grad(this % full_val_num)
    complex*16, intent(in) :: hess(this % full_val_num,this % full_val_num)
    complex*16             :: array_ef(this % eff_val_num)
    complex*16             :: array_efp(this % eff_val_num)
    complex*16             :: grad_ef(this % eff_val_num)
    complex*16             :: hess_ef(this%eff_val_num,this%eff_val_num)
    logical                :: res
    real*8                 :: re
    integer                :: i, j, ei, ej

    ! update counter
    this % counter = this % counter + 1
    
    ! create effective grad and hess
    if (this % full_val_num .eq. this % eff_val_num) then
       array_ef(:) = array(:)
       grad_ef(:) = grad(:)
       hess_ef(:,:) = hess(:,:)
    else
       do ej = 1, this % eff_val_num
          grad_ef(ej) = (0.0d0, 0.0d0)
          do ei = 1, this % eff_val_num
             hess_ef(ei, ej) = (0.0d0, 0.0d0)
          end do
       end do
       
       do i = 1, this % full_val_num
          ei = this % opt_info_array(i)
          if(ei .ne. 0) then
             array_ef(ei) = array(i)
             grad_ef(ei)  =grad_ef(ei) + grad(i)
             do j = 1, this % full_val_num
                ej = this % opt_info_array(j)
                if(ej .ne. 0) then
                   hess_ef(ei,ej) = hess_ef(ei,ej) + hess(i, j)
                end if
             end do
          end if
       end do
    end if

    ! Newton-Raphson
    call MATH_newton_raphson_onestep(&
         array_ef, array_efp, hess_ef, grad_ef, this % eff_val_num)

    if(OPT_debugQ) then
       write(*,*) "counter:", this % counter
       write(*,*) "hess_ef:", hess_ef
       write(*,*) "grad_ef:", grad_ef
       write(*,*) "arry_ef:", array_ef
       write(*,*) "arry_efp:", array_efp
    end if
    
    ! Check convergence
    convQ = .true.
    do i = 1, this % eff_val_num
       re = (array_efp(i) - array_ef(i)) / array_ef(i)
       convQ = convQ .and. (abs(re) < this % eps_val)
       convQ = convQ .and. (abs(grad_ef(i)) < this % eps_grad)
    end do

    ! continue?
    res = (.not. convQ) .and. (this % counter < this % loop_max)

    ! calculate new full array
    do i = 1, this % full_val_num
       ei = this % opt_info_array(i)
       if(ei .ne. 0) then
          array(i) = array_efp(ei)
       end if
    end do
  end function OPT_update_and_optimize_complex
  subroutine OPT_reset_counter(this)
    type(OPT_Object)       :: this
    this % counter = 0
  end subroutine OPT_reset_counter
  !=========Check================
  function OPT_correctQ(this) result(res)
    type(OPT_Object) :: this
    logical          :: res, tmp, tmp2
    integer          :: mn
    integer          :: i, j
    tmp = .true.


    mn = minval(this % opt_info_array)
    tmp = tmp .and. (mn .ge. 0)
    do i = 1, this % eff_val_num
       tmp2 = .false.
       do j = 1, this % full_val_num
          tmp2 = tmp2 .or. (i .eq. this % opt_info_array(j))
       end do
       tmp = tmp .and. tmp2
    end do
    res = tmp
  end function OPT_correctQ
  !=========Display==============
  subroutine OPT_display(this)
    type(OPT_Object) :: this
    write(*,*) "loop max: ", this % loop_max
    write(*,*) "eps_grad: ", this % eps_grad
    write(*,*) "eps_val : ", this % eps_val
    write(*,*) "opt_info_array : ", this % opt_info_array
    write(*,*) "full_val_num: ", this % full_val_num
    write(*,*) "eff_val_num: ", this % eff_val_num
    write(*,*) "correct?:", OPT_correctQ(this)
  end subroutine OPT_display
  !=========Test=================
  subroutine OPT_test()
    type(OPT_Object) :: opt
    opt = OPT_new( (/0,1,2/), 10)
    call OPT_display(opt)
    call OPT_delete(opt)
    opt = OPT_new( (/0,1,3/), 10)
    call OPT_display(opt)
  end subroutine OPT_test
  subroutine OPT_test_opt_one_dim()
    type(OPT_Object) :: opt
    complex*16       :: grad(1), hess(1,1), array(1)
    logical          :: convQ
    opt = OPT_new( (/1/), 10)
    array(1) = (2.0d0, 0.0d0)
    grad(1)   = 2.0d0 * array(1) + 1
    hess(1,1) = 2.0d0
    call OPT_display(opt)
    write(*,*) "opt start"
    do while (OPT_update_and_optimize_complex(opt, convQ, array, grad, hess))
       write(*,*) array
       grad(1)   = 2.0d0 * array(1) + 1
       hess(1,1) = 2.0d0
    end do
    write(*,*) array
  end subroutine OPT_test_opt_one_dim
  subroutine OPT_test_same_vals()
    type(OPT_Object) :: opt
    complex*16       :: grad(2), hess(2,2), array(2)
    logical          :: convQ

    write(*,*) "======OPT test same vals========"

    !    opt = OPT_new_from_start_index( (/1/), 2, 10)
    opt = OPT_new( (/1, 1/), 10)
    call OPT_display(opt)
    array = (/ (1.0d0, 0.0d0), (1.0d0, 0.0d0) /)

    grad(1) = (-1.0d0, 0.0d0)
    grad(2) = (4.0d0,  0.0d0)

    hess(1,1) = (2.0d0, 0.0d0)
    hess(2,1) = (0.0d0, 0.0d0)
    hess(1,2) = (0.0d0, 0.0d0)
    hess(2,2) = (4.0d0, 0.0d0)

    do while (OPT_update_and_optimize_complex(opt, convQ, array, grad, hess))
       grad(1) = 2*array(1) - 3
       grad(2) = 4*array(2)
    end do
  end subroutine OPT_test_same_vals
  subroutine OPT_test_same_vals2()
    type(OPT_Object) :: opt
    complex*16       :: grad(2), hess(2,2), array(2)
    logical          :: convQ

    write(*,*) "======OPT test same vals2========"

    opt = OPT_new_from_start_index( (/1/), 2, 10)

    call OPT_display(opt)
    array = (/ (1.0d0, 0.0d0), (1.0d0, 0.0d0) /)

    grad(1) = (-1.0d0, 0.0d0)
    grad(2) = (4.0d0,  0.0d0)

    hess(1,1) = (2.0d0, 0.0d0)
    hess(2,1) = (0.0d0, 0.0d0)
    hess(1,2) = (0.0d0, 0.0d0)
    hess(2,2) = (4.0d0, 0.0d0)

    do while (OPT_update_and_optimize_complex(opt, convQ, array, grad, hess))
       grad(1) = 2*array(1) - 3
       grad(2) = 4*array(2)
    end do
  end subroutine OPT_test_same_vals2
  subroutine OPT_test_same_vals3()
    type(OPT_Object) :: opt
    complex*16       :: grad(3), hess(3,3), array(3)
    logical          :: convQ

    write(*,*) "======OPT test same vals3========"
    opt = OPT_new( (/1, 1, 0/), 10)

    call OPT_display(opt)
    array = (/ (1.0d0, 0.0d0), (1.0d0, 0.0d0), (1.0d0, 0.0d0) /)

    grad(1) = (-1.0d0, 0.0d0)
    grad(2) = (4.0d0,  0.0d0)
    grad(3) = (1.0d0,  0.0d0)

    hess(1,1) = (2.0d0, 0.0d0)
    hess(2,1) = (0.0d0, 0.0d0)
    hess(3,1) = (0.0d0, 0.0d0)
    hess(1,2) = (0.0d0, 0.0d0)
    hess(1,3) = (0.0d0, 0.0d0)
    hess(2,3) = (0.0d0, 0.0d0)
    hess(3,2) = (0.0d0, 0.0d0)
    hess(2,2) = (4.0d0, 0.0d0)
    hess(3,3) = (0.0d0, 0.0d0)

    do while (OPT_update_and_optimize_complex(opt, convQ, array, grad, hess))
       grad(1) = 2*array(1) - 3
       grad(2) = 4*array(2)
    end do
  end subroutine OPT_test_same_vals3
end module class_OPT_optimization
