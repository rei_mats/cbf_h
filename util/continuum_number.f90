module class_CN_contium_number
  implicit none
  type CN_Object
     real*8  :: start_val
     integer :: max_count
     real*8  :: delta_val
     integer :: counter
  end type CN_Object
contains
  !-----Constructor-------
  function CN_new(start_val, end_val, delta_val) result(res)
    real*8 , intent(in) :: start_val, end_val, delta_val
    type(CN_Object) ::res
    
    res % start_val = start_val

    if( (end_val - start_val) * delta_val > 0.0d0) then
       res % delta_val = delta_val
    else
       res % delta_val = -delta_val
    end if
    
    res % max_count = int((end_val - start_val) / res % delta_val)
    
    call CN_reset_counter(res)
    
  end function CN_new
  function CN_new_from_read_0start(in_name) result(res)
    type(CN_Object) ::res
    character(5), intent(in), optional :: in_name
    character(5)    :: name
    real*8          :: max_val, delta_val

    if(present(in_name)) then
       name = in_name
    else
       name = "val"
    end if
    write (*,*) "max " // name // ", delta " // name
    read (*,*) max_val, delta_val
    write (*,*) max_val, delta_val
    res = CN_new(0.0d0, max_val, delta_val)
  end function CN_new_from_read_0start
  subroutine CN_reset_counter(this)
    type(CN_Object), intent(inout) :: this
    this % counter = -1
  end subroutine CN_reset_counter
  !-----set next value----
  function CN_set_next(this, next, index) result(res)
    type(CN_Object), intent(inout) :: this
    real*8, intent(out)            :: next
    integer, intent(out), optional :: index
    logical                        :: res
    integer  c
    c = this % counter
    if(c < this % max_count) then
       !       next = this % start_val + this % delta_val * c
       this % counter = c + 1
       next = CN_get_current_val(this)
       res = .true.
    else
       res = .false.
    end if
    index = this % counter + 1
  end function CN_set_next
  !-----get num-----------
  function CN_get_max_count(this) result(res)
    type(CN_Object), intent(in) :: this
    integer res
    res = this % max_count + 2
  end function CN_get_max_count
  function CN_get_val(this) result(res)
    type(CN_Object), intent(in) :: this
    real*8  :: res
    res = this % max_count * this % delta_val
  end function CN_get_val
  !-----display-------
  subroutine CN_display(this)
    type(CN_Object) :: this
    write (*,*) "start val:", this % start_val
    write (*,*) "max count:", this % max_count
    write (*,*) "delta val:", this % delta_val
    write (*,*) "counter", this % counter
  end subroutine CN_display
  !-----get val--------
  function CN_get_current_val(this) result(res)
    type(CN_Object), intent(in)  :: this
    real*8                       :: res
    res = this % start_val + this % delta_val * this % counter
  end function CN_get_current_val
  function CN_is_int(this, precise) result(res)
    type(CN_Object), intent(in)  :: this
    integer,intent(in), optional :: precise
    logical                     :: res
    real*8                      :: curr
    curr = CN_get_current_val(this)
    if(present(precise)) then
       res = mod(nint(curr * precise), precise) .eq. 0
    else
       res = mod(nint(curr * 1000), 1000) .eq. 0
    end if
  end function CN_is_int
  !-----test case--------
  subroutine CN_testcase
    type(CN_Object) :: cn
    real*8  :: next
    logical :: is_digit
    real*8, allocatable :: val_times_2(:)
    integer :: i
    cn = CN_new(2.0d0, 5.0d0, 0.1d0)
    allocate(val_times_2(CN_get_max_count(cn)))

    do while(CN_set_next(cn, next, i))
       val_times_2(i) = 2 * next
       is_digit = CN_is_int(cn)
       write (*,*) next, is_digit
    end do
    write (*,*) "two times"
    call CN_reset_counter(cn)
    do while(CN_set_next(cn, next, i))
       write (*,*) next, val_times_2(i)
    end do
    
  end subroutine CN_testcase
  subroutine CN_testcase2
    type(CN_Object) :: cn
    real*8  :: next
    integer :: i

    cn = CN_new(5.0d0, 1.0d0, 1.0d0)
    do while(CN_set_next(cn, next, i))
       write(*,*) i, ":", next
    end do
  end subroutine CN_testcase2
end module class_CN_contium_number
