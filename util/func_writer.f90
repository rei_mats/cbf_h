module class_FW_function_writer
  implicit none
  type FW_Object
     integer :: func_num
     real*8  :: x0, dx
     integer :: xnum
     integer :: fileno
     integer :: counter
     logical :: openQ
  end type FW_Object
contains
  !========Constructor========
  function FW_new(num, x0, x1, dx) result(res)
    integer, intent(in)      :: num
    real*8, intent(in)       :: x0, x1, dx
    type(FW_Object)          :: res
    res % func_num  = num
    res % x0        = x0
    res % dx        = dx
    res % xnum      = int((x1-x0) / dx)
    call FW_reset(res)
    res % openQ     = .false.
  end function FW_new
  subroutine FW_open(this, file_name, file_no)
    type(FW_Object), intent(inout) :: this
    character(*)                   :: file_name
    integer                        :: file_no
    this % fileno = file_no
    this % openQ = .true.
    open(this % fileno, file = file_name)
  end subroutine FW_open
  subroutine FW_close(this)
    type(FW_Object), intent(inout) :: this
    
    if(.not. this % openQ) then
       write (*,*) "this is not opened. in FW_open"
       stop
    end if

    this % openQ = .false.
    call FW_reset(this)
    close(this % fileno)
    
  end subroutine FW_close
  !========Main loop==========
  function FW_next(this, x, val_array) result(res)
    type(FW_Object), intent(inout) :: this
    real*8, intent(in)             :: val_array(this % func_num)
    real*8, intent(out)            :: x
    real*8                         :: old_x
    logical                        :: res
    integer                        :: i

    if(.not. this % openQ) then
       write (*,*) "this is not opened. in FW_next"
       stop
    end if

    i = this % counter
    this % counter = i + 1
    old_x = i * this % dx + this % x0
    x = (i + 1) * this % dx + this % x0
    
    if(i .eq. -1) then
       res = .true.
    else
       write (this % fileno,*) old_x, val_array
       res = (this % counter < this % xnum)
    end if


  end function FW_next
  !========Control============
  subroutine FW_reset(this)
    type(FW_Object) :: this
    this % counter = -1
  end subroutine FW_reset
  function FW_get_file_no(this) result(res)
    type(FW_Object) :: this
    integer         :: res
    res = this % fileno
  end function FW_get_file_no
  subroutine FW_set_file_no(this, fileno)
    type(FW_Object) :: this
    integer, intent(in) :: fileno
    this % fileno = fileno
  end subroutine FW_set_file_no
  !========Unit test==========
  subroutine FW_unit_test
    type(FW_Object)   :: fw
    integer, parameter:: fileno = 123
    integer,parameter :: func_num = 3
    real*8            :: func_array(func_num) = 0.0d0
    real*8            :: x

    fw = FW_new(func_num, 0.1d0, 2.0d0, 0.01d0)

    call FW_open(fw, 'res.d', fileno)
    do while (FW_next(fw, x, func_array))
       func_array(1) = x
       func_array(2) = x ** 2
       func_array(3) = x ** 2 -x
    end do
    call FW_close(fw)

  end subroutine FW_unit_test
end module class_FW_function_writer
