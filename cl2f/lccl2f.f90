module class_LCCF_linear_comb_complex_l2_functions
  use class_CL2F_complex_l2_function
  implicit none
  type LCCF_Object
     integer                        :: num
     type(CL2F_Object), allocatable :: cl2f_array(:)
     complex*16, allocatable        :: coe_array(:)
  end type LCCF_Object
contains
  !--------Construct---------------
  function LCCF_new(num) result(res)
    integer, intent(in) :: num
    type(LCCF_Object)   :: res
    res % num = num
    allocate(res % cl2f_array(num))
    allocate(res % coe_array(num))
  end function LCCF_new
  function LCCF_new_same_orbital_exponent(pn_min, pn_max, orbital_exponent, basis_type) result(res)
    integer, intent(in) :: pn_min
    integer, intent(in) :: pn_max
    complex*16, intent(in)  :: orbital_exponent
    integer, intent(in) :: basis_type
    type(LCCF_Object)   :: res
    type(CL2F_Object)   :: basis
    integer             :: num, pn, i
    num = pn_max - pn_min + 1
    if(num < 1) then
       write(*,*) "pn_max <= pn_min. in LCCF_new_same_orbital_exponent"
       stop
    end if
    res = LCCF_new(num)
    i = 1
    do pn = pn_min, pn_max
       basis = CL2F_new_normalize(pn, orbital_exponent, basis_type)
       call LCCF_set_CL2F_at_n(res, basis, i)
       call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), i)
       i = i + 1
    end do
  end function LCCF_new_same_orbital_exponent
  subroutine LCCF_delete(this)
    type(LCCF_Object), intent(inout) :: this
    deallocate(this%cl2f_array)
    deallocate(this%coe_array)
  end subroutine LCCF_delete
  function LCCF_new_from_read() result(res)
    type(LCCF_Object) :: res
    character         :: basis_type_ch
    integer           :: basis_type, basis_num
    integer           :: i, pn
    real*8            :: rpart, ipart
    type(CL2F_Object) :: basis

    write (*,*) "basis type(s:STO,g:GTO), basis num"
    read (*,*) basis_type_ch, basis_num
    write (*,*) basis_type_ch, basis_num
    if(basis_type_ch .eq. "s") then
       basis_type = CL2F_CSTO
    else if(basis_type_ch .eq. "g") then
       basis_type = CL2F_CGTO
    else
       write (*,*) "bad basis type. in CBS_new_from_read"
       stop
    end if

    res = LCCF_new(basis_num)
    write (*,*) "real part, imag part of orbital exponent, principle number"

    do i = 1, basis_num
       read (*,*) rpart, ipart, pn
       write (*,*) rpart, ipart, pn
       basis = CL2F_new_normalize(pn, dcmplx(rpart, ipart), basis_type)
       call LCCF_set_CL2F_at_n(res, basis, i)
    end do
    
  end function LCCF_new_from_read
  !--------Setter-----------------
  subroutine LCCF_set_CL2F_at_n(this, cl2f, n)
    type(LCCF_Object), intent(inout) :: this
    type(CL2F_Object), intent(in)    :: cl2f
    integer, intent(in)              :: n
    call LCCF_check_index(this, n, "LCCF_set_CL2F_at_n")
    this % cl2f_array(n) = cl2f
  end subroutine LCCF_set_CL2F_at_n
  subroutine LCCF_set_coefficient_at_n(this, coe, n)
    type(LCCF_Object), intent(inout) :: this
    complex*16, intent(in)           :: coe
    integer, intent(in)              :: n
    call  LCCF_check_index(this, n, "LCCF_set_coefficient_at_n")
    this % coe_array(n) = coe
  end subroutine LCCF_set_coefficient_at_n
  subroutine LCCF_set_oe_at_n(this, oe, n)
    type(LCCF_Object), intent(inout) :: this
    complex*16, intent(in)           :: oe
    integer, intent(in)              :: n
    call  LCCF_check_index(this, n, "LCCF_set_coefficient_at_n")
    call CL2F_set_orbital_exponent(this % cl2f_array(n), oe)
  end subroutine LCCF_set_oe_at_n
  !--------Getter------------------
  function LCCF_get_CL2F_at_n(this, n) result(res)
    type(LCCF_Object), intent(inout) :: this
    integer, intent(in)              :: n
    type(CL2F_Object)                :: res
    call LCCF_check_index(this, n, "LCCF_get_CL2F_at_n")
    res = this % cl2f_array(n)
  end function LCCF_get_CL2F_at_n
  function LCCF_get_coefficient_at_n(this, n) result(res)
    type(LCCF_Object), intent(inout) :: this
    integer, intent(in)              :: n
    complex*16                       :: res
    call LCCF_check_index(this, n, "LCCF_get_coefficient_at_n")
    res = this % coe_array(n)
  end function LCCF_get_coefficient_at_n
  function LCCF_get_coefficients(this) result(res)
    type(LCCF_Object), intent(in) :: this
    complex*16  :: res(this % num)
    res(:) = this % coe_array(:)
  end function LCCF_get_coefficients
  function LCCF_get_ors(this) result(res)
    type(LCCF_Object), intent(in) :: this
    complex*16  :: res(this % num)
    integer i
    do i = 1, this % num
       res(i) = CL2F_get_orbital_exponent(this % cl2f_array(i))
    end do
  end function LCCF_get_ors
  function LCCF_get_num(this) result(res)
    type(LCCF_Object)   :: this
    integer             :: res
    res = this % num
  end function LCCF_get_num
  function LCCF_get_pns(this) result(res)
    type(LCCF_Object), intent(in) :: this
    integer                       :: res(this % num)
    integer                       :: i
    do i = 1, this % num
       res(i) = CL2F_get_principle_number(this % cl2f_array(i))
    end do
  end function LCCF_get_pns
  !--------Calc-----------------
  function LCCF_value_at_r(this, r) result(res)
    type(LCCF_Object), intent(in) :: this
    real*8, intent(in)           :: r
    complex*16  :: amp, res, tmp
    integer i
    amp = (0.0d0, 0.0d0)
    do i = 1, this % num
       tmp = this % coe_array(i) * CL2F_value_at_r(this % cl2f_array(i), r) 
       amp = amp + tmp
    end do
    res = amp
  end function LCCF_value_at_r
  function LCCF_differentiate_at_r(this, r) result(res)
    type(LCCF_Object), intent(in) :: this
    real*8, intent(in)           :: r
    complex*16  :: res, tmp, amp
    integer i
    amp = (0.0d0, 0.0d0)
    do i = 1, this % num
       tmp = this % coe_array(i) * CL2F_differentiate_at_r(this % cl2f_array(i), r)
       amp = amp + tmp
    end do
    res = amp
  end function LCCF_differentiate_at_r
  function LCCF_integrate_with_CL2F(this, cl2f) result(res)
    type(LCCF_Object), intent(in) :: this
    type(CL2F_Object), intent(in) :: cl2f
    type(CL2F_Object)             :: tmp_cl2f
    complex*16  :: res
    integer     :: i
    complex*16  :: ctmp, coe, int
    ctmp = (0.0d0, 0.0d0)
    do i = 1, this % num
       coe = this % coe_array(i)
       tmp_cl2f = this % cl2f_array(i)
       int = CL2F_matrix_element_rm(tmp_cl2f, 0, cl2f)
       ctmp = ctmp + int * coe
    end do
    res = ctmp
  end function LCCF_integrate_with_CL2F
  function LCCF_integrate_with_lccf(this1, this2) result(res)
    type(LCCF_Object) :: this1, this2
    complex*16        :: res, tmp
    integer i, j

    tmp = (0.0d0, 0.0d0)

    do i = 1, this1 % num
       do j = 1, this2 % num
          tmp = tmp + &
               LCCF_get_coefficient_at_n(this1, i) * &
               LCCF_get_coefficient_at_n(this2, j) * &
               CL2F_matrix_element_rm( &
               LCCF_get_CL2F_at_n(this1, i), &
               0, &
               LCCF_get_CL2F_at_n(this2, j))
       end do
    end do
    res = tmp
  end function LCCF_integrate_with_lccf
  !--------Check-----------------
  subroutine LCCF_check_index(this, i, from)
    type(LCCF_Object), intent(in) :: this
    integer, intent(in) :: i
    character(*)           :: from
    if(i <= 0 .or. i > this % num) then
       write (*,*) "Bad index. in" // from
       stop
    end if
  end subroutine LCCF_check_index
  !--------Display---------------
  subroutine LCCF_display(this)
    type(LCCF_Object), intent(in) :: this
    integer                       :: i
    write (*,*) 
    write (*,*) "-----BEGIN LCCF DISPLAY------"
    do i = 1, this % num
       write (*,*) "index :", i
       write (*,*) "coefficients:", real(this % coe_array(i)) ,aimag(this % coe_array(i))
       write (*,*) "functin :"
       call CL2F_display(this % cl2f_array(i))
    end do
    write (*,*) "-----END LCCF DISPLAY------"
    write (*,*) 
    
  end subroutine LCCF_display
  !--------Testcase
  subroutine LCCF_testcase
    type(LCCF_Object) :: lccf
    type(CL2F_Object) :: cl2f
    integer           :: num = 5
    integer           :: i

    !----new----
    write (*,*) "----simple test-----"
    lccf = LCCF_new(num)

    !----set----
    do i = 1, LCCF_get_num(lccf)
       cl2f = CL2F_new_STO_normalize(i, (1.0d0, 0.0d0)*i)
       call LCCF_set_CL2F_at_n(lccf, cl2f, i)
    end do

    !----display----
    do i = 1, LCCF_get_num(lccf)
       call CL2F_display(LCCF_get_CL2F_at_n(lccf, i))
    end do

    call LCCF_delete(lccf)
    write (*,*) ""
    !----same orbital exponent-----------
    write (*,*) "---same orbital exponent"
    lccf = LCCF_new_same_orbital_exponent(3, 10, (2.0d0, -1.4d0), CL2F_CSTO)
    call LCCF_display(lccf)
  end subroutine LCCF_testcase

end module class_LCCF_linear_comb_complex_l2_functions
