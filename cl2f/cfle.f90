module class_CFLE_complex_function_linked_element
  use class_CL2F_complex_l2_function
  implicit none
  type CFLE_Object
     type(CL2F_Object)          :: cl2f
     type(CFLE_Object), pointer :: ptr_next
  end type CFLE_Object
contains
  
end module class_CFLE_complex_function_linked_element
