module class_CFGTO_complex_free_gauss_type_orbital
  implicit none
  type CFGTO_Object
     ! coefficient * r^{principle_number} e^{-a r^2 - b r}
     integer      :: principle_number
     complex*16   :: a
     complex*16   :: b
     complex*16   :: coefficient
     logical      :: normalize
  end type CFGTO_Object
contains
  !------Constructor--------
  function CFGTO_new(pn, a, b, c) result(res)
    integer, intent(in)    :: pn
    complex*16, intent(in) :: a, b, c
    type(CFGTO_Object)     :: res
    res % normalize = .false.
    call CFGTO_set_principle_number(res, pn)
    call CFGTO_set_a(res, a)
    call CFGTO_set_b(res, b)
    call CFGTO_set_coefficient(res, c)
  end function CFGTO_new

  !------Setter-------------
  subroutine CFGTO_set_principle_number(this, principle_number)
    type(CFGTO_Object)  :: this
    integer, intent(in) :: principle_number
    this % principle_number = principle_number
  end subroutine CFGTO_set_principle_number
  subroutine CFGTO_set_a(this, a)
    type(CFGTO_Object)     :: this
    complex*16, intent(in) :: a
    this % a = a
  end subroutine CFGTO_set_a
  subroutine CFGTO_set_b(this, b)
    type(CFGTO_Object)     :: this
    complex*16, intent(in) :: b
    this % b = b
  end subroutine CFGTO_set_b
  subroutine CFGTO_set_coefficient(this, c)
    type(CFGTO_Object)     :: this
    complex*16, intent(in) :: c
    this % coefficient = c    
  end subroutine CFGTO_set_coefficient

  !------Numerical----------
  function CFGTO_value_at_r(this, r) result(res)
    type(CFGTO_Object) :: this
    real*8, intent(in) :: r
    complex*16         :: res
    res = this % coefficient * r ** (this % principle_number) * &
         exp(-this % a * r * r - this % b * r)
  end function CFGTO_value_at_r

  !------Complex symmetric matrix element------
  function CFGTO_matrix_element_rm(bra, m, ket) result(res)
    use math
    type(CFGTO_Object), intent(in) :: bra, ket
    integer, intent(in)            :: m
    complex*16                     :: res, a, b, c
    integer                        :: n
    n = bra % principle_number + ket % principle_number + m
    c = bra % coefficient * ket % coefficient
    a = bra % a + ket % a
    b = bra % b + ket % b
    res = MATH_sto_gto_integrate(n, b, a, 100)
  end function CFGTO_matrix_element_rm
  function CFGTO_matrix_element_d_dm(bra, m, ket) result(res)
    use math
    type(CFGTO_Object), intent(in) :: bra, ket
    integer, intent(in)            :: m
    complex*16                     :: res, a1, b1, a2, b2, c, tmp
    integer                        :: n1, n2 ! 1:bra, 2:ket
    c = bra % coefficient * ket % coefficient
    a1 = bra % a
    a2 = ket % a
    b1 = bra % b
    b2 = ket % b
    if(m .eq. 1) then
       tmp =          n2 * MATH_sto_gto_integrate(n1 + n2 - 1, &
                                         b1 + b2, a1 + a2, 100)
       tmp = tmp -2 * a2 * MATH_sto_gto_integrate(n1 + n2 + 1, &
                                         b1 + b2, a1 + a2, 100)
       tmp = tmp    - b2 * MATH_sto_gto_integrate(n1 + n2, &
                                         b1 + b2, a1 + a2, 100)
    else if(m .eq. 2) then
       tmp = n2 * (n2 - 1) * MATH_sto_gto_integrate(n1 + n2 -2, &
                               b1 + b2, a1 + a2, 100)   
       tmp = tmp - 2 * n2 * b2 * MATH_sto_gto_integrate(n1 + n2 -1, &
                               b1 + b2, a1 + a2, 100)
       tmp = tmp + (2 * a2 * (2 * n2 + 1) + b2 * b2) * &
            MATH_sto_gto_integrate(n1 + n2, b1 + b2, a1 + a2, 100)
       tmp = tmp + 4 * a2 * b2 * MATH_sto_gto_integrate(n1 + n2 +1, &
                               b1 + b2, a1 + a2, 100)
       tmp = tmp + 4 * a2 * a2 * MATH_sto_gto_integrate(n1 + n2 +2, &
                               b1 + b2, a1 + a2, 100)
    else
       write (*,*) "m must be equal to 1 or 2. in CFGTO_matrix_element_d_dm"
       stop
    end if
    res = tmp * c
  end function CFGTO_matrix_element_d_dm

  !-----Display CFGTO_Object----------
  subroutine CFGTO_display(this)
    type(CFGTO_Object) :: this
    write (*,*) ""
    write (*,*) "--------BEGIN----------"
    write(*,*) "principle number :", this % principle_number
    write(*,*) "a :", this % a
    write(*,*) "b :", this % b
    write(*,*) "coefficient      :", this % coefficient
    write (*,*) "---------END-----------"    
  end subroutine CFGTO_display

  !-----Testcase----------------------
  subroutine CFGTO_testcase_basic
    type(CFGTO_Object) :: l2
    l2 = CFGTO_new(2, (1.0d0, 0.0d0), (2.0d0, 2.0d0), (1.0d0, 0.0d0))
    call CFGTO_display(l2)
  end subroutine CFGTO_testcase_basic
  
end module class_CFGTO_complex_free_gauss_type_orbital
