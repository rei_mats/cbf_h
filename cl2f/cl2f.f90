module class_CL2F_complex_l2_function
  use class_CGTO_complex_gauss_type_orbital
  use class_CSTO_complex_slater_type_orbital
  implicit none
  integer, parameter :: CL2F_CSTO = 1
  integer, parameter :: CL2F_CGTO = 2


  type CL2F_Object
     type(CSTO_Object) :: csto
     type(CGTO_Object) :: cgto
     integer           :: function_type
  end type CL2F_Object
contains
  !---------Constructor------------
  function CL2F_new_normalize(pn, oe, basis_type) result(res)
    integer, intent(in) :: pn
    integer, intent(in) :: basis_type
    complex*16          :: oe
    type(CL2F_Object)   :: res
    if(basis_type .eq. CL2F_CSTO) then
       res = CL2F_new_STO_normalize(pn, oe)
    else if(basis_type .eq. CL2F_CGTO) then
       res = CL2F_new_GTO_normalize(pn, oe)
    else
       write (*,*) "bad basis_type. in CL2F_new_normalize"
       stop
    end if
  end function CL2F_new_normalize
  function CL2F_new_normalize_from_char(pn, oe, basis_type_ch) result(res)
    integer, intent(in) :: pn
    character, intent(in) :: basis_type_ch
    complex*16          :: oe
    type(CL2F_Object)   :: res
    if(basis_type_ch .eq. "s") then
       res = CL2F_new_normalize(pn, oe, CL2F_CSTO)
    else if(basis_type_ch .eq. "g") then
       res = CL2F_new_normalize(pn, oe, CL2F_CGTO)
    else
       write(*,*) "bad basis type character. in CL2F_new_normalize_from_char"
       stop
    end if
  end function CL2F_new_normalize_from_char
  function CL2F_new(pn, oe, coe, basis_type) result(res)
    integer, intent(in)    :: pn
    integer, intent(in)    :: basis_type
    complex*16, intent(in) :: oe, coe
    type(CL2F_Object)      :: res
    if(basis_type .eq. CL2F_CSTO) then
       res = CL2F_new_STO(pn, oe, coe)
    else if(basis_type .eq. CL2F_CGTO) then
       res = CL2F_new_GTO(pn, oe, coe)
    else
       write (*,*) "bad basis_type. in CL2F_new"
       stop
    end if
  end function CL2F_new

  function CL2F_new_STO(pn, oe, coe) result(res)
    integer, intent(in) :: pn
    complex*16          :: oe, coe
    type(CL2F_Object)   :: res
    type(CSTO_Object)   :: csto
    res % function_type = CL2F_CSTO
    csto = CSTO_new(pn, oe, coe)
    res % csto = csto
  end function CL2F_new_STO
  function CL2F_new_GTO(pn, oe, coe) result(res)
    integer, intent(in) :: pn
    complex*16          :: oe, coe
    type(CL2F_Object)   :: res
    type(CGTO_Object)   :: cgto
    res % function_type = CL2F_CGTO
    cgto = CGTO_new(pn, oe, coe)
    res % cgto = cgto
  end function CL2F_new_GTO
  function CL2F_new_STO_normalize(pn, oe) result(res)
    integer, intent(in) :: pn
    complex*16          :: oe
    type(CL2F_Object)   :: res
    type(CSTO_Object)   :: csto
    
    csto = CSTO_new_normalize(pn, oe)
    res % csto = csto
    res % function_type = CL2F_CSTO
  end function CL2F_new_STO_normalize
  function CL2F_new_GTO_normalize(pn, oe) result(res)
    integer, intent(in) :: pn
    complex*16          :: oe
    type(CL2F_Object)   :: res
    type(CGTO_Object)   :: cgto
    
    cgto = CGTO_new_normalize(pn, oe)
    res % cgto = cgto
    res % function_type = CL2F_CGTO
  end function CL2F_new_GTO_normalize
  function CL2F_new_STO_direct(sto) result(res)
    type(CSTO_Object), intent(in) :: sto
    type(CL2F_Object)             :: res
    res % csto = sto
    res % function_type = CL2F_CSTO
  end function CL2F_new_STO_direct
  function CL2F_new_GTO_direct(gto) result(res)
    type(CGTO_Object), intent(in) :: gto
    type(CL2F_Object)             :: res
    res % cgto = gto
    res % function_type = CL2F_CGTO
  end function CL2F_new_GTO_direct
  
  !--------Setter-------------------
  subroutine CL2F_set_principle_number(this, principle_number)
    integer, intent(in) :: principle_number
    type(CL2F_Object)   :: this

    select case (this % function_type)
    case (CL2F_CSTO)
       call CSTO_set_principle_number(this % csto, principle_number)
    case (CL2F_CGTO)
       call CGTO_set_principle_number(this % cgto, principle_number)
    case default
       write (*,*) "bad function_type. in CL2F_set_principle_number"
    end select
  end subroutine CL2F_set_principle_number
  subroutine CL2F_set_orbital_exponent(this, orbital_exponent)
    complex*16, intent(in) :: orbital_exponent
    type(CL2F_Object)   :: this

    select case (this % function_type)
    case (CL2F_CSTO)
       call CSTO_set_orbital_exponent(this % csto, orbital_exponent)
    case (CL2F_CGTO)
       call CGTO_set_orbital_exponent(this % cgto, orbital_exponent)
    case default
       write (*,*) "bad function_type. in CL2F_set_orbital_exponent"
    end select
  end subroutine CL2F_set_orbital_exponent
  subroutine CL2F_set_coefficient(this, coefficient)
    complex*16, intent(in) :: coefficient
    type(CL2F_Object)   :: this

    select case (this % function_type)
    case (CL2F_CSTO)
       call CSTO_set_coefficient(this % csto, coefficient)
    case (CL2F_CGTO)
       call CGTO_set_coefficient(this % cgto, coefficient)
    case default
       write (*,*) "bad function_type. in CL2F_set_coefficient"
    end select
  end subroutine CL2F_set_coefficient

  !--------Getter-------------------
  function CL2F_get_principle_number(this) result(res)
    integer :: res
    type(CL2F_Object) :: this
    
    select case (this % function_type)
    case (CL2F_CSTO)
       res = CSTO_get_principle_number(this % csto)
    case (CL2F_CGTO)
       res = CGTO_get_principle_number(this % cgto)
    case default
       write (*,*) "bad function_type. in CL2F_get_principle_number"
    end select
  end function CL2F_get_principle_number
  function CL2F_get_orbital_exponent(this) result(res)
    complex*16        :: res
    type(CL2F_Object) :: this
    select case (this % function_type)
    case (CL2F_CSTO)
       res = CSTO_get_orbital_exponent(this % csto)
    case (CL2F_CGTO)
       res = CGTO_get_orbital_exponent(this % cgto)
    case default
       write (*,*) "bad function_type. in CL2F_get_orbital_exponent"
    end select
  end function CL2F_get_orbital_exponent
  function CL2F_get_coefficient(this) result(res)
    complex*16        :: res
    type(CL2F_Object) :: this
    select case (this % function_type)
    case (CL2F_CSTO)
       res = CSTO_get_coefficient(this % csto)
    case (CL2F_CGTO)
       res = CGTO_get_coefficient(this % cgto)
    case default
       write (*,*) "bad function_type. in CL2F_get_coefficient"
    end select
  end function CL2F_get_coefficient
  function CL2F_get_normalized(this) result(res)
    logical           :: res
    type(CL2F_Object) :: this
    select case (this % function_type)
    case (CL2F_CSTO)
       res = CSTO_get_normalized(this % csto)
    case (CL2F_CGTO)
       res = CGTO_get_normalized(this % cgto)
    case default
       write (*,*) "bad function_type. in CL2F_get_normalized"
    end select
  end function CL2F_get_normalized
  function CL2F_get_function_type(this) result(res)
    type(CL2F_Object) :: this
    integer           :: res
    res = this % function_type
  end function CL2F_get_function_type

  !---------Numerical calculation-----------
  function CL2F_value_at_r(this, r) result(res)
    type(CL2F_Object), intent(in)  :: this
    real*8, intent(in)             :: r
    complex*16                     :: res
    
    select case (this % function_type)
    case (CL2F_CSTO)
       res = CSTO_value_at_r(this % csto, r)
    case (CL2F_CGTO)
       res = CGTO_value_at_r(this % cgto, r)
    case default
       write (*,*) "bad function_type. in CL2F_value_at_r"
    end select
  end function CL2F_value_at_r
  function CL2F_differentiate_at_r(this, r) result(res)
    type(CL2F_Object), intent(in)  :: this
    real*8, intent(in)             :: r
    complex*16                     :: res
    
    select case (this % function_type)
    case (CL2F_CSTO)
       res = CSTO_differentiate_at_r(this % csto, r)
    case (CL2F_CGTO)
       res = CGTO_differentiate_at_r(this % cgto, r)
    case default
       write (*,*) "bad function_type. in CL2F_differentiate_at_r"
    end select
  end function CL2F_differentiate_at_r
  !--------Analytica calculation------------
  function CL2F_differentiate_orbital_exponent(this, num) result(res)
    type(CL2F_Object), intent(in) :: this
    integer, intent(in)           :: num
    type(CL2F_Object)             :: res
    type(CSTO_Object)             :: csto
    type(CGTO_Object)             :: cgto

    select case (this % function_type)
    case (CL2F_CSTO)
       csto = CSTO_differentiate_orbital_exponent(this % csto, num)
       res = CL2F_new_STO(&
            CSTO_get_principle_number(csto), &
            CSTO_get_orbital_exponent(csto), &
            CSTO_get_coefficient(csto))
    case (CL2F_CGTO)
       write (*,*) "mada junbi sitenai. in CL2F_differentiate_orbital_exponent"
       stop
       cgto = CGTO_differentiate_orbital_exponent(this % cgto)
       res = CL2F_new_GTO(&
            CGTO_get_principle_number(cgto), &
            CGTO_get_orbital_exponent(cgto), &
            CGTO_get_coefficient(cgto))
    case default
       write (*,*) "bad function_type. in CL2F_differentiate_orbital_exponent"
       stop
    end select
  end function CL2F_differentiate_orbital_exponent
  function CL2F_complex_conjugate(this) result(res)
    type(CL2F_Object), intent(in) :: this
    type(CL2F_Object)             :: res
    type(CSTO_Object)             :: csto
    type(CGTO_Object)             :: cgto
    select case (this % function_type)
    case (CL2F_CSTO)
       csto = CSTO_complex_conjugate(this % csto)
       if ( CSTO_get_normalized(csto) ) then
          res = CL2F_new_STO_normalize(&
               CSTO_get_principle_number(csto), &
               CSTO_get_orbital_exponent(csto))
       end if
          res = CL2F_new_STO(&
               CSTO_get_principle_number(csto), &
               CSTO_get_orbital_exponent(csto), &
               CSTO_get_coefficient(csto))
    case (CL2F_CGTO)
       write (*,*) "mada junbi sitenai. in CL2F_complex_conjugate"
       stop
    case default
       write (*,*) "bad function_type. in CL2F_complex_conjugate"
       stop
    end select
  end function CL2F_complex_conjugate
  function CL2F_create_differentiate_r(this, n) result(res)
    type(CL2F_Object), intent(in) :: this
    integer, intent(in)           :: n
    type(CL2F_Object), allocatable:: res(:)
    type(CSTO_Object), allocatable:: cstos(:)
    type(CGTO_Object), allocatable:: cgtos(:)
    integer i, num

    select case (this % function_type)
    case (CL2F_CSTO)
       cstos = CSTO_create_differentiate_r(this % csto, n)
       num = size(cstos)
       allocate(res(num))
       do i = 1, num
          res(i) = CL2F_new_STO_direct(cstos(i))
       end do
    case (CL2F_CGTO)
       cgtos = CGTO_create_differentiate_r(this % cgto, n)
       num = size(cgtos)
       allocate(res(num))
       do i = 1, num
          res(i) = CL2F_new_GTO_direct(cgtos(i))
       end do
    case default
       write (*,*) "bad function_type. in CL2F_differentiate_orbital_exponent"
       stop
    end select
  end function CL2F_create_differentiate_r
  function CL2F_times_r_m(this, m) result(res)
    type(CL2F_Object), intent(in) :: this
    integer, intent(in)           :: m
    type(CL2F_Object)             :: res
    integer pn, type
    complex*16   :: c, z
    pn = CL2F_get_principle_number(this)
    c  = CL2F_get_coefficient(this)
    z  = CL2F_get_orbital_exponent(this)
    type = this % function_type
    res = CL2F_new(pn + m, z, c, type)
  end function CL2F_times_r_m
  
  !---------Complex symmetric Matrix element -----
  function CL2F_matrix_element_rm(bra, m, ket) result(res)
    use math
    type(CL2F_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m
    complex*16                    :: res
    if( bra % function_type .eq. CL2F_CSTO .and. &
        ket % function_type .eq. CL2F_CSTO) then
       res = CSTO_matrix_element_rm(bra % csto, m, ket % csto)
    else if(bra % function_type .eq. CL2F_CGTO .and. &
         ket % function_type .eq. CL2F_CGTO) then
       res = CGTO_matrix_element_rm(bra % cgto, m, ket % cgto)
    else
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_rm"
       stop
    end if
  end function CL2F_matrix_element_rm
  function CL2F_matrix_element_d_dm(bra, m, ket) result(res)
    type(CL2F_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m
    complex*16                    :: res
    if( bra % function_type .eq. CL2F_CSTO .and. &
        ket % function_type .eq. CL2F_CSTO) then
       res = CSTO_matrix_element_d_dm(bra % csto, m, ket % csto)
    else if(bra % function_type .eq. CL2F_CGTO .and. &
         ket % function_type .eq. CL2F_CGTO) then
       res = CGTO_matrix_element_d_dm(bra % cgto, m, ket % cgto)
    else
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_d_dm"
       stop
    end if
  end function CL2F_matrix_element_d_dm
  function CL2F_matrix_element_dif_func_rm(bra, m, ket, bradif, ketdif) result(res)
    type(CL2F_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m, bradif, ketdif
    complex*16                    :: res
    if( bra % function_type .eq. CL2F_CSTO .and. &
        ket % function_type .eq. CL2F_CSTO) then
       res = CSTO_matrix_element_dif_func_rm(bra % csto, m, ket % csto, bradif, ketdif)
    else if(bra % function_type .eq. CL2F_CGTO .and. &
         ket % function_type .eq. CL2F_CGTO) then
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_d_dm"
       stop
    else
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_d_dm"
       stop
    end if    
  end function CL2F_matrix_element_dif_func_rm
  function CL2F_matrix_element_dif_d2_dx2(bra, ket, bradif, ketdif) result(res)
    type(CL2F_Object), intent(in) :: bra, ket
    integer, intent(in)           :: bradif, ketdif
    complex*16                    :: res
    if( bra % function_type .eq. CL2F_CSTO .and. &
        ket % function_type .eq. CL2F_CSTO) then
       res = CSTO_matrix_element_dif_d2_dx2(bra % csto, ket % csto, bradif, ketdif)
    else if(bra % function_type .eq. CL2F_CGTO .and. &
         ket % function_type .eq. CL2F_CGTO) then
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_dif_d2_dx2"
       stop
    else
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_dif_d2_dx2"
       stop
    end if    
    
  end function CL2F_matrix_element_dif_d2_dx2

  function CL2F_matrix_element_dif_oe_rm(bra, m, ket, bradif, ketdif) result(res)
    type(CL2F_Object), intent(in) :: bra, ket
    integer, intent(in)           :: bradif, ketdif, m
    complex*16                    :: res

    if( bra % function_type .eq. CL2F_CSTO .and. &
        ket % function_type .eq. CL2F_CSTO) then
       res = CSTO_matrix_element_dif_oe_rm(bra % csto, m, ket % csto, bradif, ketdif)
    else if(bra % function_type .eq. CL2F_CGTO .and. &
         ket % function_type .eq. CL2F_CGTO) then
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_d_dm"
       stop
    else
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_d_dm"
       stop
    end if    
  end function CL2F_matrix_element_dif_oe_rm
  function CL2F_matrix_element_dif_oe_d2_dx2(bra, ket, bradif, ketdif) result(res)
    type(CL2F_Object), intent(in) :: bra, ket
    integer, intent(in)           :: bradif, ketdif
    complex*16                    :: res

    if( bra % function_type .eq. CL2F_CSTO .and. &
        ket % function_type .eq. CL2F_CSTO) then
       res = CSTO_matrix_element_dif_oe_d2_dx2(bra % csto, ket % csto, bradif, ketdif)
    else if(bra % function_type .eq. CL2F_CGTO .and. &
         ket % function_type .eq. CL2F_CGTO) then
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_dif_oe_d2_dx2"
       stop
    else
       write(*,*) "mada junbi sitenai. in CL2F_matrix_element_dif_oe_d2_dx2"
       stop
    end if    
  end function CL2F_matrix_element_dif_oe_d2_dx2

  
  !---------Display-----------------
  subroutine CL2F_display(this)
    type(CL2F_Object) :: this
    select case (this % function_type)
    case (CL2F_CSTO)
       call CSTO_display(this % csto)
    case (CL2F_CGTO)
       call CGTO_display(this % cgto)
    case default
       write (*,*) "bad function_type. in CL2F_display"
    end select
  end subroutine CL2F_display
  
  !---------Calc--------------------
  ! set this % coefficient to normalization constant
  subroutine CL2F_normalize(this)
    type(CL2F_Object) :: this
    select case (this % function_type)
    case (CL2F_CSTO)
       call CSTO_normalize(this % csto)
    case (CL2F_CGTO)
       call CGTO_normalize(this % cgto)
    case default
       write (*,*) "bad function_type. in CL2F_normalize"
    end select
  end subroutine CL2F_normalize

  !---------Testcase----------------
  subroutine CL2F_testcase_basic
    type(CL2F_Object) :: cl2f1, cl2f2

    write (*,*) "----BEGIN CL2F TEST----"
    write (*,*) "cSTO test."
    cl2f1 = CL2F_new_STO_normalize(2, (1.2d0, 0.0d0))
    cl2f2 = CL2F_new_STO_normalize(3, (1.2d0,-0.8d0))
    call CL2F_display(cl2f1)
    call CL2F_display(cl2f2)
    write (*,*) "(d/dz cl2f1|r^2|cl2f2)"
    write (*,*) "program:", CL2F_matrix_element_dif_func_rm(cl2f1, 2, cl2f2,1,0)
    write (*,*) "ecact  :", (-2.52603131819990d0, -10.98860223023356d0)
    write (*,*) "(d/dz cl2f1|r^2|d/dz cl2f2)"
    write (*,*) "program:", CL2F_matrix_element_dif_func_rm(cl2f1, 2, cl2f2,1,1)
    write (*,*) "ecact  :", (10.4349162535245d0, 19.4258804283396d0)
    
    write (*,*) "(cl2f1|r^2|d/dz cl2f2)"
    write (*,*) "program:", CL2F_matrix_element_dif_func_rm(cl2f1, 2, cl2f2,0,1)
    write (*,*) "ecact  :", (-8.50994073550202d0, -1.51957828884353d0)
    write (*,*) "(d/dz cl2f1|d2/dx2|cl2f2)"
    write (*,*) "program:", CL2F_matrix_element_dif_d2_dx2(cl2f1,cl2f2,1,0)
    write (*,*) "ecact  :", (0.545012664726766d0, 0.798413515763671d0)
    write (*,*) "(cl2f1|d2/dx2|d/dz cl2f2)"
    write (*,*) "program:", CL2F_matrix_element_dif_d2_dx2(cl2f1,cl2f2,0,1)
    write (*,*) "ecact  :", (-0.862369803824293d0, -0.474847789481435d0)
    write (*,*) "----END CL2F TEST----"
  end subroutine CL2F_testcase_basic

  subroutine CL2F_testcase_differate_r
    type(CL2F_Object)              :: cl2f
    
    type(CL2F_Object), allocatable :: cl2f_array(:)
    integer i

    write (*,*) "==============case 1===================="
    cl2f = CL2F_new_GTO(0, (2.0d0, 0.0d0), (3.0d0, 0.0d0))
    cl2f_array = CL2F_create_differentiate_r(cl2f, 1)
    write (*,*) "****Before differate*****"
    call CL2F_display(cl2f)
    write (*,*) "****After one differate*****"
    do i = 1, size(cl2f_array)
       call CL2F_display(cl2f_array(i))
    end do
    write (*,*) "n1,c-12"
    deallocate(cl2f_array)

    write (*,*) 
    write (*,*) "==============case 2===================="
    cl2f = CL2F_new_GTO(1, (2.0d0, 0.0d0), (3.0d0, 0.0d0))
    cl2f_array = CL2F_create_differentiate_r(cl2f, 1)
    write (*,*) "****Before differate*****"
    call CL2F_display(cl2f)
    write (*,*) "****After one differate*****"
    do i = 1, size(cl2f_array)
       call CL2F_display(cl2f_array(i))
    end do
    write (*,*) "n0,c3;n2,c-12"
    deallocate(cl2f_array)
    

    write (*,*) 
    write (*,*) "==============case 3===================="
    cl2f = CL2F_new_GTO(0, (2.0d0, 0.0d0), (3.0d0, 0.0d0))
    cl2f_array = CL2F_create_differentiate_r(cl2f, 2)
    write (*,*) "****Before differate*****"
    call CL2F_display(cl2f)
    write (*,*) "****After two differate*****"
    do i = 1, size(cl2f_array)
       call CL2F_display(cl2f_array(i))
    end do
    write (*,*) "n0,c-12;n2,c48"
    deallocate(cl2f_array)

    write (*,*) 
    write (*,*) "==============case 4===================="
    cl2f = CL2F_new_GTO(1, (2.0d0, 0.0d0), (3.0d0, 0.0d0))
    cl2f_array = CL2F_create_differentiate_r(cl2f, 2)
    write (*,*) "****Before differate*****"
    call CL2F_display(cl2f)
    write (*,*) "****After two differate*****"
    do i = 1, size(cl2f_array)
       call CL2F_display(cl2f_array(i))
    end do
    write (*,*) "n1,c-36;n3,c48"
    deallocate(cl2f_array)

    write (*,*) 
    write (*,*) "==============case 5===================="
    cl2f = CL2F_new_GTO(2, (2.0d0, 0.0d0), (3.0d0, 0.0d0))
    cl2f_array = CL2F_create_differentiate_r(cl2f, 2)
    write (*,*) "****Before differate*****"
    call CL2F_display(cl2f)
    write (*,*) "****After two differate*****"
    do i = 1, size(cl2f_array)
       call CL2F_display(cl2f_array(i))
    end do
    write (*,*) "n0,c6;n2,c-60;n4,c48"
    deallocate(cl2f_array)

    write (*,*) 
    write (*,*) "==============case 6===================="
    cl2f = CL2F_new_GTO(2, (0.092337d0, -0.11d0), (0.11855d0, -0.23d0))
    cl2f_array = CL2F_create_differentiate_r(cl2f, 2)
    write (*,*) "****Before differate*****"
    call CL2F_display(cl2f)
    write (*,*) "****After two differate*****"
    do i = 1, size(cl2f_array)
       call CL2F_display(cl2f_array(i))
    end do
    deallocate(cl2f_array)
    
    
  end subroutine CL2F_testcase_differate_r
end module class_CL2F_complex_l2_function
