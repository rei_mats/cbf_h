module class_NCSTO_normalized_complex_slater_type_orbital
  use class_CSTO_complex_slater_type_orbital
  use class_LCCF_linear_comb_complex_l2_functions
  implicit none
  
  type NCSTO_Object
     private
     type(CSTO_Object) :: csto
     type(LCCF_Object) :: first_differate
  end type NCSTO_Object
contains
  function NCSTO_new(principle_number, orbital_exponent) result(res)
    integer, intent(in) :: principle_number
    complex*16          :: orbital_exponent
    complex*16          :: normalized_const, nr
    type(NCSTO_Object)  :: res

    res % csto = CSTO_new_normalize(principle_number, orbital_exponent)
    normalized_const = CSTO_get_coefficient(res % csto)
    nr = (2 * principle_number + 1.0d0) / (2 * orbital_exponent)
    
    
    
  end function NCSTO_new
end module class_NCSTO_normalized_complex_slater_type_orbital
