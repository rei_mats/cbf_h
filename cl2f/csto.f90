module class_CSTO_complex_slater_type_orbital
  implicit none

  type CSTO_Object
     private
     integer         :: principle_number ! principle number
     complex*16      :: orbital_exponent ! orbital exponent
     complex*16      :: coefficient ! coefficient
     logical         :: normalized  ! true=> normalized CSTO
  end type CSTO_Object

contains

  !---------Constructor------------
  function CSTO_new(principle_number, orbital_exponent, coefficient) result(res)
    integer, intent(in) :: principle_number
    complex*16          :: orbital_exponent, coefficient
    type(CSTO_Object)   :: res
    res % normalized = .false.
    call CSTO_set_principle_number(res, principle_number)
    call CSTO_set_orbital_exponent(res, orbital_exponent)
    call CSTO_set_coefficient(res, coefficient)

    
  end function CSTO_new
  function CSTO_new_normalize(principle_number, orbital_exponent) result(res)
    integer, intent(in) :: principle_number
    complex*16          :: orbital_exponent
    type(CSTO_Object)   :: res

    res % principle_number = principle_number
    res % orbital_exponent = orbital_exponent
    res % normalized = .true.
    
    call CSTO_normalize(res)
  end function CSTO_new_normalize

  !--------Setter-------------------
  subroutine CSTO_set_principle_number(this, principle_number)
    integer, intent(in) :: principle_number
    type(CSTO_Object)   :: this
    this % principle_number = principle_number
    if(this % normalized) then
       call CSTO_normalize(this)
    end if
  end subroutine CSTO_set_principle_number
  subroutine CSTO_set_orbital_exponent(this, orbital_exponent)
    complex*16, intent(in) :: orbital_exponent
    type(CSTO_Object)      :: this
    this % orbital_exponent = orbital_exponent
    if(this % normalized) then
       call CSTO_normalize(this)
    end if
  end subroutine CSTO_set_orbital_exponent
  subroutine CSTO_set_coefficient(this, coefficient)
    complex*16, intent(in) :: coefficient
    type(CSTO_Object)      :: this
    this % coefficient = coefficient
    if(this % normalized) then
       call CSTO_normalize(this)
    end if
  end subroutine CSTO_set_coefficient

  !--------Getter-------------------
  function CSTO_get_principle_number(this) result(res)
    integer :: res
    type(CSTO_Object) :: this
    res = this % principle_number
  end function CSTO_get_principle_number
  function CSTO_get_orbital_exponent(this) result(res)
    complex*16        :: res
    type(CSTO_Object) :: this
    res = this % orbital_exponent
    if(this % normalized) then
       call CSTO_normalize(this)
    end if
  end function CSTO_get_orbital_exponent
  function CSTO_get_coefficient(this) result(res)
    complex*16        :: res
    type(CSTO_Object) :: this
    res = this % coefficient
  end function CSTO_get_coefficient
  function CSTO_get_normalized(this) result(res)
    logical           :: res
    type(CSTO_Object) :: this
    res = this % normalized
  end function CSTO_get_normalized

  !---------Numerical calculation-----------
  function CSTO_value_at_r(this, r) result(res)
    type(CSTO_Object), intent(in)  :: this
    real*8, intent(in)             :: r
    complex*16                     :: res, a, c
    integer                        :: n
    n = CSTO_get_principle_number(this)
    c = CSTO_get_coefficient(this)
    a = CSTO_get_orbital_exponent(this)
    res = c * r ** n * exp(-a * r)
  end function CSTO_value_at_r
  function CSTO_differentiate_at_r(this, r) result(res)
    type(CSTO_Object), intent(in)  :: this
    real*8, intent(in)             :: r
    complex*16                     :: res, c, a
    integer                        :: n

    n = this % principle_number
    c = this % coefficient
    a = this % orbital_exponent
    if(n .eq. 0) then
       res = -a * c * exp(-a * r)
    else
       res = n * c * r ** (n-1) * exp(-a * r) -a * c * r ** n * exp(-a * r) 
    end if

  end function CSTO_differentiate_at_r
  !--------Analytica calculation------------
  function CSTO_differentiate_orbital_exponent(this, num) result(res)
    type(CSTO_Object), intent(in) :: this
    integer, intent(in)          :: num
    type(CSTO_Object)             :: res
    complex*16                    :: c, z
    integer                       :: n

    if(this % normalized) then
       write (*,*) "mada junbi sitenai. in CSTO_differentiate_orbital_exponent"
       stop
    end if

    n = CSTO_get_principle_number(this) + num
    c = CSTO_get_coefficient(this) * (-1) ** num
    z = CSTO_get_orbital_exponent(this)

    res = CSTO_new(n, z, c)
  end function CSTO_differentiate_orbital_exponent
  function CSTO_complex_conjugate(this) result(res)
    type(CSTO_Object), intent(in) :: this
    type(CSTO_Object)             :: res
    complex*16                    :: c, z
    integer                       :: n
    c = CSTO_get_coefficient(this)
    z = CSTO_get_orbital_exponent(this)
    n = CSTO_get_principle_number(this)
    res = CSTO_new(n, conjg(z), conjg(c))    
  end function CSTO_complex_conjugate
  function CSTO_differentiate_r_num(this, n) result(res)
    type(CSTO_Object), intent(in) :: this
    integer, intent(in)           :: n
    integer                       :: res
    select case(n)
    case(1)
       if(n .eq. 0) then
          res = 1
       else
          res = 2
       end if
    case(2)
       if(n .eq. 0) then
          res = 1
       else if(n .eq. 1) then
          res = 2
       else
          res = 3
       end if
    case default
       write (*,*) "n must be 1 or 2. in CSTO_differentiate_r_num"
       stop
    end select
  end function CSTO_differentiate_r_num
  function CSTO_create_differentiate_r(this, n) result(res)
    type(CSTO_Object), intent(in) :: this
    integer, intent(in)           :: n
    type(CSTO_Object), allocatable:: res(:)
    complex*16  :: c, z
    integer pn
    c = this % coefficient
    z = this % orbital_exponent
    pn = this % principle_number
    select case(n)
    case(1)
       if(n .eq. 0) then
          allocate(res(1))
          res(1) = CSTO_new(0, z, - z * c)
       else
          allocate(res(2))
          res(1) = CSTO_new(n - 1, z, pn * c)
          res(2) = CSTO_new(n,     z,-z * c)
       end if
    case(2)
       if(n .eq. 0) then
          allocate(res(1))
          res(1) = CSTO_new(0, z, z * z * c)
       else if(n .eq. 1) then
          allocate(res(2))
          res(1) = CSTO_new(n,   z, z * z * c)
          res(2) = CSTO_new(n-1, z, -2 * n * z * c)
       else
          allocate(res(3))
          res(1) = CSTO_new(n,   z, z * z * c)
          res(2) = CSTO_new(n-1, z, -2 * n * z * c)
          res(3) = CSTO_new(n-2, z, n * (n - 1) * c)
       end if
    case default
       write (*,*) "n must be 1 or 2. in CSTO_differentiate_r"
       stop
    end select
  end function CSTO_create_differentiate_r

  function CSTO_multiple_rm(this, m) result(res)
    type(CSTO_Object), intent(in) :: this
    integer, intent(in)           :: m
    type(CSTO_Object)             :: res
    complex*16                    :: c, z
    integer                       :: n
    n = CSTO_get_principle_number(this)
    c = CSTO_get_coefficient(this)
    z = CSTO_get_orbital_exponent(this)
    res = CSTO_new(n + m, z, c)
  end function CSTO_multiple_rm

  !---------Complex symmetric Matrix element -----
  function CSTO_matrix_element_rm(bra, m, ket) result(res)
    use math
    type(CSTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m
    complex*16                    :: res, z, c
    integer                       :: n
    n = bra % principle_number + ket % principle_number + m
    c = bra % coefficient * ket % coefficient
    z = bra % orbital_exponent + ket % orbital_exponent
    res = MATH_slater_integral(n, z) * c
  end function CSTO_matrix_element_rm
  function CSTO_matrix_element_d_dm(bra, m, ket) result(res)
    use math
    type(CSTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m
    complex*16                    :: res, tmp, c
    complex*16                    :: zb, zk
    integer                       :: nb, nk
    c = bra % coefficient * ket % coefficient
    zb = bra % orbital_exponent
    zk = ket % orbital_exponent
    nb = bra % principle_number 
    nk = ket % principle_number

    if(m .eq. 1) then
       tmp =       nk * MATH_slater_integral(nk + nb - 1, zb + zk)
       tmp = tmp - zk * MATH_slater_integral(nk + nb    , zb + zk)
    else if(m .eq. 2) then
       tmp = nk * (nk - 1) * MATH_slater_integral(nk + nb - 2, zb + zk)
       tmp = tmp - 2*nk*zk * MATH_slater_integral(nk + nb - 1, zb + zk)
       tmp = tmp + zk * zk * MATH_slater_integral(nk + nb, zb + zk)
    else
       write(*,*) "m must eq 1 or 2. in CSTO_matrix_element_d_dm"
       stop
    end if
    res = tmp * c
  end function CSTO_matrix_element_d_dm
  function CSTO_matrix_element_dif_func_rm(bra, m, ket, bradif, ketdif)result(res)
    use math
    type(CSTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m, bradif, ketdif
    complex*16                    :: res
    complex*16                    :: zb, zk, nrb, nrk, c
    integer                       :: nb, nk

    if ( bradif .eq. 0 .and. ketdif .eq. 0 ) then
       res = CSTO_matrix_element_rm(bra, m, ket)
    else

       c = bra % coefficient * ket % coefficient    
       zk = CSTO_get_orbital_exponent(ket)
       zb = CSTO_get_orbital_exponent(bra)

       nk = CSTO_get_principle_number(ket)
       nb = CSTO_get_principle_number(bra)

       nrb = (2 * nb + 1.0d0) / (2 * zb)
       nrk = (2 * nk + 1.0d0) / (2 * zk)

       if(bradif .eq. 1 .and. ketdif .eq. 0) then
          if(.not. bra % normalized) then
             write (*,*) "bra must be normalized. in CSTO_matrix_element_1dif_rm_0dif"
             stop
          end if
          res = nrb * CSTO_matrix_element_rm(bra, m, ket) - &
               CSTO_matrix_element_rm(bra, m+1, ket)
          
       else if(bradif .eq. 0 .and. ketdif .eq. 1) then
          if(.not. ket % normalized) then
             write (*,*) "ket must be normalized. in CSTO_matrix_element_1dif_rm_0dif"
             stop
          end if
          res = nrk * CSTO_matrix_element_rm(bra, m, ket) - &
               CSTO_matrix_element_rm(bra, m+1, ket)
       else if(bradif .eq. 1 .and. ketdif .eq. 1) then
          if(.not. ket % normalized .or. .not. bra % normalized) then
             write (*,*) "ket and bra must be normalized. in CSTO_matrix_element_1dif_rm_0dif"
             stop
          end if
          res = nrb * nrk * CSTO_matrix_element_rm(bra, m, ket) &
               -nrb *       CSTO_matrix_element_rm(bra, m+1, ket) &
               -nrk *       CSTO_matrix_element_rm(bra, m+1, ket) &
               +            CSTO_matrix_element_rm(bra, m+2, ket)
       else
          write (*,*) "dif is 0 or 1. in CSTO_matrix_element_1dif_rm_0dif"
          stop
       end if
    end if

  end function CSTO_matrix_element_dif_func_rm
  function CSTO_matrix_element_dif_d2_dx2(bra, ket, bradif, ketdif) result(res)
    use math
    type(CSTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: bradif, ketdif
    type(CSTO_Object)             :: rbra, rket
    complex*16                    :: res
    complex*16                    :: zb, zk, nrb, nrk, c
    integer                       :: nb, nk

    if( bradif .eq. 0 .and. ketdif .eq. 0) then
       res = CSTO_matrix_element_d_dm(bra, 2, ket)
    else
       c = bra % coefficient * ket % coefficient
       nb = bra % principle_number 
       nk = ket % principle_number
       zb = bra % orbital_exponent
       zk = ket % orbital_exponent
       nrb = (2 * nb + 1.0d0) / (2 * zb)
       nrk = (2 * nk + 1.0d0) / (2 * zk)
       rbra = CSTO_multiple_rm(bra, 1)
       rket = CSTO_multiple_rm(ket, 1)

       if(bradif .eq. 1 .and. ketdif .eq. 0) then
          if(.not. bra % normalized) then
             write (*,*) "bra must be normalized. in CSTO_matrix_element_dif_d2_dx2"
             stop
          end if
          res = nrb * CSTO_matrix_element_d_dm(bra, 2, ket) &
               -      CSTO_matrix_element_d_dm(rbra,2, ket)
          
       else if(bradif .eq. 0 .and. ketdif .eq. 1) then
          if(.not. ket % normalized) then
             write (*,*) "ket must be normalized. in CSTO_matrix_element_dif_d2_dx2"
             stop
          end if
       
          res = nrk * CSTO_matrix_element_d_dm(bra, 2, ket) &
               -      CSTO_matrix_element_d_dm(bra,2, rket)
       else if(bradif .eq. 1 .and. ketdif .eq. 1) then
          if(.not. ket % normalized .or. .not. bra % normalized) then
             write (*,*) "ket and bra must be normalized. in CSTO_matrix_element_dif_d2_dx2"
             stop
          end if
          res = nrb * nrk * CSTO_matrix_element_d_dm(bra,  2, ket) &
               -nrb *       CSTO_matrix_element_d_dm(rbra, 2, ket) &
               -nrk *       CSTO_matrix_element_d_dm(bra,  2,rket) &
               +            CSTO_matrix_element_d_dm(rbra, 2,rket)
       else
          write (*,*) "dif must be 0 or 1. in CSTO_matrix_element_dif_d2_dx2"
          stop
       end if
    end if
  end function CSTO_matrix_element_dif_d2_dx2

  function CSTO_matrix_element_dif_oe_rm(bra, m, ket, bradif, ketdif) result(res)
    use math
    type(CSTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: bradif, ketdif, m
    type(CSTO_Object)             :: dbra, dket
    complex*16                    :: res

    if(bradif .eq. 0 .and. ketdif .eq. 0) then
       res = CSTO_matrix_element_rm(bra, m, ket)
    else
       dbra = CSTO_differentiate_orbital_exponent(bra, bradif)
       dket = CSTO_differentiate_orbital_exponent(ket, ketdif)
       res = CSTO_matrix_element_rm(dbra, m, dket)
    end if
    
  end function CSTO_matrix_element_dif_oe_rm
  function CSTO_matrix_element_dif_oe_d2_dx2(bra,ket, bradif, ketdif) result(res)
    use math
    type(CSTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: bradif, ketdif
    type(CSTO_Object)             :: dbra, dket
    complex*16                    :: res

    if(bradif .eq. 0 .and. ketdif .eq. 0) then
       res = CSTO_matrix_element_d_dm(bra, 2, ket)
    else
       dbra = CSTO_differentiate_orbital_exponent(bra, bradif)
       dket = CSTO_differentiate_orbital_exponent(ket, ketdif)
       res = CSTO_matrix_element_d_dm(dbra, 2, dket)
    end if
    
  end function CSTO_matrix_element_dif_oe_d2_dx2

  
  !---------Display-----------------
  subroutine CSTO_display(this)
    type(CSTO_Object) :: this
    write(*,*) "-----BEGIN:Display CSTO_Object--------------"
    write(*,*) "principle number :", this % principle_number
    write(*,*) "orbital_exponent: ", real(this % orbital_exponent), &
         aimag(this % orbital_exponent)
    write(*,*) "coefficient      :", this % coefficient
    write(*,*) "-----END:Display CSTO_Object--------------"
  end subroutine CSTO_display
  
  !---------Calc--------------------
  ! set this % coefficient to normalization constant
  subroutine CSTO_normalize(this)
    use math
    type(CSTO_Object) :: this
    complex*16        :: z, c, root
    integer           :: n, i
    n = this % principle_number
    z = this % orbital_exponent
    root = sqrt(2.0d0*z)
    c = root
    do i = 1, 2 * n
       c = c * root / sqrt(dble(i))
    end do
    this % coefficient = c 
  end subroutine CSTO_normalize

  !---------Testcase----------------
  subroutine CSTO_testcase_basic
    type(CSTO_Object) :: csto1, csto2
    write (*,*) "---not normalized cSTO---------"
    csto1 = CSTO_new(2, (1.2d0, 0.0d0), (1.0d0, 1.2d0))
    csto2 = CSTO_new(3, (1.2d0, -0.8d0), (1.0d0, -0.5d0))
    call CSTO_display(csto1)
    call CSTO_display(csto2)
    write (*,*) "------Matrix element test-------"
    write (*,*) "(csto1|r^2|csto2)"
    write (*,*) "program:", CSTO_matrix_element_rm(csto1, 2, csto2)
    write (*,*) "ecact  :", (-5.18334960937500d0, 0.81085968017578d0)
    write (*,*) "(csto1|d2/dx2|csto2)"
    write (*,*) "program:", CSTO_matrix_element_d_dm(csto1, 2, csto2)
    write (*,*) "ecact  :", (-0.02361328125d0, -0.41578125d0)
    
    write (*,*) "---normalized cSTO----------"    
    csto1 = CSTO_new_normalize(2, (1.2d0, 0.0d0))
    csto2 = CSTO_new_normalize(3, (1.2d0, -0.8d0))
    call CSTO_display(csto1)
    call CSTO_display(csto2)
    write (*,*) "(d/dz csto1|r^2|csto2)"
    write (*,*) "program:", CSTO_matrix_element_dif_func_rm(csto1, 2, csto2,1,0)
    write (*,*) "ecact  :", (-2.52603131819990d0, -10.98860223023356d0)
    write (*,*) "(csto1|r^2|d/dz csto2)"
    write (*,*) "program:", CSTO_matrix_element_dif_func_rm(csto1, 2, csto2,0,1)
    write (*,*) "ecact  :", (-8.50994073550202d0, -1.51957828884353d0)
    write (*,*) "(d/dz csto1|d2/dx2|csto2)"
    write (*,*) "program:", CSTO_matrix_element_dif_d2_dx2(csto1,csto2,1,0)
    write (*,*) "ecact  :", (0.545012664726766d0, 0.798413515763671d0)
    write (*,*) "(csto1|d2/dx2|d/dz csto2)"
    write (*,*) "program:", CSTO_matrix_element_dif_d2_dx2(csto1,csto2,0,1)
    write (*,*) "ecact  :", (-0.862369803824293d0, -0.474847789481435d0)

    write (*,*) ""
    write (*,*) "normalized cSTO test"
    csto1 = CSTO_new_normalize(10, (2.0d0, -1.4d0))
    call CSTO_display(csto1)
  end subroutine CSTO_testcase_basic
end module Class_CSTO_complex_slater_type_orbital
