module class_CBS_complex_basis_set
  use class_CL2F_complex_l2_function
  use class_LCCF_linear_comb_complex_l2_functions
  
  implicit none
  type CBS_Object
     type(LCCF_Object) :: ket_basis_set
     type(LCCF_Object) :: bra_basis_set
     logical           :: is_complex_symmetry
     integer           :: num
     
  end type CBS_Object
contains
  !-----Constructor------
  function CBS_new(num, is_complex_symmetry) result(res)
    integer, intent(in) :: num
    logical, intent(in) :: is_complex_symmetry
    type(CBS_Object)    :: res
    res % ket_basis_set = LCCF_new(num)
    res % bra_basis_set = LCCF_new(num)
    res % is_complex_symmetry = is_complex_symmetry
    res % num = num
  end function CBS_new
  function CBS_new_from_read(is_complex_symmetry, is_normalized) result(res)
    logical, intent(in) :: is_complex_symmetry, is_normalized
    type(CBS_Object)    :: res
    character           :: basis_type_ch
    integer             :: basis_type, basis_num
    integer             :: i, pn
    real*8              :: rpart, ipart
    type(CL2F_Object)   :: basis

    write (*,*) "basis type(s:STO,g:GTO), basis num"
    read (*,*) basis_type_ch, basis_num
    write (*,*) basis_type_ch, basis_num
    if(basis_type_ch .eq. "s") then
       basis_type = CL2F_CSTO
    else if(basis_type_ch .eq. "g") then
       basis_type = CL2F_CGTO
    else
       write (*,*) "bad basis type. in CBS_new_from_read"
       stop
    end if
    res = CBS_new(basis_num, is_complex_symmetry)
       
    write (*,*) "real part, imag part of orbital exponent, principle number"
    do i = 1, basis_num
       read (*,*) rpart, ipart, pn
       write (*,*) rpart, ipart, pn
       if(is_normalized) then
          basis = CL2F_new_normalize(pn, dcmplx(rpart, ipart), basis_type)
       else
          basis = CL2F_new(pn, dcmplx(rpart, ipart), dcmplx(1.0d0, 0.0d0), basis_type)          
       end if
       call CBS_set_CL2F_at_n(res, basis, i)
    end do
  end function CBS_new_from_read

  !-----Setter-----------
  subroutine CBS_set_CL2F_at_n(this, cl2f, n)
    type(CBS_Object), intent(inout) :: this
    type(CL2F_Object), intent(in):: cl2f
    type(CL2F_Object)            :: bra
    integer, intent(in)          :: n
    call LCCF_set_CL2F_at_n(this % ket_basis_set, cl2f, n)
    if(this % is_complex_symmetry) then
       bra = cl2f
    else
       bra = CL2F_complex_conjugate(cl2f)
    end if
    call LCCF_set_CL2F_at_n(this % bra_basis_set, bra, n)    
  end subroutine CBS_set_CL2F_at_n
  subroutine CBS_set_coefficient(this, coe, n)
    type(CBS_Object), intent(inout) :: this
    complex*16, intent(in)          :: coe
    integer, intent(in)          :: n
    call LCCF_set_coefficient_at_n(this % ket_basis_set, coe, n)
    if(this % is_complex_symmetry) then
       call LCCF_set_coefficient_at_n(this % bra_basis_set, coe, n)
    else
       call LCCF_set_coefficient_at_n(this % bra_basis_set, conjg(coe), n)
    end if
       
  end subroutine CBS_set_coefficient
  subroutine CBS_set_oe_at_n(this, oe, n)
    type(CBS_Object), intent(inout) :: this
    complex*16, intent(in)          :: oe
    integer, intent(in)          :: n
    call LCCF_set_oe_at_n(this % ket_basis_set, oe, n)
    if(this % is_complex_symmetry) then
       call LCCF_set_oe_at_n(this % bra_basis_set, oe, n)
    else
       call LCCF_set_oe_at_n(this % bra_basis_set, conjg(oe), n)
    end if
  end subroutine CBS_set_oe_at_n

  !-----Getter-----------
  function CBS_get_num(this) result(res)
    type(CBS_Object), intent(in) :: this
    integer                      :: res
    res = LCCF_get_num(this % ket_basis_set)
  end function CBS_get_num
  function CBS_get_coefficients(this) result(res)
    type(CBS_Object), intent(in) :: this
    complex*16                   :: res(this % num)
    res(:) = LCCF_get_coefficients(this % ket_basis_set)
  end function CBS_get_coefficients
  function CBS_get_oe_at_n(this, n) result(res)
    type(CBS_Object), intent(inout) :: this
    integer, intent(in)          :: n
    complex*16                   :: res
    res = CL2F_get_orbital_exponent(LCCF_get_CL2F_at_n(this % ket_basis_set, n))
  END function CBS_get_oe_at_n
  function CBS_get_coe_at_n(this, n) result(res)
    type(CBS_Object), intent(inout) :: this
    integer, intent(in)          :: n
    complex*16                   :: res
    res = LCCF_get_coefficient_at_n(this % ket_basis_set, n)
  END function CBS_get_coe_at_n
  function CBS_get_basis_at_n(this, n) result(res)
    type(CBS_Object), intent(inout) :: this
    integer, intent(in)             :: n
    type(CL2F_Object)               :: res
    res = LCCF_get_CL2F_at_n(this % ket_basis_set, n)
  END function CBS_get_basis_at_n

  !------Value----------
  function CBS_value_at_r(this, r) result(res)
    type(CBS_Object), intent(in) :: this
    real*8, intent(in)           :: r
    complex*16  :: res
    res = LCCF_value_at_r(this % ket_basis_set, r)
  end function CBS_value_at_r

  !----Matrix representation-----
  function CBS_matrix_rm(this, m) result(res)
    type(CBS_Object), intent(inout) :: this
    integer, intent(in)          :: m
    complex*16                   :: res(this % num, this % num)
    complex*16                   :: ele
    integer                      :: i, j
    type(CL2F_Object)            :: bra, ket


    do i = 1, this % num
       bra = LCCF_get_CL2F_at_n(this % bra_basis_set, i)
       do j = 1, this % num
          ket = LCCF_get_CL2F_at_n(this % ket_basis_set, j)
          ele = CL2F_matrix_element_rm(bra,m,ket)
          res(i, j) = ele
       end do
    end do
  end function CBS_matrix_rm
  function CBS_matrix_d_dm(this, m) result(res)
    type(CBS_Object), intent(inout) :: this
    integer, intent(in)          :: m
    complex*16                   :: res(this % num, this % num)
    complex*16                   :: ele
    integer                      :: i, j
    type(CL2F_Object)            :: bra, ket

    do i = 1, this % num
       bra = LCCF_get_CL2F_at_n(this % bra_basis_set, i)
       do j = 1, this % num
          ket = LCCF_get_CL2F_at_n(this % ket_basis_set, j)
          ele = CL2F_matrix_element_d_dm(bra,m,ket)
          res(i, j) = ele
       end do
    end do

  end function CBS_matrix_d_dm
  function CBS_matrix_dif_oe_rm(this, m, bradif, ketdif) result(res)
    type(CBS_Object), intent(inout) :: this
    integer, intent(in)             :: m, bradif, ketdif
    type(CL2F_Object)               :: bra, ket
    complex*16                      :: res(this % num, this % num)
    complex*16                      :: ele
    integer                         :: i, j

    do i = 1, this % num
       bra = LCCF_get_CL2F_at_n(this % bra_basis_set, i)
       do j = 1, this % num
          ket = LCCF_get_CL2F_at_n(this % ket_basis_set, j)
          ele = CL2F_matrix_element_dif_oe_rm(bra,m,ket, bradif, ketdif)
          res(i, j) = ele
       end do
    end do
  end function CBS_matrix_dif_oe_rm
  function CBS_matrix_dif_oe_d2_dx2(this, bradif, ketdif) result(res)
    type(CBS_Object), intent(inout) :: this
    integer, intent(in)             :: bradif, ketdif
    type(CL2F_Object)               :: bra, ket
    complex*16                      :: res(this % num, this % num)
    complex*16                      :: ele
    integer                         :: i, j

    do i = 1, this % num
       bra = LCCF_get_CL2F_at_n(this % bra_basis_set, i)
       do j = 1, this % num
          ket = LCCF_get_CL2F_at_n(this % ket_basis_set, j)
          
          ele = CL2F_matrix_element_dif_oe_d2_dx2(bra,ket, bradif, ketdif)
          res(i, j) = ele
       end do
    end do
  end function CBS_matrix_dif_oe_d2_dx2

  !----Storing result calculation------------
  

  !----Vector representation-----
  function CBS_vec_LCCF(this, lccf) result(res)
    type(CBS_Object), intent(inout) :: this
    type(LCCF_Object), intent(inout):: lccf
    complex*16                   :: res(this % num)
    type(CL2F_Object)            :: basis
    integer     :: i, j
    complex*16  :: ctmp, coe, int
    do i = 1, this % num
       basis = LCCF_get_CL2F_at_n(this % ket_basis_set, i)
       res(i) = LCCF_integrate_with_CL2F(lccf, basis)
    end do
  end function CBS_vec_LCCF

  !----Display------------
  subroutine CBS_display(this)
    type(CBS_Object), intent(in) :: this
    write (*,*) "-----BEGIN CBS display-----"
    write (*,*) "is complex symmetry:", this % is_complex_symmetry
    write (*,*)
    call LCCF_display(this % ket_basis_set)
  end subroutine CBS_display

  !----Test case----------
  subroutine CBS_testcase
    type(CBS_Object)   :: basis_set
    type(CL2F_Object)  :: basis1, basis2
    integer, parameter :: num = 2
    complex*16         :: mat(num, num)
    
    basis1 = CL2F_new_STO(2, (1.2d0, 0.0d0), (1.0d0, 1.2d0))
    basis2 = CL2F_new_STO(3, (1.2d0,-0.8d0), (1.0d0,-0.5d0))

    basis_set = CBS_new(num, .true.)
    call CBS_set_CL2F_at_n(basis_set, basis1, 1)
    call CBS_set_CL2F_at_n(basis_set, basis2, 2)

    write (*,*) 
    write (*,*) "----BEGIN CBS TESTCASE------"
    write (*,*) "d2/dx2"
    mat = CBS_matrix_d_dm(basis_set, 2)
    write (*,*) mat(1,:)
    write (*,*) mat(2,:)
    
    write (*,*) "x^2"
    mat     = CBS_matrix_rm(basis_set, 2)
    write (*,*) mat(1,:)
    write (*,*) mat(2,:)

    write (*,*) "(d/dz |r2|) differate only orbital expornent"
    mat = CBS_matrix_dif_oe_rm(basis_set, 2, 1, 0)
    write (*,*) "exact non diag ele:", (16.3609d0, 2.75077d0)
    write (*,*) mat(1,:)
    write (*,*) mat(2,:)

    write (*,*) "(|r2| d/dz) differate only orbital expornent"
    mat = CBS_matrix_dif_oe_rm(basis_set, 2, 0, 1)
    write (*,*) mat(1,:)
    write (*,*) mat(2,:)

    write (*,*) "(|d2/dx2| d/dz) differate only orbital expornent"
    mat = CBS_matrix_dif_oe_d2_dx2(basis_set, 0, 1)
    write (*,*) mat(1,:)
    write (*,*) mat(2,:)

    write (*,*) "(d/dz|d2/dx2|) differate only orbital expornent"
    mat = CBS_matrix_dif_oe_d2_dx2(basis_set, 1, 0)
    write (*,*) "exact non diag ele(2|1):", (-0.533628d0, 0.685503d0)
    write (*,*) "exact non diag ele(1|2):", (0.10065d0, 0.767534d0)
    write (*,*) mat(1,:)
    write (*,*) mat(2,:)

    write (*,*) "----END CBS TESTCASE------"
    
  end subroutine CBS_testcase
  subroutine CBS_testcase_set_oe
    type(CBS_Object)   :: basis_set
    type(CL2F_Object)  :: basis1
    integer, parameter :: num = 1
    
    basis1 = CL2F_new_STO(2, (1.2d0, 0.0d0), (1.0d0, 1.2d0))

    basis_set = CBS_new(num, .true.)
    call CBS_set_CL2F_at_n(basis_set, basis1, 1)
    call CBS_display(basis_set)

    call CBS_set_oe_at_n(basis_set, (0.8d0, 0.4d0), 1)
    call CBS_display(basis_set)
  end subroutine CBS_testcase_set_oe
    
end module class_CBS_complex_basis_set
