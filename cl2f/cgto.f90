module class_CGTO_complex_gauss_type_orbital
  implicit none
  
  type CGTO_Object
     private
     integer         :: principle_number ! principle number
     complex*16      :: orbital_exponent ! orbital exponent
     complex*16      :: coefficient ! coefficient
     logical         :: normalize   ! true => normalized
  end type CGTO_Object
contains
    !---------Constructor------------
  function CGTO_new(principle_number, orbital_exponent, coefficient) result(res)
    integer, intent(in) :: principle_number
    complex*16          :: orbital_exponent, coefficient
    type(CGTO_Object)   :: res
    res % normalize = .false.
    call CGTO_set_principle_number(res, principle_number)
    call CGTO_set_orbital_exponent(res, orbital_exponent)
    call CGTO_set_coefficient(res, coefficient)
    
  end function CGTO_new
  function CGTO_new_normalize(principle_number, orbital_exponent) result(res)
    integer, intent(in) :: principle_number
    complex*16          :: orbital_exponent
    type(CGTO_Object)   :: res

    res % normalize = .true.

    res % principle_number = principle_number
    res % orbital_exponent = orbital_exponent
    
    
    call CGTO_normalize(res)
  end function CGTO_new_normalize

  !--------Setter-------------------
  subroutine CGTO_set_principle_number(this, principle_number)
    integer, intent(in) :: principle_number
    type(CGTO_Object)   :: this
    this % principle_number = principle_number
    if(this % normalize) then
       call CGTO_normalize(this)
    end if
  end subroutine CGTO_set_principle_number
  subroutine CGTO_set_orbital_exponent(this, orbital_exponent)
    complex*16, intent(in) :: orbital_exponent
    type(CGTO_Object)      :: this
    this % orbital_exponent = orbital_exponent
    if(this % normalize) then
       call CGTO_normalize(this)
    end if
  end subroutine CGTO_set_orbital_exponent
  subroutine CGTO_set_coefficient(this, coefficient)
    complex*16, intent(in) :: coefficient
    type(CGTO_Object)      :: this
    this % coefficient = coefficient
    if(this % normalize) then
       call CGTO_normalize(this)
    end if
  end subroutine CGTO_set_coefficient

  !--------Getter-------------------
  function CGTO_get_principle_number(this) result(res)
    integer :: res
    type(CGTO_Object) :: this
    res = this % principle_number
  end function CGTO_get_principle_number
  function CGTO_get_orbital_exponent(this) result(res)
    complex*16        :: res
    type(CGTO_Object) :: this
    res = this % orbital_exponent
  end function CGTO_get_orbital_exponent
  function CGTO_get_coefficient(this) result(res)
    complex*16        :: res
    type(CGTO_Object) :: this
    res = this % coefficient
  end function CGTO_get_coefficient
  function CGTO_get_normalized(this) result(res)
    logical           :: res
    type(CGTO_Object) :: this
    res = this % normalize
  end function CGTO_get_normalized

  !---------Numerical calculation-----------
  function CGTO_value_at_r(this, r) result(res)
    type(CGTO_Object), intent(in)  :: this
    real*8, intent(in)             :: r
    complex*16                     :: res, a, c
    integer                        :: n
    n = CGTO_get_principle_number(this)
    c = CGTO_get_coefficient(this)
    a = CGTO_get_orbital_exponent(this)
    res = c * r ** n * exp(-a * r * r)
  end function CGTO_value_at_r
  function CGTO_differentiate_at_r(this, r) result(res)
    type(CGTO_Object), intent(in)  :: this
    real*8, intent(in)             :: r
    complex*16                     :: res, c, a
    integer                        :: n
    
    n = this % principle_number
    c = this % coefficient
    a = this % orbital_exponent
    if(n .eq. 0) then
       res = -2 * a * r * c * exp(-a * r * r)
    else
       res = n * c * r ** (n-1) * exp(-a * r * r) &
            -2 * a * c * r ** (n+1) * exp(-a * r * r) 
    end if
  end function CGTO_differentiate_at_r
  !--------Analytica calculation------------
  function CGTO_differentiate_orbital_exponent(this, num) result(res)
    type(CGTO_Object), intent(in) :: this
    integer, intent(in), optional :: num
    type(CGTO_Object)             :: res
    complex*16                    :: c, z
    integer                       :: n

    n = CGTO_get_principle_number(this)
    c = CGTO_get_coefficient(this)
    z = CGTO_get_orbital_exponent(this)
    if(present(num)) then
       if(num .eq. 1) then
          res = CGTO_new(n + 2, z, -c)
       else if(num .eq. 2) then
          res = CGTO_new(n + 4, z, +c)
       else
          write (*,*) "num must be 1 or 2. in CGTO_differentiate_orbital_exponent"
          stop
       end if
    else
       res = CGTO_new(n + 2, z, -c)
    end if
  end function CGTO_differentiate_orbital_exponent

  function CGTO_create_differentiate_r(this, n) result(res)
    type(CGTO_Object), intent(in) :: this
    integer, intent(in)           :: n
    type(CGTO_Object), allocatable:: res(:)
    complex*16  :: c, z
    integer        pn
    c = this % coefficient
    z = this % orbital_exponent
    pn = this % principle_number
    select case(n)
    case(1)
       if(pn .eq. 0) then
          allocate(res(1))
          res(1) = CGTO_new(pn + 1, z, - 2 * z * c)
       else
          allocate(res(2))
          res(1) = CGTO_new(pn + 1, z, - 2 * z * c)
          res(2) = CGTO_new(pn - 1, z, pn * c)
       end if
    case(2)
       if(pn .eq. 0 .or. pn .eq. 1) then
          allocate(res(2))
          res(1) = CGTO_new(pn,   z, -2 * z * c * (2 * pn + 1))
          res(2) = CGTO_new(pn+2, z,  4 * z * z * c)
       else
          allocate(res(3))
          res(1) = CGTO_new(pn,   z, -2 * z * (2 * pn + 1)* c)
          res(2) = CGTO_new(pn+2, z,  4 * z * z           * c)
          res(3) = CGTO_new(pn-2, z, pn * (pn - 1)        * c)
       end if
    case default
       write (*,*) "n must be 1 or 2. in CGTO_differentiate_r"
       stop
    end select
       
    
  end function CGTO_create_differentiate_r

  !---------Complex symmetric Matrix element -----
  function CGTO_matrix_element_rm(bra, m, ket) result(res)
    use math
    type(CGTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m
    complex*16                    :: res, z, c
    integer                       :: n
    c = bra % coefficient * ket % coefficient
    n = bra % principle_number + ket % principle_number + m
    z = bra % orbital_exponent + ket % orbital_exponent
    res = MATH_gauss_integral(n, z) * c
  end function CGTO_matrix_element_rm
  function CGTO_matrix_element_d_dm(bra, m, ket) result(res)
    use math
    type(CGTO_Object), intent(in) :: bra, ket
    integer, intent(in)           :: m
    complex*16                    :: res, tmp, c
    complex*16                    :: zb, zk
    integer                       :: nb, nk
    c = bra % coefficient * ket % coefficient
    nb = bra % principle_number 
    nk = ket % principle_number
    zb = bra % orbital_exponent
    zk = ket % orbital_exponent
    if(m .eq. 1) then
       tmp =       nk * MATH_slater_integral(nk + nb - 1, zb + zk)
       tmp = tmp-2*zk * MATH_slater_integral(nk + nb + 1, zb + zk)
    else if(m .eq. 2) then
       tmp = nk * (nk - 1)     * MATH_slater_integral(nk + nb - 2, zb + zk)
       tmp = tmp-2*zk*(2*nk+1) * MATH_slater_integral(nk + nb    , zb + zk)
       tmp = tmp+4* zk * zk    * MATH_slater_integral(nk + nb + 2, zb + zk)
    else
       write(*,*) "m must eq 1 or 2. in CGTO_matrix_element_d_dm"
       stop
    end if
    res = tmp * c
  end function CGTO_matrix_element_d_dm

  !---------Display-----------------
  subroutine CGTO_display(this)
    type(CGTO_Object) :: this
    write(*,*) "-----BEGIN:Display CGTO_Object--------------"
    write(*,*) "principle number :", this % principle_number
    write(*,*) "orbital exponent :", this % orbital_exponent
    write(*,*) "coefficient      :", this % coefficient
    write(*,*) "-----END:Display CGTO_Object--------------"
  end subroutine CGTO_display
  
  !---------Calc--------------------
  ! set this % coefficient to normalization constant
  subroutine CGTO_normalize(this)
    use math
    type(CGTO_Object) :: this
    complex*16        :: z, c
    integer           :: n
    n = this % principle_number
    z = this % orbital_exponent
    c = (2.0d0 / pi) ** 0.25d0 * z ** (n / 2) * &
         z ** (0.5d0 * n + 0.25d0 - n /2)
    this % coefficient = c
  end subroutine CGTO_normalize

  !---------Testcase----------------
  subroutine CGTO_testcase_basic
    type(CGTO_Object) :: csto
    csto = CGTO_new_normalize(2, (1.0d0, 0.0d0))
    call CGTO_display(csto)
  end subroutine CGTO_testcase_basic
end module class_CGTO_complex_gauss_type_orbital
