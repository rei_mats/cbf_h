module class_CFLL_complex_function_linked_list
  use class_CL2F_complex_l2_function
  use class_CFLE_complex_function_linked_element
  implicit none
  type CFLL_Object
     type(CFLE_Object), pointer :: ptr_init_node
     type(CFLE_Object), pointer :: ptr_curr_node
  end type CFLL_Object
contains
  !------Constructor----------
  function CFLL_new() result(res)
    type(CFLL_Object) :: res
    nullify(res % ptr_init_node)
    nullify(res % ptr_curr_node)
  end function CFLL_new

  !------Operate--------------
  subroutine CFLL_add_cl2f(this, cl2f)
    type(CFLL_Object), intent(inout) :: this
    type(CL2F_Object), intent(in)    :: cl2f
    type(CFLE_Object), pointer :: ptr_new_node

    allocate(ptr_new_node)
    ptr_new_node % cl2f = cl2f
    if(associated(this % ptr_init_node)) then
       ptr_new_node % ptr_next => this % ptr_init_node
    else
       nullify(ptr_new_node % ptr_next)
    end if
    this % ptr_init_node => ptr_new_node
  end subroutine CFLL_add_cl2f
  subroutine CFLL_delete(this, ptr_delete_node)
    type(CFLL_Object), intent(inout) :: this
    type(CFLE_Object), intent(in), optional, pointer :: ptr_delete_node
    type(CFLE_Object), pointer :: ptr_curr_node
    if(present(ptr_delete_node)) then
       write (*,*) "mada junbi sitenai. in CFLL_delete"
       stop
    else
       ptr_curr_node => this % ptr_init_node
       this % ptr_init_node => ptr_curr_node % ptr_next
       deallocate(ptr_curr_node)
    end if
  end subroutine CFLL_delete
  subroutine CFLL_delete_at_n(this, n)
    type(CFLL_Object), intent(inout) :: this
    integer, intent(in)              :: n
    type(CFLE_Object), pointer        :: ptr_node, ptr_pre_node
    integer counter
    logical succucess
    if(n .eq. 1) then
       call CFLL_delete(this)
    else
       counter = 1
       succucess = .false.
       ptr_pre_node => this % ptr_init_node
       do while (CFLL_loop_next_node(this, ptr_node))
          if(counter .eq. n) then
             succucess = .true.
             if(associated(ptr_node % ptr_next)) then
                ptr_pre_node % ptr_next => ptr_node % ptr_next
             else
                nullify(ptr_pre_node % ptr_next)
             end if
             deallocate(ptr_node)
          end if
          counter = counter + 1
          ptr_pre_node => ptr_node
       end do
       if(.not. succucess) then
          write (*,*) "bad n. in CFLL_loop_next_node"
          stop
       end if
    end if

    
  end subroutine CFLL_delete_at_n
  function CFLL_get_previous_node(this, ptr_node) result(res)
    type(CFLL_Object), intent(inout)       :: this
    type(CFLE_Object), intent(in), pointer :: ptr_node
    type(CFLE_Object), pointer             :: ptr_curr, res
    logical succucess

    succucess = .false.
    do while (CFLL_loop_next_node(this, ptr_curr))
       if(associated(ptr_curr % ptr_next, ptr_node)) then
          succucess = .true.
          res = ptr_curr
       end if
    end do
    if(.not. succucess) then
       write (*,*) "no ptr_node in this linked list. in CFLL_get_previous_node"
       stop
    end if
  end function CFLL_get_previous_node

  !------accessor------------
  function CFLL_get_size(this) result(res)
    type(CFLL_Object), intent(inout)       :: this
    type(CFLE_Object), pointer :: ptr
    integer res, c
    c = 0
    do while(CFLL_loop_next_node(this, ptr))
       c = c + 1
    end do
    res = c
  end function CFLL_get_size

  !------Loop----------------
  function CFLL_loop_next_CL2F(this, ptr_next_cl2f) result(res)
    type(CFLL_Object), intent(inout) :: this
    type(CL2F_Object), intent(out), pointer :: ptr_next_cl2f
    logical res
    if(associated(this % ptr_curr_node)) then
       if(associated(this % ptr_curr_node % ptr_next)) then
          this % ptr_curr_node => this % ptr_curr_node % ptr_next
          ptr_next_cl2f => this % ptr_curr_node % cl2f
          res = .true.
       else
          nullify(this % ptr_curr_node)
          res = .false.
       end if
    else
       if(associated(this % ptr_init_node)) then
          ptr_next_cl2f => this % ptr_init_node % cl2f
          this % ptr_curr_node => this % ptr_init_node 
          res = .true.
       else
          res = .false.
       end if
    end if
  end function CFLL_loop_next_CL2F
  function CFLL_loop_next_node(this, ptr_next_node) result(res)
    type(CFLL_Object), intent(inout) :: this
    type(CFLE_Object), intent(out), pointer :: ptr_next_node
    logical res
    if(associated(this % ptr_curr_node)) then
       if(associated(this % ptr_curr_node % ptr_next)) then
          this % ptr_curr_node => this % ptr_curr_node % ptr_next
          ptr_next_node => this % ptr_curr_node
          res = .true.
       else
          nullify(this % ptr_curr_node)
          res = .false.
       end if
    else
       if(associated(this % ptr_init_node)) then
          ptr_next_node => this % ptr_init_node
          this % ptr_curr_node => this % ptr_init_node 
          res = .true.
       else
          res = .false.
       end if
    end if
  end function CFLL_loop_next_node

  !------Display members-----
  subroutine CFLL_display(this)
    type(CFLL_Object), intent(inout) :: this
    type(CFLE_Object), pointer       :: ptr_node

    do while (CFLL_loop_next_node(this, ptr_node))
       call CL2F_display(ptr_node % cl2f)
    end do

!    ptr_cfle => this % ptr_init_node
!    do
!       if(associated(ptr_cfle)) then
!          call CL2F_display(ptr_cfle % cl2f)
!          ptr_cfle => ptr_cfle % ptr_next
!       else
!          exit
!       end if
!    end do
  end subroutine CFLL_display

  !------Testcase------------
  subroutine CFLL_test
    type(CFLL_Object) linked_list
    type(CL2F_Object) cl2f
    type(CL2F_Object), pointer  :: ptr_cl2f
    type(CFLE_Object), pointer  :: ptr_node
    linked_list = CFLL_new()
    
    cl2f = CL2F_new_STO(2, (2.0d0,0.0d0), (3.0d0, 0.0d0))
    call CFLL_add_cl2f(linked_list, cl2f)
    cl2f = CL2F_new_STO(3, (2.0d0,0.0d0), (10.0d0, 0.0d0))
    call CFLL_add_cl2f(linked_list, cl2f)
    cl2f = CL2F_new_STO(5, (2.0d0,0.0d0), (13.0d0, 0.0d0))
    call CFLL_add_cl2f(linked_list, cl2f)

    do while (CFLL_loop_next_node(linked_list, ptr_node))
       call CL2F_display(ptr_node % cl2f)
    end do

    write (*,*) "####"
    call CFLL_delete_at_n(linked_list, 2)
    do while(CFLL_loop_next_CL2F(linked_list, ptr_cl2f))
       call CL2F_display(ptr_cl2f)
    end do
    
    cl2f = CL2F_new_STO(6, (2.0d0,0.0d0), (13.0d0, 0.0d0))
    call CFLL_add_cl2f(linked_list, cl2f)
    cl2f = CL2F_new_STO(10, (3.0d0,0.0d0), (13.0d0, 0.0d0))
    call CFLL_add_cl2f(linked_list, cl2f)
    call CFLL_delete_at_n(linked_list, 1)

    write (*,*) "####"
    do while(CFLL_loop_next_CL2F(linked_list, ptr_cl2f))
       call CL2F_display(ptr_cl2f)
    end do
    
  end subroutine CFLL_test
end module class_CFLL_complex_function_linked_list
