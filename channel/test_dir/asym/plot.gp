set term postscript eps enhanced color
set output 'plot.eps'
set size 0.5,0.5
set grid
set key right
set xrange [80:100]
  plot "< ./a.out" w l ti "real part"
replot "< ./a.out" u 1:3 w l ti "imag part"
replot "~/sh_ylcls/cbf_h/ver8/common/im_fowf_1skp_v0.7.d" w l ti "exact im" 
set term postscript eps enhanced color
set output 'plot.eps'
replot
