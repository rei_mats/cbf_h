program main
  use class_CHD_channel_and_dipole
  type(CHD_Object) :: chd
  integer          :: i, num
  real*8           :: x, dx, w
  complex*16       :: psi
  dx = 0.1d0
  w = 1.2
  num = 1000
  chd = CHD_new(CHD_CHA_1s_kp, CHD_DP_velocity)
  do i = 1, num
     x = i * dx 
     psi = CHD_first_order_asym(chd,w, x)
     write (*,*) x, real(psi), aimag(psi)
  end do
  write (*,*) "# k:", CHD_k_final_state(chd, w)
  write (*,*) "# radial transition amplitude: ", &
       CHD_radial_transition_amp(chd, w)

end program main
