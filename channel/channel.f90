module class_CHD_channel_and_dipole
  use class_LCCF_linear_comb_complex_l2_functions
  use class_CL2F_complex_l2_function
  use module_SIO_simple_inout
  implicit none
  integer, parameter :: CHD_CHA_1s_kp = 1
  integer, parameter :: CHD_CHA_2p_ks = 2
  integer, parameter :: CHD_CHA_2p_kd = 3
  integer, parameter :: CHD_CHA_3d_kp = 4
  integer, parameter :: CHD_CHA_3d_kf = 5
  integer, parameter :: CHD_CHA_2s_kp = 6
!  integer, parameter :: CHD_CHA_custom= 100
  integer, parameter :: CHD_CHA_r30_l1 = 10
  integer, parameter :: CHD_DP_length = 1
  integer, parameter :: CHD_DP_velocity=2
  integer, parameter :: CHD_DP_accelate=3
!  integer, parameter :: CHD_DP_custom = 100
  ! channel_custom is considered as state whose energy is 0
  
  type CHD_Object
     integer           :: channel
     integer           :: dipole
     type(LCCF_Object) :: dipole_initial_state
     real*8            :: charge
     logical           :: have_dipole_initial_stateQ
     logical           :: customQ
!     integer           :: l_for_custom
  end type CHD_Object

  
contains
  !------Constructor--------
  function CHD_new(channel, dipole, charge, check_customQ) result(res)
    integer, intent(in) :: channel
    integer, intent(in) :: dipole
    real*8, intent(in), optional :: charge
    logical, intent(in), optional :: check_customQ
    type(CHD_Object)    :: res, tmp
    integer             :: fileno = 133
    tmp % channel = channel
    tmp % dipole  = dipole
    if(present(charge)) then
       tmp % charge = charge
    else
       tmp % charge = 1.0d0
    end if
    call CHD_check_channel(tmp, "CHD_new")
    call CHD_check_dipole(tmp, "CHD_new")
    tmp % have_dipole_initial_stateQ = .false.
    tmp % customQ = .false.
    if(present(check_customQ)) then
       if(check_customQ) then
          call CHD_read_driven_info_from_file(tmp)
       end if
    end if
    res = tmp
  end function CHD_new
  function CHD_new_from_character(channel, dipole_ch, charge) result(res)
    integer, intent(in) :: channel
    character, intent(in) :: dipole_ch
    real*8, intent(in), optional :: charge
    type(CHD_Object)    :: res
    integer             :: dipole

    dipole = CHD_get_DP_from_character(dipole_ch)

    if(present(charge)) then
       res = CHD_new(channel, dipole, charge)
    else
       res = CHD_new(channel, dipole)
    end if
    call CHD_check_channel(res, "CHD_new")
    call CHD_check_dipole(res, "CHD_new")
  end function CHD_new_from_character
  function CHD_new_from_read() result(res)
    type(CHD_Object) :: res, tmp
    integer          :: channel
    character        :: dipole
    real*8           :: charge
    write (*,*) "# channel(1:1skp, 2:2pks, 3:2pkd, 4:3dkp, 5:3dkf) &
         , dipole(l:length,v:velocity,a:accel), charge"
    read(*,*) channel, dipole, charge
    tmp = CHD_new_from_character(channel, dipole, charge)
!    if(dipole .eq. "l") then
!       tmp = CHD_new(channel, CHD_DP_length, charge)
!    else if(dipole .eq. "v") then
!       tmp = CHD_new(channel, CHD_DP_velocity, charge)
!    end if
!    call CHD_check_channel(tmp, "CHD_new_from_read")
    res = tmp
  end function CHD_new_from_read
  function CHD_new_custom(ch, coe, pn, oe, basis_type, charge) result(res)
    complex*16, intent(in) :: coe, oe
    integer, intent(in)    :: pn, ch
 !   integer, intent(in)    :: l
    integer, intent(in)    :: basis_type
    real*8, intent(in)     :: charge
    type(CHD_Object)       :: res
    type(CL2F_Object)      :: cl2f

!    res % channel = ch
!    res % dipole  = CHD_DP_custom
!!    res % l_for_custom = l
!    res % dipole_initial_state = LCCF_new(1)
!    res % have_dipole_initial_stateQ = .true.
!
!    cl2f = CL2F_new(pn, oe, coe, basis_type)
!    call LCCF_set_CL2F_at_n(res % dipole_initial_state, cl2f, 1)
!    call LCCF_set_coefficient_at_n( &
!         res % dipole_initial_state, (1.0d0, 0.0d0), 1)
!    
    !    res % charge = charge
    write (*,*) "mada junbi sitenai. in CHD_new_custom"
    stop
  end function CHD_new_custom
  function CHD_new_from_file(ch, file_name) result(res)
    character(*) :: file_name
    integer, intent(in) :: ch
    type(CHD_Object) :: res
    complex*16   :: coe, oe
    integer      :: pn, l, basis_type
    real*8       :: charge, rpart, ipart
    integer      ::  file_no = 20
    open(file_no, file = file_name)

    write (*,*) ""
    write (*,*) "****START CHD_new_from_file****"

    write (*,*) "basis type(1:STO, 2:GTO)"
    read(file_no, *) basis_type
    write (*,*) basis_type

    write (*,*) "l"
    read(file_no, *) l
    write (*,*) l

    write (*,*) "coefficient"
    read(file_no, *) rpart, ipart
    coe = cmplx(rpart, ipart)
    write (*,*) coe
    
    write (*,*) "pn"
    read (file_no,*) pn
    write (*,*) pn

    write (*,*) "orbital expornent"
    read (file_no,*) rpart, ipart
    oe = cmplx(rpart, ipart)
    write (*,*) oe

    write (*,*) "charge"
    read (file_no,*) charge
    write (*,*) charge

    write (*,*) "*******************************"
    close(file_no)
    
!    res = CHD_new_custom(ch, coe, pn, oe, basis_type, l, charge)
    res = CHD_new_custom(ch, coe, pn, oe, basis_type, charge)
  end function CHD_new_from_file
  subroutine CHD_read_driven_info_from_file(this)
    type(CHD_Object) :: this
    integer          :: fileno = 133
    complex*16   :: coe, oe
    integer      :: pn, l, basis_type, num, i
    real*8       :: charge, rpart, ipart
    type(CL2F_Object)      :: cl2f


    
    open(fileno, file = 'label.d', status = 'old', err = 999)
    this % customQ = .true.
    this % have_dipole_initial_stateQ = .true.    
    write (*,*) ""
    write (*,*) "****START CHD_read_driven_info_from_file****"

    write (*,*) "basis type(1:STO, 2:GTO)"
    read(fileno, *) basis_type
    write (*,*) basis_type

    write (*,*) "l"
    read(fileno, *) l
    write (*,*) l

    write (*,*)     "# primitive functions"
    read (fileno,*) num
    write (*,*) num

    this % dipole_initial_state = LCCF_new(num)

    do i = 1, num
       write (*,*) "coefficient"
       read(fileno, *) rpart, ipart
       coe = cmplx(rpart, ipart)
       write (*,*) coe
       
    
       write (*,*) "pn"
       read (fileno,*) pn
       write (*,*) pn

       write (*,*) "orbital expornent"
       read (fileno,*) rpart, ipart
       oe = cmplx(rpart, ipart)
       write (*,*) oe

       cl2f = CL2F_new(pn, oe, coe, basis_type)
       call LCCF_set_CL2F_at_n(this % dipole_initial_state, &
            cl2f, i)
       call LCCF_set_coefficient_at_n(this % dipole_initial_state, &
            (1.0d0, 0.0d0), i)
    enddo

    write (*,*) "charge"
    read (fileno,*) charge
    write (*,*) charge

    write (*,*) "*******************************"    
    close(fileno)

999 oe = (1.0d0, 0.0)
  end subroutine CHD_read_driven_info_from_file
  function CHD_get_DP_from_character(ch) result(res)
    character, intent(in) :: ch
    integer               :: res
    select case (ch)
    case ('l')
       res = CHD_DP_length
    case ('v')
       res = CHD_DP_velocity
    case ('a')
       res = CHD_DP_accelate
    case default
       write (*,*) "This character dipole is not supported."
       write (*,*) "in CHD_get_DP_from_character"
       stop
    end select
  end function CHD_get_DP_from_character
  !-----get information of initial state
  function CHD_l_initial_state(this) result(res)
    type(CHD_Object), intent(in) :: this
    integer                      :: res
    select case (this % channel)
    case (CHD_CHA_1s_kp)
       res = 0
    case (CHD_CHA_2p_ks)
       res = 1
    case (CHD_CHA_2p_kd)
       res = 1
    case (CHD_CHA_3d_kp)
       res = 2
    case (CHD_CHA_3d_kf)
       res = 2
    case (CHD_CHA_2s_kp)
       res = 0
!    case (CHD_CHA_custom)
!       write(*,*) "this is custom channel dipole"
!       write(*,*) "there is no initial l. in CHD_l_initial_state"
!       stop
    case default
       write(*,*) "bad channel. in CHD_l_initial_state"
       stop
    end select
  end function CHD_l_initial_state
  function CHD_energy_initial_state(this) result(res)
    type(CHD_Object), intent(in) :: this
    real*8                       :: res

    select case (this % channel)
    case (CHD_CHA_1s_kp)
       res = -0.50d0 
    case (CHD_CHA_2p_ks)
       res = -1.0d0 / 8.0d0
    case (CHD_CHA_2p_kd)
       res = -1.0d0 / 8.0d0
    case (CHD_CHA_2s_kp)
       res = -1.0d0 / 8.0d0
    case (CHD_CHA_3d_kp)
       res = -1.0d0 / 18.0d0
    case (CHD_CHA_3d_kf)
       res = -1.0d0 / 18.0d0
!    case (CHD_CHA_custom)
!       res = 0.0d0
    case default
       write (*,*) "bad channel. in CHD_energy_initial_state"
       stop
    end select
  end function CHD_energy_initial_state
  function CHD_create_dipole_initial(this, basis_type) result(res)
    type(CHD_Object), intent(in) :: this
    integer, intent(in)           :: basis_type
    type(LCCF_Object)            :: res
    type(CL2F_Object)            :: basis
    complex*16                   :: coe, oe
    integer                      :: pn

    select case(basis_type)
    case (CL2F_CSTO)
       select case( this % dipole)
       case (CHD_DP_accelate) !-------------ACCEL FORM----------------
          select case (this % channel)
          case (CHD_CHA_1s_kp)
             coe = 2.0d0 / sqrt(3.0d0)
             pn  = -1
             oe  = (1.0d0, 0.0d0)
          case (CHD_CHA_2p_ks)
             coe = 1.0d0 / (6.0d0 * sqrt(2.0d0))
             pn = 0
             oe = (0.5d0, 0.0d0)
          case (CHD_CHA_2p_kd)
             coe = 1.0d0 / (3.0d0 * sqrt(10.0d0))
             pn = 0
             oe = (0.5d0, 0.0d0)
          case (CHD_CHA_3d_kp)
             coe = 4.0d0 / (81 * sqrt(30.0d0)) * sqrt(4.0d0 / 3.0d0) / sqrt(5.0d0)
             pn = 1
             oe = cmplx(1.0d0 / 3.0d0, 0.0d0)
          case (CHD_CHA_3d_kf)
             coe = 4.0d0 / (81 * sqrt(30.0d0)) * sqrt(4.0d0 / 3.0d0) * sqrt(27.0d0 / 140.0d0)
             oe = cmplx(1.0d0 / 3.0d0, 0.0d0)
             pn = 1
          case default
             write (*,*) "mada junbi sitenai. in CHD_create_dipole_initial"
             stop
          end select
          res = LCCF_new(1)
          basis = CL2F_new_STO(pn, oe, coe)
          call LCCF_set_CL2F_at_n(res, basis, 1)
          call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 1)
          
       case (CHD_DP_length)   !-------------LENGTH FORM---------------
          select case (this % channel)
          case (CHD_CHA_1s_kp)
             coe = 2.0d0 / sqrt(3.0d0)
             pn  = 2
             oe  = (1.0d0, 0.0d0)
          case (CHD_CHA_2p_ks)
             coe = 1.0d0 / (3.0d0 * sqrt(8.0d0))
             pn = 3
             oe = (0.5d0, 0.0d0)
          case (CHD_CHA_2p_kd)
             coe = 1.0d0 / (3.0d0 * sqrt(10.0d0))
             pn = 3
             oe = (0.5d0, 0.0d0)
          case (CHD_CHA_3d_kp)
             coe = 4.0d0 / (81 * sqrt(30.0d0)) * sqrt(4.0d0 / 3.0d0) / sqrt(5.0d0)
             pn = 4
             oe = cmplx(1.0d0 / 3.0d0, 0.0d0)
          case (CHD_CHA_3d_kf)
             coe = 4.0d0 / (81 * sqrt(30.0d0)) * sqrt(4.0d0 / 3.0d0) * sqrt(27.0d0 / 140.0d0)
             oe = cmplx(1.0d0 / 3.0d0, 0.0d0)
             pn = 4
          case default
             write (*,*) "mada junbi sitenai. in CHD_create_dipole_initial"
             stop
          end select
          res = LCCF_new(1)
          basis = CL2F_new_STO(pn, oe, coe)
          call LCCF_set_CL2F_at_n(res, basis, 1)
          call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 1)
          
       case (CHD_DP_velocity) !----------VELOCITY FORM---------------
          select case (this % channel)
          case (CHD_CHA_1s_kp)
             coe = -2.0d0 / sqrt(3.0d0)
             pn  = 1
             oe  = (1.0d0, 0.0d0)
             res = LCCF_new(1)
             basis = CL2F_new_STO(pn, oe, coe)
             call LCCF_set_CL2F_at_n(res, basis, 1)
             call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 1)
          case (CHD_CHA_2p_ks)
             res = LCCF_new(2)
             coe = sqrt(2.0d0) / 4.0d0 
             pn  = 1
             oe  = (0.5d0, 0.0d0)
             basis = CL2F_new_STO(pn, oe, coe)
             call LCCF_set_CL2F_at_n(res, basis, 1)
             call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 1)
             coe = - sqrt(2.0d0) / 24.0d0
             pn  = 2
             oe  = (0.5d0, 0.0d0)
             basis = CL2F_new_STO(pn, oe, coe)
             call LCCF_set_CL2F_at_n(res, basis, 2)
             call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 2)
          case (CHD_CHA_2p_kd)
             res = LCCF_new(1)
             coe = -1.0d0 / (6.0d0 * sqrt(10.0d0))
             pn = 2
             oe = (0.5d0, 0.0d0)
             basis = CL2F_new_STO(pn, oe, coe)
             call LCCF_set_CL2F_at_n(res, basis, 1)
             call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 1)
          case (CHD_CHA_3d_kp)
             res = LCCF_new(2)
             coe = 2.0d0 * 4.0d0 / (81.0d0 * sqrt(18.0d0))
             pn = 2
             oe = cmplx(1.0d0 / 3.0d0, 0.0d0)
             basis = CL2F_new_STO(pn, oe, coe)
             call LCCF_set_CL2F_at_n(res, basis, 1)
             call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 1)
             coe = -4.0d0 / 30.0d0 * 4.0d0 / (81 * sqrt(18.0d0))
             pn = 3
             basis = CL2F_new_STO(pn, oe, coe)
             call LCCF_set_CL2F_at_n(res, basis, 2)
             call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0), 2)
          case (CHD_CHA_3d_kf)
             res = LCCF_new(1)
             coe = -4.0d0 / (81 * 5 * sqrt(42.0d0))
             pn  = 3
             oe  = cmplx(1.0d0 / 3.0d0, 0.0d0)
             basis = CL2F_new_STO(pn, oe, coe)
             call LCCF_set_CL2F_at_n(res, basis, 1)
             call LCCF_set_coefficient_at_n(res, (1.0d0, 0.0d0) ,1)
          case default
             write (*,*) "mada junbi sitenai. in CHD_create_dipole_initial"
             stop
          end select
!       case (CHD_DP_custom)
!          write(*,*) "this is custom channel dipole"
!          write(*,*) "it is possible to create. in CHD_create_dipole_initial"
!          stop
       case default
          write (*,*) "mada junbi sitenai.in CHD_create_dipole_initial"
          stop
       end select
    case (CL2F_CGTO)
       write (*,*) "mada junbi sitenai.in CHD_create_dipole_initial"
       stop
    end select
  end function CHD_create_dipole_initial
  function CHD_get_dipole_init_state(this) result(res)
    type(CHD_Object) :: this
    type(LCCF_Object) :: res
    if(.not. this % have_dipole_initial_stateQ) then
       this % dipole_initial_state = CHD_create_dipole_initial(this, CL2F_CSTO)
    end if
    res = this % dipole_initial_state
  end function CHD_get_dipole_init_state
  !-----get information of final state--------
  function CHD_l_final_state(this) result(res)
    type(CHD_Object), intent(in) :: this
    integer                      :: res
    select case (this % channel)
    case (CHD_CHA_1s_kp)
       res = 1
    case (CHD_CHA_2p_ks)
       res = 0
    case (CHD_CHA_2p_kd)
       res = 2
    case (CHD_CHA_3d_kp)
       res = 1
    case (CHD_CHA_3d_kf)
       res = 3
    case (CHD_CHA_2s_kp)
       res = 1
!    case (CHD_CHA_custom)
!       res = this % l_for_custom
    case default
       write(*,*) "no data. in CHD_l_final_state"
       stop
    end select
  end function CHD_l_final_state
  function CHD_k_final_state(this, w) result(res)
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: w
    real*8                       :: res

    res = sqrt(2 * (w + CHD_energy_initial_state(this)))
       
  end function CHD_k_final_state
  function CHD_energy_final_state(this, w) result(res)
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: w
    real*8                       :: res
    res = w + CHD_energy_initial_state(this)
  end function CHD_energy_final_state
  !-----get information of photoionization
  function CHD_partital_cross_section_in_au(this, w) result(res)
    use mod_PC_physical_constant
    use math
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: w
    real*8                       :: res
    real*8  :: coe, root, up, down, c
    c = PC_light_speed
    select case (this % channel)
    case (CHD_CHA_1s_kp)
       coe = 32.0d0 * pi ** 2 * 1.0d0 /(3.0d0 * c * w ** 4)
       root = sqrt(2.0d0 * w - 1.0d0)
       up = exp(-4.0d0 / root * atan(root))
       down = 1.0d0 - exp(-2.0d0 * pi / root)
       res = coe * up / down
    case (CHD_CHA_2p_ks)
       coe = 2.0d0 * pi**2 /(c * 3.0d0**3 * 2.0d0 **2 * w ** 5)
       root = sqrt(2.0d0 * w - 0.25d0)
       up = exp(-4.0d0 / root * atan(2.0d0*root))
       down = 1-exp(-2.0d0 * pi / root)
       res = coe * up / down
    case (CHD_CHA_2p_kd)
       coe = 2.0d0 * pi**2 * 2.0d0 /(c * 27.0d0)
       coe = coe * (1/w**5 +3.0d0/(8.0d0 * w**6) )
       root = sqrt(2.0d0 * w - 0.25d0)
       up = exp(-4.0d0 /root * atan(2.0d0*root))
       down = 1.0d0 - exp(-2.0d0 * pi /root)
       res = coe * up / down
    case (CHD_CHA_3d_kp)
       coe = 4.0d0 * pi**2 / c * 2.0d0 ** 5 * 1.0d0 / dble(5 ** 2 * 3 ** 10)
       coe = coe * (1.0d0 / (w ** 6) + 4.0d0 / 9.0d0 * 1.0d0 / (w ** 7))
       root = sqrt(18.0d0 * w - 1.0d0)
       up = exp(-4.0d0 * 3 /root * atan(root))
       down = 1.0d0 - exp(-3.0d0 * 2 * pi /root)
       res = coe * up / down
    case (CHD_CHA_3d_kf)
       coe = 4.0d0 * pi**2 / c * 2 ** 6 * 1.0d0 / dble(5 ** 2 * 3 ** 9)
       coe = coe * (4.0d0 / w ** 6 + 37.0d0 / 18.0d0 / w ** 7 + 10.0d0 / 81.0d0 / w ** 8)
       root = sqrt(18.0d0 * w - 1.0d0)
       up = exp(-4.0d0 * 3 /root * atan(root))
       down = 1.0d0 - exp(-3.0d0 * 2 * pi /root)
       res = coe * up / down
    case (CHD_CHA_2s_kp)
       coe = 4.0d0 * pi * pi * w / c * 1.0 / 3.0d0
       coe = coe * (1.0d0 / (2.0d0 * w ** 5) + 3.0d0 / (16.0d0 * w ** 6))
       root = sqrt(8.0d0 * w - 1.0d0)
       up = exp(-8.0d0 / root * atan(root))
       down = 1.0d0 - exp(-4.0d0 * pi / root)
       res = coe * up / down
!    case (CHD_CHA_custom)
!       write(*,*) "this is custom channel dipole"
!       write(*,*) "there is no sigma. in CHD_partital_cross_section_in_au"
!       stop
    case default
       write (*,*) "bad channel. in CHD_partial_cross_section_in_au"
       stop
    end select
  end function CHD_partital_cross_section_in_au
  function CHD_partial_cross_section_in_au_from_fdp(this, fdp, w) result(res)
    use mod_PC_physical_constant
    use math
    type(CHD_Object), intent(in) :: this
    complex*16, intent(in)       :: fdp
    real*8, intent(in)           :: w
    real*8                       :: res, c, quote

    c = PC_light_speed
    select case(this % channel)
    case(CHD_CHA_1s_kp)
       quote = 1.0d0
    case(CHD_CHA_r30_l1)
       quote = 1.0d0
    case(CHD_CHA_2p_ks)
       quote = 1.0d0 / 3.0d0
    case(CHD_CHA_2p_kd)
       quote = 5.0d0 / 6.0d0
    case(CHD_CHA_3d_kp)
       quote = 1.0d0 / 2.0d0
    case(CHD_CHA_3d_kf)
       quote = 7.0d0 / 9.0d0
    case(CHD_CHA_2s_kp)
       quote = 1.0d0
!    case(CHD_CHA_custom)
!       write(*,*) "this is custom channel dipole"
!       write(*,*) "there is no sigma. in CHD_partital_cross_section_in_au_from"
!       stop
    case default
       write(*,*) "bad channel in CHD_partial_cross_section_in_au_from_fdp"
    end select
    
    select case (this % dipole)
    case (CHD_DP_length)
       res = -quote * 4 * pi * w / c * aimag(fdp)
    case (CHD_DP_velocity)
       res = -quote * 4 * pi / (w * c) * aimag(fdp)
    case (CHD_DP_accelate)
       res = -quote * 4 * pi / (w ** 3 * c) * aimag(fdp)       
!    case (CHD_DP_custom)
!       write (*,*) "This custom dipole. in CHD_partial_cross_section_in_au_from_fdp"
!       stop
    end select
  end function CHD_partial_cross_section_in_au_from_fdp
  function CHD_regular_radial_transition_moment_from_fdp(this, fdp, w) result(res)
    use math
    type(CHD_Object) :: this
    complex*16, intent(in) :: fdp
    real*8, intent(in)     :: w
    real*8  :: res, k, tmp
    integer :: l0, l
    k  = sqrt(2 * CHD_energy_final_state(this, w))
    l0 = CHD_l_initial_state(this)
    l  = CHD_l_final_state(this)
    tmp= sqrt(-aimag(fdp))
    select case(this % channel)
    case(CHD_CHA_1s_kp)
       res = tmp /sqrt(1.0d0 / 3.0d0)
    case(CHD_CHA_2p_ks)
       res = tmp / MATH_matrix_ele_cos(l,0,l0,0)
    case(CHD_CHA_2p_kd)
       res = tmp / MATH_matrix_ele_cos(l,0,l0,0)
    case(CHD_CHA_3d_kp)
       res = tmp / sqrt(4.0d0 / 15.0d0)
    case(CHD_CHA_3d_kf)
       res = tmp / sqrt(9.0d0 / 35.0d0)
!    case(CHD_CHA_custom)
!       write(*,*) "this is custom channel dipole"
!       write(*,*) "there is no rtm. in CHD_regular_radial_transition_moment_from_fdp"
!       stop
    case default
       write(*,*) "mada junbi sitenai. in CHD_regular_radial_transition_moment_from_fdp"
       stop
    end select
    if(this % dipole .eq. CHD_DP_velocity) then
       res = res / w
    end if
  end function CHD_regular_radial_transition_moment_from_fdp
  function CHD_coulomb_phase_shift(this, w) result(res)
!    use fgsl
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: w
    real*8  :: res
!    integer :: l
!    real*8  :: k
!    real(fgsl_double) :: zr, zi
!    type(fgsl_sf_result) :: inr, arg
!    integer(fgsl_int) :: tmp
    
!    k = CHD_k_final_state(this, w)
!    l = CHD_l_final_state(this)
!    zr = dble(l + 1)
!    zi = dble(-1.0d0 / k)
!    tmp = fgsl_sf_lngamma_complex_e(zr, zi, inr, arg)
    !    res = arg % val
    res = 0.0d0
    write(*,*) "mada junbi sitenai. in CHD_coulomb_phase_shift"
    stop
  end function CHD_coulomb_phase_shift
  function CHD_additional_phase_shift_by_matching(this, psi, dpsi, w, l, r_match) result(res)
!    use fgsl
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: psi, dpsi, w ! calced wf and its derivative and w.
    integer, intent(in)          :: l
    real*8, intent(in)           :: r_match
    real*8 :: res
!    real*8                       :: k
!    real*8                       :: up, down
!    type(fgsl_sf_result) :: f
!    type(fgsl_sf_result) :: fp
!    type(fgsl_sf_result) :: g
!    type(fgsl_sf_result) :: gp
!    real(fgsl_double) :: exp_f
!    real(fgsl_double) :: exp_g
    
!    k = CHD_k_final_state(this, w)
!    call fgsl_sf_coulomb_wave_fg_e(-1.0d0 / k, r_match * k, l, 0, &
!         f, fp, g, gp, exp_f, exp_g)
!    up   = dpsi / psi * f -fp
!    down = dpsi / psi * g -gp
    !    res = up / down
    res = 1.0d0 
    write(*,*) "mada junbi sitenai. in CHD_additional_phase_shift_by_matching"
    stop
  end function CHD_additional_phase_shift_by_matching
  function CHD_operate_final_state_hamiltonian_to_cl2f(this, cl2f) result(res)
    type(CHD_Object), intent(in) :: this
    type(CL2F_Object), intent(in) :: cl2f
    type(LCCF_Object)             :: res
    write (*,*) "mada junbi sitenai. in CHD_operate_final_state_hamiltonian_to_cl2f"
    stop
  end function CHD_operate_final_state_hamiltonian_to_cl2f
  function CHD_asymmetry_parameter(channel_p, channel_m, fdp_p, fdp_m, phase_p, phase_m, w) result(res)
    type(CHD_Object) :: channel_m, channel_p
    complex*16, intent(in) :: fdp_m, fdp_p
    real*8, intent(in)     :: w, phase_m, phase_p
    real*8                 :: moment_p, moment_m
    real*8                 :: tmp0, tmp1, tmp2, tmp3, res
    logical                :: is_correct_channel_pair
    integer                :: l

    call CHD_check_correct_pair(channel_p, channel_m, "CHD_asymmetry_parameter")
    
    moment_p = CHD_regular_radial_transition_moment_from_fdp(&
         channel_p, fdp_p, w)
    moment_m = CHD_regular_radial_transition_moment_from_fdp(&
         channel_m, fdp_m, w)

    l = CHD_l_initial_state(channel_m)
    
    tmp0 = l * (l - 1) * moment_m * moment_m
    tmp1 = (l + 1) * (l + 2) * moment_p * moment_p
    tmp2 = - 6 * l * (l + 1) * moment_p * moment_m * &
         cos(phase_p - phase_m)
    tmp3 = (2 * l + 1) * (l * moment_m * moment_m &
         + (l + 1) * moment_p * moment_p)
    res = (tmp0 + tmp1 + tmp2) / tmp3
  end function CHD_asymmetry_parameter
  function CHD_asymmetry_parameter_exact(channel_p, channel_m, w) result(res)
    type(CHD_Object)   :: channel_m, channel_p
    real*8, intent(in) :: w
    real*8             :: res
    call CHD_check_correct_pair(channel_p, channel_m, "CHD_asymmetry_parameter_exact")

    select case(channel_p % channel)
    case(CHD_CHA_2p_kd)
       res = 4.0d0 / (6.0d0 * w + 2.0d0)
    case(CHD_CHA_3d_kf)
       res = (2.0d0 + 3.0d0 * w) / (1.0d0 + 15.0d0 * w)
    case default
       write(*,*) "bad channel. in CHD_asymmetry_parameter_exact"
       stop
    end select
    
  end function CHD_asymmetry_parameter_exact
  subroutine CHD_check_correct_pair(channel_p, channel_m, from) 
    type(CHD_Object) :: channel_p, channel_m
    character(*)     :: from
    logical          :: tmp
    tmp = &
         (channel_p % channel .eq. CHD_CHA_2p_kd .and. &
         channel_m % channel .eq. CHD_CHA_2p_ks) .or.  &
         (channel_p % channel .eq. CHD_CHA_3d_kf .and. &
         channel_m % channel .eq. CHD_CHA_3d_kp)
    if(.not. tmp) then
       write(*,*) "bad channel pair.", from
       stop
    end if
  end subroutine CHD_check_correct_pair
 !-----calc first order wave function--------------
  function CHD_first_order_asym(this, w, r) result(res)
    use math
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: w, r
    complex*16                   :: res
    real*8                       :: k, d
    complex*16                   :: phi
    integer                      :: l
    k = CHD_k_final_state(this, w)
    l = CHD_l_final_state(this)
    d = CHD_radial_transition_amp(this, w)
    phi = (1.0d0, 0.0d0) * (k * r + 1.0d0 / k * log(2 * k * r) - &
         l * pi / 2.0d0 + CHD_coulomb_phase_shift(this, w))
    !    res = -2 * d / k * MATH_expi(phi)
    res = -2 * d / k * cmplx(cos(real(phi)), sin(real(phi)))
  end function CHD_first_order_asym
  function CHD_radial_transition_amp(this, w) result(res)
    use math
    use mod_PC_physical_constant
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: w
    real*8                       :: res, tmp, c, k, wg, cs
    integer                      :: l, l0, lm
    k = CHD_k_final_state(this, w)

    select case(this % channel)
    case (CHD_CHA_1s_kp)
       tmp = 8 * sqrt(2.0d0 * pi * k / 3.0d0)
       tmp = tmp * sqrt(1.0d0 / (32 * w**5) * CHD_kn(this, w))
    case (CHD_CHA_2p_ks)
       tmp = 128.0d0 * sqrt(k * pi) 
       tmp = tmp / (3 * (1.0d0 + 4 * k * k) ** 3) * sqrt(CHD_kn(this, w))
    case (CHD_CHA_2p_kd)
       tmp = 1024.0d0 * sqrt(1 + 1.0d0 / (k * k)) * &
            sqrt(4 + 1.0d0 / (k * k)) * sqrt(k ** 5) *  sqrt(pi / 5.0d0)
       tmp = tmp / (3 * (1 + 4 * k * k) ** 4) * sqrt(CHD_kn(this, w))
    case (CHD_CHA_3d_kp)
       tmp = 1728.0d0 * sqrt(1.0d0 + 1 / (k * k)) * sqrt(k) ** 3 * sqrt(pi)
       tmp = tmp / (5 * (1 + 9 * k * k) ** 4) * sqrt(CHD_kn(this, w))
    case (CHD_CHA_3d_kf)
       tmp = 15552.0d0 * sqrt(k) ** 7 * sqrt(3 * pi/ 7.0d0)
       tmp = tmp * sqrt(1 + 1.0d0 / (k * k)) * sqrt(4 + 1.0d0 / (k * k)) * sqrt(9 + 1.0d0 / (k * k))
       tmp = tmp / (5 * (1 + 9 * k * k) ** 5) * sqrt(CHD_kn(this, w))
!    case(CHD_CHA_custom)
!       write(*,*) "this is custom channel dipole"
!       write(*,*) "there is no transition amp. in CHD_radial_transition_amp"
!       stop       
    case default
       write (*,*) "mada junbi sitenai. in CHD_radial_transition_amp"
       stop
    end select

    select case (this % dipole)
    case (CHD_DP_length)
       res = tmp
    case (CHD_DP_velocity)
       res = -tmp
    case (CHD_DP_accelate)
       res = tmp
    end select
    
  end function CHD_radial_transition_amp
  function CHD_kn(this, w) result(res)
    use math
    type(CHD_Object), intent(in) :: this
    real*8, intent(in)           :: w
    real*8                       :: res, root
    integer                      :: n0
    select case(this % channel)
    case (CHD_CHA_1s_kp)
       n0 = 1
    case (CHD_CHA_2p_ks)
       n0 = 2
    case (CHD_CHA_2p_kd)
       n0 = 2
    case (CHD_CHA_3d_kp)
       n0 = 3
    case (CHD_CHA_3d_kf)
       n0 = 3
    case (CHD_CHA_2s_kp)
       n0 = 2
!    case (CHD_CHA_custom)
!       write(*,*) "this is custom channel dipole"
!       write(*,*) "there is no kn. in CHD_kn"
!       stop
    end select
    root = sqrt(2.0d0 * n0 * n0 * w - 1.0d0)
    res = exp(-4.0d0 * n0 / root * atan(root)) /&
         (1.0d0 - exp(-2 * pi * n0 / root))
  end function CHD_kn
  !----get other information-----------
  function CHD_matrix_element_of_cos(this) result(res)
    type(CHD_Object)    :: this
    integer             :: l0, l
    real*8              :: res, up, down
!    if(this % channel .eq. CHD_CHA_custom) then
!       write(*,*) "this is custom channel dipole"
!       write(*,*) "there is no <l0|cos|l'0>. in CHD_matrix_element_of_cos"
!       stop
!    end if
    l0 = CHD_l_initial_state(this)
    l  = CHD_l_final_state(this)

    if(l .eq. l0 + 1) then
       up = dble((l0 + 1) ** 2)
       down = dble((2 * l0 + 3) * (2 * l0 + 1))
       res = sqrt(up / down)
    else if(l .eq. l0 - 1) then
       up = dble(l0 ** 2)
       down = dble(4 * l0 * l0 - 1)
       res = sqrt(up / down)
    else
       res = 0.0d0
    end if
  end function CHD_matrix_element_of_cos
  function CHD_get_charge(this) result(res)
    type(CHD_Object)    :: this
    real*8               :: res
    res = this % charge
  end function CHD_get_charge
  !----display status------------------
  subroutine CHD_display(this)
    type(CHD_Object) :: this
    character(6)     :: ch
    character(8)     :: di
    write (*,*) this % channel
    write (*,*) this % dipole
    
    select case (this % channel)
    case (CHD_CHA_1s_kp)
       ch = "1s->kp"
    case (CHD_CHA_2p_ks)
       ch = "2p->ks"
    case (CHD_CHA_2p_kd)
       ch = "2p->kd"       
    case (CHD_CHA_3d_kp)
       ch = "3d->kp"
    case (CHD_CHA_3d_kf)
       ch = "3d->kf"
    case (CHD_CHA_2s_kp)
       ch = "2d->kp"
!    case (CHD_CHA_custom)
!       ch = "custom"
    case default
       write (*,*) "bad data. in CHD_display"
       stop
    end select

    select case (this % dipole)
    case (CHD_DP_length)
       di = "length  "
    case (CHD_DP_velocity)
       di = "velocity"
    case (CHD_DP_accelate)
       di = "accelate"
    case default
       write (*,*) "bad data. in CHD_display"
       stop
    end select

    write (*,*) "#"
    write (*,*) "#-----BEGIN CHD display-----"
    write (*,*) "#channel:", ch
    write (*,*) "#dipole:", di
    write (*,*) "#charge:", this % charge
    write (*,*) "#final l :", CHD_l_final_state(this)
    write (*,*) "#custom? :", this % customQ
    write (*,*) "#have mu0? :", this % have_dipole_initial_stateQ
    if (this % have_dipole_initial_stateQ) then
       call LCCF_display(this % dipole_initial_state)
    end if
    write (*,*) "#-----END CHD display-----"
    write (*,*) "#"
  end subroutine CHD_display
  !-----Check--------------
  subroutine CHD_check_channel(this, from)
    type(CHD_Object), intent(in) :: this
    character(*)                 :: from
    integer                      :: channel
    channel = this % channel
    if((channel < 1 .or. channel > 6) .and. (channel .ne. 100)) then
       write(*,*) "Bad channel. in " // from
       stop
    end if
  end subroutine CHD_check_channel
  subroutine CHD_check_dipole(this, from)
    type(CHD_Object), intent(in) :: this
    character(*)                 :: from
    integer                      :: dipole
    dipole = this % dipole
    if((dipole < 1 .or. dipole > 3) .and. (dipole .ne. 100)) then
       write(*,*) "Bad dipole. in " // from
       stop
    end if
  end subroutine CHD_check_dipole
  !-----test case----------
  subroutine CHD_test_case_read
    type(CHD_Object) :: chd
    chd = CHD_new_from_read()
    call CHD_display(chd)
  end subroutine CHD_test_case_read
  subroutine CHD_test_custom
    type(CHD_Object) :: chd
    integer :: ll
    real*8  :: k
    !    chd = CHD_new_custom(CHD_CHA_1s_kp, (2.0d0, 1.5d0), 2, (1.0d0, -0.5d0), CL2F_CSTO, 1, 1.0d0)
    chd = CHD_new_custom(CHD_CHA_1s_kp, (2.0d0, 1.5d0), 2, (1.0d0, -0.5d0), CL2F_CSTO, 1.0d0)
    write (*,*) "wave number k:", CHD_k_final_state(chd, 0.5d0)
    write (*,*) "energy E:", CHD_energy_final_state(chd, 0.5d0)   
    call CHD_display(chd)    
    ll = CHD_l_initial_state(chd)
    write (*,*) "CHD_l_initial_state", ll

  end subroutine CHD_test_custom
  subroutine CHD_test_1skp
    use module_SIO_simple_inout
    type(CHD_Object) :: chd
    real*8           :: w

    chd = CHD_new(CHD_CHA_1s_kp, CHD_DP_length)
    w = 1.2d0
    write (*,*) "Test for 1s -> kp photoionization of Hydrogen using length form."
    write (*,*) "photon energy w is :", w
!    call SIO_test_value_double( &
!         CHD_coulomb_phase_shift(chd, w), -0.395117d0, "Coulomb phase shift", 1.0d-5)
!    call SIO_test_value_double( &
!         CHD_radial_transition_amp(chd, w), 0.325599d0, "radial transition amp", 1.0d-5)
!    call SIO_test_value_complex( &
!         CHD_first_order_asym(chd, w, 200.0d0), (-0.238726d0, -0.495893d0), &
!         "asymmptotic behavior of first order wave function at 200.0 bor:", 1.0d-5)
  end subroutine CHD_test_1skp
  subroutine CHD_test_1skp_velocity
    use module_SIO_simple_inout
    type(CHD_Object) :: chd
    real*8           :: w
    chd = CHD_new(CHD_CHA_1s_kp, CHD_DP_velocity)
    w = 1.2d0
    write (*,*) "Test for 1s-> kp photoionization of hydrogen using velocity form."
    write (*,*) "photo energy w is:", w
    write(*,*) "first order asym", CHD_first_order_asym(chd, w, 90.0d0)
  end subroutine CHD_test_1skp_velocity
  subroutine CHD_test_2pkd_length
    use module_SIO_simple_inout
    type(CHD_Object) :: chd
    real*8           :: w
    chd = CHD_new(CHD_CHA_2p_kd, CHD_DP_length)
    w = 0.7d0 + 0.125d0
    write (*,*) "Test for 2p -> kd photoionization of Hydrogen using length form."
    write (*,*) "photon energy w is :", w
!    call SIO_test_value_double( &
!         CHD_coulomb_phase_shift(chd, w), -0.794934d0, "Coulomb phase shift", 1.0d-5)
!    call SIO_test_value_double( &
!         CHD_radial_transition_amp(chd, w),0.0855011d0, "radial transition amp", 1.0d-5)
!    call SIO_test_value_complex( &
!         CHD_first_order_asym(chd, w, 100.0d0), (-0.13446d0, 0.0529847d0), &
!         "asymmptotic behavior of first order wave function at 100.0 bor:", 1.0d-5)
  end subroutine CHD_test_2pkd_length
  subroutine CHD_test_custom_read
    type(CHD_Object) :: chd
    chd = CHD_new_from_file(CHD_CHA_1s_kp, 'label.d')
    call CHD_display(chd)
  end subroutine CHD_test_custom_read
  
end module class_CHD_channel_and_dipole
