# Purpose
By this program, you can solve photoionization probrem of hydrogen atom
by the Complex basis function method (CBF). Main calculation is to
find optimized complex orbital exponents of basis functions (STO or GTO).
By connecting WKB solution to CBF solution, you can also calculate the Coulomb phase shift
and asymmetry parameter.
