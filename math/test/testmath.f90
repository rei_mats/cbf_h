program testmath
  use math
  !  call MATH_testcase_a_inverse_b
  call MATH_testcase_sto_gto_integrate
  call MATH_testcase_complex_inverse
  call MATH_erfc_mori_testcase
end program testmath
