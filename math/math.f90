module math
  implicit none
  public
  real(8), parameter :: pi = 3.14159265358979323d0
contains
  !-------------Complex Symmetric matrix calc---------------
  function MATH_complex_matrix_inverse(in,num) result(out)
    integer, intent(in)    :: num
    complex*16, intent(in) :: in(num,num)
    complex*16             :: out(num,num)
    complex*16             :: inc(num,num)
    integer                :: i,j,k
    complex*16             :: tmp_ele
    
    do i = 1, num
       do j = 1, num
          if(i .eq. j) then
             out(i, j) = (1.0d0, 0.0d0)
          else
             out(i, j) = (0.0d0, 0.0d0)
          endif
       enddo
    enddo
    inc(:,:) = in(:,:)
    
    do k = 1, num
       tmp_ele = inc(k,k)
       inc(k,:) = inc(k,:) / tmp_ele
       out(k,:) = out(k,:) / tmp_ele
       
       do i = 1, k - 1
          tmp_ele = inc(i,k)
          inc(i,:) = inc(i,:) - inc(k,:) * tmp_ele
          out(i,:) = out(i,:) - out(k,:) * tmp_ele
       end do
       do i = k + 1, num
          tmp_ele = inc(i,k)
          inc(i,:) = inc(i,:) - inc(k,:) * tmp_ele
          out(i,:) = out(i,:) - out(k,:) * tmp_ele
       end do
    end do
  end function MATH_complex_matrix_inverse
  function MATH_complex_matmul(in1, in2, n) result(out)
    integer, intent(in)    :: n
    complex*16, intent(in) :: in1(n,n), in2(n,n)
    complex*16             :: out(n,n)
    complex*16 tmp
    integer i, j, k
    do i = 1, n
       do j = 1, n
          tmp = (0.0d0, 0.0d0)
          do k = 1, n
             tmp = tmp + in1(i,k) * in2(k,j)
          enddo
          out(i,j) = tmp
       enddo
    enddo
  end function MATH_complex_matmul
  function MATH_complex_a_inverse_b(a, b) result(res)
    complex*16, intent(in)  :: a(:,:)
    complex*16, intent(in)  :: b(:)
    complex*16, allocatable :: c(:,:)
    complex*16              :: res(size(b))
    integer             :: num
    integer             :: k, l
    complex*16              :: pivot, targ
    

    num = size(b)
    if((size(a(1,:)) .ne. num) .or. (size(a(:,1)) .ne. num)) then
       write(*,*) "size a must be equal to size of b. in MATH_a_inverse_b"
       stop
    end if

    allocate(c(num,num+1))
    do k = 1, num
       do l = 1, num
          c(k,l) = a(k,l)
       end do
       c(k,num+1) = b(k)
    end do
    
    do k = 1, num
       pivot = c(k,k)
       c(k,:) = c(k,:) / pivot
       do l = 1, num
          if (l .ne. k) then
             targ = c(l,k)
             c(l,:) = c(l,:) - targ * c(k,:)
          end if
       end do
    end do

    do k = 1, num
       res(k) = c(k, num+1)
    end do
    
  end function MATH_COMPLEX_A_INVERSE_B
  function MATH_complex_mat_a_inverse_mat_b(a, b) result(res)
    complex*16, intent(in)  :: a(:,:)
    complex*16, intent(in)  :: b(:,:)
    complex*16              :: c(size(b),size(b)*2)
    complex*16              :: res(size(b), size(b))
    integer                 :: num
    integer                 :: k, l
    complex*16              :: pivot, targ
    
    num = size(b(1,:))
    
    if((size(a(1,:)) .ne. num) .or. (size(a(:,1)) .ne. num)) then
       write(*,*) "size a must be equal to size of b. in MATH_a_inverse_b"
       stop
    end if

    do k = 1, num
       do l = 1, num
          c(k,l) = a(k,l)
          c(k,num + l) = b(k,l)
       end do
    end do
    
    do k = 1, num
       pivot = c(k,k)
       c(k,:) = c(k,:) / pivot
       do l = 1, num
          if (l .ne. k) then
             targ = c(l,k)
             c(l,:) = c(l,:) - targ * c(k,:)
          end if
       end do
    end do

    do k = 1, num
       do l = 1, num
          res(k,l) = c(k, num + l)
       end do
    end do
  end function MATH_complex_mat_a_inverse_mat_b
  function MATH_complex_vector_product(a,b) result(res)
    complex*16, intent(in) :: a(:), b(:)
    complex*16  :: tmp, res
    integer i, num
    
    num = size(b(:))
    if(size(a(:)) .ne. num) then
       write(*,*) "size a must be equal to size of b. in MATH_complex_vector_product"
       stop
    end if

    tmp = (0.0d0, 0.0d0)
    do i = 1, num
       tmp = tmp + a(i) * b(i)
    end do
    res = tmp

  end function MATH_complex_vector_product
  subroutine MATH_complex_eigenvalue_by_triangle_matrix(mat, n, evalues)
    complex*16, intent(in)     :: mat(n,n)
    complex*16, intent(out)    :: evalues(n)
    integer, intent(in)        :: n
    complex*16                 :: tmpmat(n, n), pivot, target
    integer                    :: i, k      ! index of matrix

    tmpmat(:,:) = mat(:,:)

    do k = 1, n
       pivot = tmpmat(k, k)
       do i = 1, n
          if(i /= k) then
             target = tmpmat(i, k)
             call MATH_complex_mat_row_change(tmpmat, n, k, i, pivot)
          end if
       end do
       evalues(k) = (0.0d0, 0.0d0)
    end do
    
  end subroutine MATH_complex_eigenvalue_by_triangle_matrix
  subroutine MATH_complex_mat_row_change(mat, n, from, to, coe)
    complex*16, intent(inout)     :: mat(n,n)
    integer, intent(in)        :: n, from, to
    complex*16, intent(in)     :: coe
    integer                    :: i
    
    do i = 1, n
       mat(to, i) = mat(to, i) + coe * mat(from, i)
    end do
  end subroutine MATH_complex_mat_row_change
  subroutine MATH_display_complex_matrix(mat)
    complex*16, intent(in) :: mat(:,:)
    integer                :: n, m, i
    n = size(mat, 1)
    m = size(mat, 2)
    do i = 1, n
       write(*,*) mat(i,:)
    end do
  end subroutine MATH_display_complex_matrix
  subroutine MATH_testcase_complex_mat_eigenvalues
    complex*16 in(3,3)
    in(1,1) = (2.0d0,3.0d0)
    in(2,1) = (1.0d0, 0.0d0)
    in(3,1) = (2.0d0, 0.0d0)
    in(3,2) = (-1.0d0, 2.0d0)
    in(1,3) = (-2.0d0, 4.0d0)
    in(2,2) = (0.0d0, 2.0d0)
    in(3,3) = (1.0d0, 1.0d0)
    write(*,*) "before"
    call MATH_display_complex_matrix(in)
    call MATH_complex_mat_row_change(in, 3, 1, 2, (2.0d0, 0.0d0))
    write(*,*) "after"
    call MATH_display_complex_matrix(in)
  end subroutine MATH_testcase_complex_mat_eigenvalues
  subroutine MATH_testcase
    complex*16 in(3,3), out(3,3)
    complex*16 in1(2,2), in2(2,2), out2(2,2)
    in(1,1) = (2.0d0,3.0d0)
    in(2,1) = (1.0d0, 0.0d0)
    in(3,1) = (2.0d0, 0.0d0)
    in(3,2) = (-1.0d0, 2.0d0)
    in(1,3) = (-2.0d0, 4.0d0)
    in(2,2) = (0.0d0, 2.0d0)
    in(3,3) = (1.0d0, 1.0d0)
    out = MATH_complex_matrix_inverse(in,3)
    write(*,*) "in:", in
    write(*,*) "out:", out
    write(*,*) matmul(in,out)
    write(*,*)"---------------------c_matmul-------------------"
    in1(1,1) = (1.0d0, 0.0d0)
    in1(1,2) = (0.0d0, 1.0d0)
    in1(2,1) = (0.0d0, 0.0d0)
    in1(2,2) = (2.0d0, -1.0d0)
    
    in2(1,1) = (-1.0d0, 0.0d0)
    in2(1,2) = (1.0d0, 1.0d0)
    in2(2,1) = (2.0d0, 0.0d0)
    in2(2,2) = (2.0d0, -1.0d0)
    out2 = MATH_complex_matmul(in1, in2, 2)
    write(*,*) "MATH_complex_matmul"
    write(*,*) out2
    write(*,*) "matmul"
    write(*,*) matmul(in1, in2)
    
    write(*,*) "MATH_facfac"
    write(*,*) "MATH_facfac(5):", MATH_facfac(5)
    write(*,*) "MATH_facfac(6):", MATH_facfac(6)

    !    write(*,*) "MATH_integral_gauss_slater(1,2,3)", MATH_integral_gauss_slater(1,(2.0d0,0.0d0), (3.0d0,0.0d0))
    write(*,*) "MATH_sto_gto_integrate(2,(0.5d0, 0.0d0), (0.7d0, -1.2d0), 10)"
    write(*,*) MATH_sto_gto_integrate(2,(0.5d0, 0.0d0), (0.7d0, -1.2d0), 10)
    write(*,*) (0.822396240987783d0, 0.21639245481037d0)
    
    write(*,*) "MATH_sto_gto_integrate(1,(1.0d0,0.0d0),(0.379630,0.06343), 10)"
    write(*,*) MATH_sto_gto_int_big_exponent(1, (0.37963d0, -0.06343d0),10)
    write(*,*) MATH_sto_gto_integrate(1, (1.0d0,0.0d0),(0.37963d0, -0.06343d0),10)
    write(*,*) MATH_sto_gto_int_big_exponent(1, (0.5d0, -0.5d0),10)
    write(*,*) MATH_sto_gto_integrate(1, (1.0d0, 0.0d0),(0.5d0, -0.5d0),10)
    write(*,*) MATH_sto_gto_int_big_exponent(1, (0.5d0, -0.6d0),10)
    write(*,*) MATH_sto_gto_integrate(1, (1.0d0, 0.0d0), (0.5d0, -0.6d0),10)
    write(*,*) MATH_sto_gto_int_big_exponent(1, (0.5d0, -0.4d0),10)
    write(*,*) MATH_sto_gto_integrate(1, (1.0d0, 0.0d0), (0.5d0, -0.4d0),10)
write(*,*) "pn = 2"
    write(*,*) MATH_sto_gto_int_big_exponent(2, (0.5d0, -5d0),10)
    write(*,*) MATH_sto_gto_integrate(2, (1.0d0,0.0d0),(0.5d0, -5d0),10)
    write(*,*) MATH_sto_gto_int_big_exponent(2, (0.5d0, -0.6d0),10)
    write(*,*) MATH_sto_gto_integrate(2, (1.0d0,0.0d0),(0.5d0, -0.6d0),10)
    write(*,*) MATH_sto_gto_int_big_exponent(2, (0.5d0, -0.4d0),10)
    write(*,*) MATH_sto_gto_integrate(2, (1.0d0, 0.0d0),(0.5d0, -0.4d0),10)
    write(*,*) MATH_sto_gto_int_big_exponent(2, (5.0d0, -5.0d0),10)
    write(*,*) MATH_sto_gto_integrate(2,(1.0d0,0.0d0), (5.0d0, -5.0d0),10)
write(*,*) "mid pn = 2"
    write(*,*) MATH_sto_gto_integrate(2,(1.0d0,0.0d0), (0.2d0, -0.001d0),10)
    write(*,*) MATH_sto_gto_integrate(3,(1.0d0,0.0d0), (0.2d0, -0.001d0),10)
    write(*,*) MATH_sto_gto_integrate(3,(1.0d0,0.0d0), (0.06756565d0, -0.1444052),10)
    write(*,*) MATH_sto_gto_integrate(2,(1.0d0,0.0d0), (0.06756565d0, -0.1444052),10)
    write(*,*) MATH_sto_gto_integrate(4,(1.0d0,0.0d0), (0.06756565d0, -0.1444052),10)
    write(*,*) MATH_sto_gto_integrate(5,(1.0d0,0.0d0), (0.06756565d0, -0.1444052),10)
    write(*,*) MATH_sto_gto_integrate(6,(1.0d0,0.0d0), (0.06756565d0, -0.1444052),10)
    write(*,*) MATH_sto_gto_integrate(7,(1.0d0,0.0d0), (0.06756565d0, -0.1444052),10)
    write(*,*) MATH_sto_gto_integrate(3,(1.0d0,0.0d0), (0.001d0, -0.2d0),10)


  end subroutine MATH_testcase
  subroutine MATH_testcase_complex_inverse
    complex*16 a(2,2), b(2,2), c(2,2), d(2,2)
    write (*,*) "-----MATH_testcase_complex_inverse-----"
    a(1,1) = (2.0d0,3.0d0)
    a(2,1) = (1.0d0, 0.0d0)
    a(1,2) = (1.0d0, 0.0d0)
    a(2,2) = (0.0d0, 2.0d0)

    b(1,1) = (2.0d0,1.0d0)
    b(2,1) = (1.0d0, 1.0d0)
    b(1,2) = (1.0d0, -1.0d0)
    b(2,2) = (0.5d0, 1.0d0)

    write (*,*) "a"
    write (*,*) a(1,:)
    write (*,*) a(2,:)

    write (*,*) "b"
    write (*,*) b(1,:)
    write (*,*) b(2,:)

    write (*,*) "c = a^-1 b"
    c = MATH_complex_mat_a_inverse_mat_b(a, b)
    write (*,*) c(1,:)
    write (*,*) c(2,:)

    write (*,*) "d = a c"
    d = matmul(a, c)
    write (*,*) d(1,:)
    write (*,*) d(2,:)
  end subroutine MATH_testcase_complex_inverse
  !------------Complex function-------------
  ! calculation of exp(i z)
  function MATH_expi(z) result(res)
    complex*16, intent(in) :: z
    complex*16  :: res
    res = exp(cmplx(0.0d0, 1.0d0) * z)
  end function MATH_expi
  !-------------Real matrix---------------------------------
  function MATH_a_inverse_b(a, b) result(res)
    real*8, intent(in)  :: a(:,:)
    real*8, intent(in)  :: b(:)
    real*8, allocatable :: c(:,:)
    real*8              :: res(size(b))
    integer             :: num
    integer             :: k, l
    real*8              :: pivot, targ
    

    num = size(b)
    if((size(a(1,:)) .ne. num) .or. (size(a(:,1)) .ne. num)) then
       write(*,*) "size a must be equal to size of b. in MATH_a_inverse_b"
       stop
    end if

    allocate(c(num,num+1))
    do k = 1, num
       do l = 1, num
          c(k,l) = a(k,l)
       end do
       c(k,num+1) = b(k)
    end do
    
    do k = 1, num
       pivot = c(k,k)
       c(k,:) = c(k,:) / pivot
       do l = 1, num
          if (l .ne. k) then
             targ = c(l,k)
             c(l,:) = c(l,:) - targ * c(k,:)
          end if
       end do
    end do

    do k = 1, num
       res(k) = c(k, num+1)
    end do
    
  end function MATH_a_inverse_b
  subroutine MATH_testcase_a_inverse_b
    integer,parameter :: num =2
    real*8            :: a(num,num), b(num)

    a(1,1) = 3.0d0
    a(2,1) = 1.0d0
    a(1,2) = 1.0d0
    a(2,2) = -2.0d0
    b(1) = 9.0d0
    b(2) = -4.0d0
    write(*,*) MATH_a_inverse_b(a,b)

  end subroutine MATH_testcase_a_inverse_b
  !-------------Newton-Raphson-------------------------------
  subroutine MATH_newton_raphson_onestep(xk, xkp, hess, grad, num)
    integer, intent(in) :: num
    complex*16 :: xk(num)
    complex*16 :: xkp(num)
    complex*16 :: hess(num,num), grad(num)
    complex*16 :: hess_inv(num,num)
!    hess_inv =  MATH_complex_matrix_inverse(hess, num)
    !    function MATH_complex_a_inverse_b(a, b) result(res)
    !    xkp = xk - matmul(hess_inv, grad)
    xkp = xk - MATH_complex_a_inverse_b(hess, grad)
  end subroutine MATH_newton_raphson_onestep
  !f(x,y) = x^4 + (y-1)^2
  subroutine MATH_nr_test
    implicit none
    integer,parameter :: num = 2
    complex*16 :: xk(num)
    complex*16 :: xkp(num)
    complex*16 x, y
    complex*16 hess(num,num), grad(num)
    real(8), parameter :: eps = 0.001d0
    integer i, j, k
    xk = (/ (1.0d0, -1.0d0), (2.0d0, -2.0d0) /)
    
    do k = 1, 100
       x = xk(1)
       y = xk(2)
       write(*,*) "(k,x,y)", k, x, y
       grad(1) = 4.0d0 * x**3 
       grad(2) = 2.0d0 * (y - 1.0d0)
       hess(1,1) = 12.0d0 * x**2 
       hess(1,2) = 0.0d0
       hess(2,1) = 0.0d0
       hess(2,2) = 2.0d0
       call MATH_newton_raphson_onestep(xk, xkp, hess, grad, num)
       xk = xkp
    enddo
  end subroutine MATH_nr_test
  !-------------Easy Math functions-------------------------
  function MATH_fac(n)result(res)
    integer, intent(in) :: n
    integer i, tmp, res
    if(n .eq. 0) then
       res = 1
    else if(n < 0) then
       write(*,*) "n must bigger than equal 0 in MATH_fac"
       stop
    else
       tmp = 1
       do i = 1, n
          tmp = i * tmp
       enddo
       res = tmp
    endif
  end function MATH_fac
  function MATH_facfac(n) result(res)
    integer, intent(in) :: n
    integer             :: res, i, tmp, num
    
    if( n .eq. 0) then
       res = 1
    else if (n < 0) then
       write(*,*) "n must bigger than equal 0 in MATH_facfac"
       stop
    else
       num = n / 2
       tmp = 1
       do i = 1, num
          tmp = tmp * (n - i * 2 + 2)
       end do
       res = tmp
    end if
  end function MATH_facfac
  function MATH_C(n,k) result(res)
    integer, intent(in)  :: n,k
    integer              :: res
    res = MATH_fac(n) / (MATH_fac(k) * MATH_fac(n - k))
  end function MATH_C
  !-------------special function----------------------------
  function MATH_erfc_mori(t, h, n) result(res)
    !calculate erfc(t) by Mori's method
    complex*16, intent(in) :: t
    real*8, intent(in)     :: h
    integer, intent(in)    :: n
    complex*16             :: res
    complex*16             :: fh, rt, tmp, nh2
    integer                :: i

    tmp = (0.0d0, 0.0d0)
    do i = 1, n
       nh2 = n * n * h * h
       tmp = tmp + exp(-nh2) / (nh2 + t * t)
    end do
    fh = 2 * t * h / pi * exp(-t*t) * (1.0d0 / (2.0d0 * t * t) + tmp)

    if(0 < real(t) .and. real(t) < abs(aimag(t)) + pi / h) then
       res = fh - 2.0d0 / (exp(2 * pi * t / h) - 1.0d0)
    else
       res = fh
    end if
  end function MATH_erfc_mori
  subroutine MATH_erfc_mori_testcase
    write (*,*) ""
    write (*,*) "-----BEGIN erfc test------"
    write (*,*) MATH_erfc_mori((0.01d0, 0.0d0), 0.5d0 ,13), erfc(0.01d0)
  end subroutine MATH_erfc_mori_testcase
  !-------------Integrate-----------------------------------
  ! <l1m1|cos \theta|l2m2>
  function MATH_matrix_ele_cos(l1,m1,l2,m2) result(res)
    integer, intent(in) :: l1, m1, l2, m2
    real*8              :: res, up, down
    if(m1 .eq. m2) then
       if(l1 .eq. l2 + 1) then
          up = dble((l2 + 1) ** 2 - m2 ** 2)
          down = dble((2 * l2 + 3) * (2 * l2 + 1))
          res = sqrt(up / down)
       else if(l1 .eq. l2 - 1) then
          up = dble(l2 ** 2 - m2 ** 2)
          down = dble(4 * l2 * l2 - 1)
          res = sqrt(up / down)
       else
          res = 0.0d0
       end if
    else
       res = 0.0d0
    end if
  end function MATH_matrix_ele_cos
  ! Integrate 0 to Infinity x^m exp(-z r) dr
  function MATH_slater_integral(m, z) result(res)
    integer, intent(in)    :: m
    complex*16, intent(in) :: z
    complex*16             :: res
    res = MATH_fac(m) / (z ** (m + 1))
  end function MATH_slater_integral
  function MATH_gauss_integral(m ,z) result(res)
    !value of Integrate_0^Infinity dr r^m Exp(-z r^2)
    integer, intent(in)    :: m
    complex*16, intent(in) :: z
    complex*16             :: res
    integer                :: n
    n = m / 2
    if(m .eq. 0) then
       res = sqrt(pi / (4.0d0 * z))
    else if(mod(m,2) .eq. 0) then
       res = MATH_facfac(2 * n - 1) / (2 ** (n + 1) * z ** n) * sqrt(pi / z)
    else
       res = MATH_fac(n) / (2.0d0 * z ** (n + 1))
    end if
  end function MATH_gauss_integral
  !k th term of (a+b)^n
  function MATH_two_term_theorem(a,b,n,k) result(res)
    complex*16, intent(in) :: a, b
    integer, intent(in)    :: n, k
    complex*16             :: res
    res = a**k * b**(n-k) * MATH_fac(n) / (MATH_fac(k) * MATH_fac(n-k))
  end function MATH_two_term_theorem
  
  ! int_0^Infinity r^m Exp(-a r) Exp(- b r^2)
  function MATH_integral_gauss_slater(m, a, b) result(res)
    integer, intent(in)    :: m
    complex*16, intent(in) :: a, b
    complex*16             :: res, tmp, term
    integer i
    tmp = cmplx(0.0d0, 0.0d0)
    do i = 0, m
       term = MATH_two_term_theorem((1.0d0,0.0d0),-0.5d0*a/b, m, i)
!       term = term * sqrt(pi) * (1 - erf(sqrt(b) * a /(2*b))) / (2 * sqrt(b))
       tmp = tmp + term
    end do
res = tmp
  end function MATH_integral_gauss_slater
  ! integrate_0^Infinity r^n Exp(-alphas x) Exp(-ag x x)
  function MATH_sto_gto_integrate(n, alphas, ag, loop_num) result(res)
    integer, intent(in) :: n         ! summation of principle number of GTO and STO
    integer, intent(in) :: loop_num  ! number of loop
    complex*16, intent(in)  :: alphas    ! exponent of STO
    complex*16, intent(in)  :: ag    ! exponent of GTO
    complex*16          :: res       ! result
    !write(*,*) "as", alphas
    res = MATH_unit_sto_gto_integrate(n, ag / (alphas * alphas),loop_num) &
         / (real(alphas) ** (n + 1))
  end function MATH_sto_gto_integrate
  !integrate 0 to infinity r^n Exp(-r-alpha r r) dr for all range. sto_gto_integrate
  function MATH_unit_sto_gto_integrate(n, alpha, loop_num) result(res)
    integer, intent(in) :: n         ! principle number of GTO
    integer, intent(in) :: loop_num  ! number of loop
    complex*16, intent(in)  :: alpha     ! exponent
    complex*16          :: res, tmp       ! result
!    write(*,*) "abs", abs(alpha)
    if(abs(alpha) < 1.0d-3) then
       tmp = MATH_sto_gto_int_small_exponent(n, alpha, loop_num)
    else if(abs(alpha) < 0.3d0) then
       tmp = MATH_sto_gto_int_mid_exponent(n, alpha)
    else
       tmp = MATH_sto_gto_int_big_exponent(n, alpha, loop_num)
    end if
    res = tmp
  end function MATH_unit_sto_gto_integrate
  !integrate 0 to infinity r^n Exp(-r-alpha r r) dr for small alpha
  function MATH_sto_gto_int_small_exponent(n, alpha, loop_num) result(res)
    integer, intent(in) :: n         ! summation of principle number of GTO and STO
    integer, intent(in) :: loop_num  ! number of loop
    complex*16, intent(in)  :: alpha ! exponent
    complex*16          :: res       ! result
    complex*16          :: tmp, tmp1, tmp2, tmp3, tmp4
    integer             :: k
    tmp = (0.0d0, 0.0d0)
    do k = 0, loop_num
       tmp1 = (-1)**k
       tmp2 = MATH_fac(n + 2 * k) / MATH_fac(k)
       tmp3 = alpha**k
       if(abs(tmp1 + tmp2 + tmp3) < 1.0d-10) then
          exit
       else
          tmp = tmp + tmp1 * tmp2 * tmp3
       end if
    end do
    res = tmp
  end function MATH_sto_gto_int_small_exponent
  !integrate 0 to infinity r^(n) Exp(-r-alpha r r) dr for big alpha
  function MATH_sto_gto_int_big_exponent(n, alpha, loop_num) result(res)
    integer, intent(in)     :: n         ! summation of principle number of GTO and STO
    integer, intent(in)     :: loop_num  ! number of loop
    complex*16, intent(in)  :: alpha     ! exponent
    complex*16              :: phi0,phi2m,phi2mp1, norm_slater, norm_gauss
    complex*16              :: tmp, t, res, tmp1, tmp2
    integer                 :: k, i, m
    t = 1 / (4.0d0 * alpha)
    ! Calculation of phi0
    tmp = sqrt(pi * t) * exp(t)
    do k = 1, loop_num
       tmp1 = (2 * t) ** k
       tmp1 = tmp1 / dble(MATH_facfac(2 * k - 1))
       if(abs(tmp1) < 1.0d-15) then
          exit
       else
          tmp = tmp -  tmp1
       end if
    end do
    phi0 = tmp
    phi2m = phi0
    tmp = (0.0d0, 0.0d0)
    do k = 0, n
       t = 1.0d0 / (4 * alpha)
       if(mod(k, 2) .eq. 0) then
          tmp = tmp + (-1) ** (n - k) * MATH_C(n, k) * phi2m
          phi2m = (k + 1) * phi2m / (2 * t) + 1
       else
          m = (k - 1) / 2
          phi2mp1 = (0.0d0, 0.0d0)
          do i = 0, m
             phi2mp1 = phi2mp1 + MATH_fac(m) / MATH_fac(m-i) / (t ** i)
          end do
          tmp = tmp + (-1) ** (n - k) * MATH_C(n, k) * phi2mp1
       end if
    end do
    res = tmp / (2 * alpha) ** n
    
    ! write(*,*) "phi0", phi0

    
!    norm_gauss = 2 ** (ng + 1) * 1 / sqrt(dble(MATH_facfac(2 * ng - 1)))
!    norm_gauss = norm_gauss / (2 * pi) ** 0.25d0 * (alpha ** (0.25d0)) ** (2 * ng + 1)
!    norm_slater = 1 / sqrt(dble(MATH_fac(2 * ns))) * sqrt(2.0d0) ** 3
!    if((ns .eq. 1) .and. (ng .eq. 1)) then
!       res = 2 / (2 * pi) ** 0.25d0 * (2*alpha*phi0-1+phi0) / alpha ** (1.25d0)
!       res = res / (norm_slater * norm_gauss)
!    else if( (ns .eq. 2) .and. (ng .eq. 2)) then
!       tmp = 1.0d0 / (3 * sqrt(10.0d0)) / (2 * pi) ** 0.25d0
!       tmp = tmp * ((2 * alpha + 12 * alpha * alpha) * phi0 - (1 + 10 * alpha) * (1 - phi0)) / (alpha ** 0.25d0) ** 13
!       res = sqrt(10.0d0) * tmp * sqrt(alpha)
!    end if
  end function MATH_sto_gto_int_big_exponent
  !integrate 0 to infinity r^(ns + ng) Exp(-r-alpha r r) dr for midium alpha
  function MATH_sto_gto_int_mid_exponent(n, alpha) result(res)
    integer,     intent(in) :: n         ! summation of principle number of GTO and STO
    complex*16, intent(in)  :: alpha     ! exponent
    integer, parameter  :: laguerre_num = 60
    !   complex*16          :: tmp,  term, root
    complex*16          :: res
    real*8              :: delta
    integer             :: i
    integer             :: num
    real*8              :: x, t
    complex*16          :: ctemp, fi, wi, fiwi
!    integer             :: k
    real*8              :: zero_points(laguerre_num)
    real*8              :: weight_factors(laguerre_num)
    delta = 0.05d0
    ctemp = (0.0d0, 0.0d0)
    num = 100
    do i = -num, num
       t = delta * i
       x = exp(pi * 0.5d0 * (t - exp(-t)))
       fi = exp(-x) * exp(-alpha * x * x) * x ** n

!       fi = exp(-x) * exp(-x * x)
       wi = (1 + exp(-t)) * x
       fiwi = fi * wi * pi * delta * 0.5d0
!       write(*,*) "t,fi*wi,ctemp", t, fiwi, ctemp
       ctemp = ctemp + fiwi
    end do
!    write(*,*) "mahi"
    res = ctemp

!    zero_points = (/ 0.059019852181507976973d0, 0.31123914619848372686d0, &
!         0.76609690554593664624d0, 1.4255975908036130863d0, 2.2925620586321902950d0, &
!         3.3707742642089977162d0, 4.6650837034671707921d0, 6.1815351187367654118d0, &
!         7.9275392471721521794d0, 9.9120980150777060188d0, 12.146102711729765550d0, &
!         14.642732289596674342d0, 17.417992646508978726d0, 20.491460082616424707d0, &
!         23.887329848169733175d0, 27.635937174332717420d0, 31.776041352374723288d0, &
!         36.358405801651621653d0, 41.451720484870767051d0, 47.153106445156323041d0, &
!         53.608574544695069824d0, 61.058531447218761564d0, 69.962240035105030431d0, &
!         81.498279233948885377d0 /)
!    weight_factors = (/ 0.14281197333478185108d0, 0.25877410751742390266d0, &
!         0.25880670727286980203d0, 0.18332268897777802489d0, &
!         0.09816627262991889219d0, 0.04073247815140864602d0, &
!         0.01322601940512015669d0, 0.003369349058478303552d0, &
!         0.0006721625640935478900d0, 0.0001044612146592751805d0, &
!         0.00001254472197799333321d0, 1.151315812737279920d-6, &
!         7.96081295913363026d-8, 4.07285898754999967d-9, &
!         1.507008226292584916d-10, 3.91773651505845139d-12, &
!         6.89418105295808569d-14, 7.81980038245944847d-16, &
!         5.35018881301003760d-18, 2.01051746455550347d-20, &
!         3.60576586455295904d-23, 2.45181884587840269d-26, &
    !         4.08830159368065782d-30, 5.57534578832835675d-35 /)
    weight_factors = (/ &
0.05988361152373338066d0, 0.12591096707540107193d0, &
0.16473078908210712807d0, 0.17239118732674750403d0, &
0.15442926800152203448d0, 0.12180351302605122744d0, &
0.08580797687984670328d0, 0.05443536722648374682d0, &
0.03125278975222337197d0, 0.01629025904763515578d0, &
0.007724745660508856773d0, 0.003336689494307682016d0, &
0.001313865863449651536d0, 0.0004717887261058255260d0, &
0.0001545007363460515129d0, 0.00004613363029151373403d0, &
0.00001255554158643320808d0, 3.112653611667138625d-6, &
7.02389231692295635d-7, 1.441394233872203205d-7, &
2.687115538247873826d-8, 4.54532404657116922d-9, &
6.96674608705399402d-10, 9.66111905164919058d-11, &
1.210137290201255147d-11, 1.366650897183087916d-12, &
1.388758356874082174d-13, 1.267044092734974907d-14, &
1.035418385014632543d-15, 7.55907205833704921d-17, &
4.91605568323678609d-18, 2.83933159577619797d-19, &
1.45143796449501839d-20, 6.54273502009292603d-22, &
2.59025190968330595d-23, 8.9664278435414917d-25, &
2.70067800927502176d-26, 7.03988949156214095d-28, &
1.57875478537644719d-29, 3.02587768945848349d-31, &
4.92016735525663888d-33, 6.7316641160505118d-35, &
7.6780965388276276d-37, 7.2246694240103543d-39, &
5.5415003983611333d-41, 3.41766012790814309d-43, &
1.66814952203784522d-45, 6.3255732716007591d-48, &
1.82313963858143672d-50, 3.89065966922280023d-53, &
5.9551615457676982d-56, 6.2854492261473107d-59, &
4.3523952400430152d-62, 1.85335648498691025d-65, &
4.4482734830374031d-69, 5.3225663149557690d-73, &
2.64120678052246112d-77, 4.0165058425505469d-82, &
1.05169410392014721d-87, 1.09094194862482007d-94 /)
    zero_points = (/ &
0.023897977262724994782, 0.12593471888169076076, &
0.30957893432678988075, 0.57499554209280526609, &
0.92236948211666379157, 1.3519383600081679397, 1.8639963442992054802, &
2.4588958438224285850, 3.1370490097858959310, 3.8989293872049917811, &
4.7450738001258887621, 5.6760845082469168260, 6.6926316627865745904, &
7.7954560890310120109, 8.9853724256576560911, 10.263272655037909547, &
11.630130063841871782, 13.087003679350245100, 14.635043234018346591, &
16.275494719209406879, 18.009706598857114263, 19.839136765434035716, &
21.765360334373534885, 23.790078389494180642, 25.915127811604900409, &
28.142492346079812161, 30.474315093739509195, 32.912912644080369003, &
35.460791112322411216, 38.120664393927127573, 40.895475014812933693, &
43.788418035940638635, 46.802968571856479942, 49.942913610317752243, &
53.212388982588307206, 56.615922542696984717, 60.158484884500430204, &
63.845549279532241991, 67.683162987059545208, 71.678032714447406955, &
75.837627854657063291, 80.170306292607892439, 84.685469194509277238, &
89.393753490252791880, 94.307274066118867443, 99.439932542889869648, &
104.80781680747746147, 110.42972668651628922, 116.32787889753132982, &
122.52887338413981661, 129.06505218529826502, 135.97646860411319811, &
143.31384526024606358, 151.14321669561511669, 159.55362523885103398, &
168.67080654892222049, 178.68392501314637910, 189.90524696213377268, &
202.93398795040067732, 219.31811577379970553 /)    
    
!    tmp = 0.0d0
!    do k = 1, laguerre_num
!       root = sqrt(1 + 4 * alpha * zero_points(k))
!!       write(*,*) "1+4ax,sqrt(1+4ax)", 1 + 4 * alpha * zero_points(k), root
!       term = weight_factors(k) / root * ( (root - 1) ** n * 1.0d0 / (2 * alpha) ** n) 
!       tmp = tmp + term
!    end do
!    do k = 1, laguerre_num
!       x = zero_points(k)
!       term = weight_factors(k) * x ** n * exp(-alpha * x * x)
!       tmp = tmp + term
    !    end do


  end function MATH_sto_gto_int_mid_exponent
  !TESTCASE for sto_gto_integrate
  subroutine MATH_testcase_sto_gto_integrate
    
    integer    :: n1 = 2, loop_num
    complex*16 :: zeta_gto = (0.1d0, 0.0d0)
    loop_num = 10
    write (*,*) "Integrate_0^(Infinity) Exp[-x] Exp[-0.1x^2] dx"
    write(*,*) MATH_unit_sto_gto_integrate(n1, zeta_gto, loop_num)
    zeta_gto = (0.1d0, 0.1d0)
    write (*,*) "Integrate_0^(Infinity) Exp[-x] Exp[-(0.1+0.1i)x^2] dx"
    write(*,*) MATH_unit_sto_gto_integrate(n1, zeta_gto, loop_num)
    zeta_gto = (0.07d0, -0.11d0)
    write (*,*) "Integrate_0^(Infinity) Exp[-x] Exp[-(0.07-0.11i)x^2] dx"
    write(*,*) MATH_unit_sto_gto_integrate(n1, zeta_gto, loop_num)
    write (*,*) "Integrate_0^(Infinity) Exp[-x] Exp[-(0.02-0.01i)x^2] dx"
    zeta_gto = (0.02d0, -0.01d0)
    write(*,*) MATH_unit_sto_gto_integrate(n1, zeta_gto, loop_num)

    zeta_gto = (0.6220044934d0,  -0.8670110555d0)
    write (*,*) "Integrate_0^(Infinity) Exp[-x] Exp[-zx^2] dx, z=", zeta_gto
    write(*,*) MATH_unit_sto_gto_integrate(n1, zeta_gto, loop_num)

    zeta_gto = (0.1441712619d0,  -0.2402302952d0)
    write (*,*) "Integrate_0^(Infinity) Exp[-x] Exp[-zx^2] dx, z=", zeta_gto
    write(*,*) MATH_unit_sto_gto_integrate(n1, zeta_gto, loop_num)
        
    zeta_gto = (2.9571295028d0,  -3.9071570810d0)
    write (*,*) "Integrate_0^(Infinity) Exp[-x] Exp[-zx^2] dx, z=", zeta_gto
    write(*,*) MATH_unit_sto_gto_integrate(n1, zeta_gto, loop_num)
    
!    write(*,*) erfc(zeta_gto)
    

  end subroutine MATH_testcase_sto_gto_integrate
! ac-sources
end module math


