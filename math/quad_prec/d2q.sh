sed -i s/"real\*8"/"real\*16"/g $1
sed -i s/"complex\*16"/"complex\*32"/g $1
sed -i s/"\([0-9]\)d\([0-9]\)"/"\1q\2"/g $1
sed -i s/"\([0-9]\)d-\([0-9]\)"/"\1q-\2"/g $1
