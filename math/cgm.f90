module class_CGM_complex_givens_method
  implicit none

  
  public :: CGM_new, CGM_delete, CGM_get_eigenvalues, CGM_get_coefficients, &
       CGM_solve_eigenvalue, CGM_print

  public
  type CGM_Object
     private
     integer                 :: num
     complex*16, allocatable :: hmat(:)
     complex*16, allocatable :: smat(:)
     complex*16, allocatable :: eigenvalue_array(:)
     complex*16, allocatable :: cmat(:,:)
     logical                 :: is_done
     logical                 :: is_hc_sce
  end type CGM_Object
  
  integer, parameter      :: max_num = 40
  integer, parameter      :: max_num2 = 1600

contains

  !--------Constructor-----------------
  function CGM_new(in_hmat, in_smat) result(res)
    complex*16, intent(in) :: in_hmat(:,:)
    complex*16, intent(in), optional :: in_smat(:,:)
    type(CGM_Object)       :: res
    integer                :: n, i, j, ij

    n = size(in_hmat(1,:))

    if( n > max_num) then
       write(*,*) "max size is 40"
       stop
    end if
    res % num = n
    allocate(res % hmat(n * (n + 1) / 2))
    allocate(res % smat(n * (n + 1) / 2))
    allocate(res % eigenvalue_array(n))
    allocate(res % cmat(n,n))
    
    res % is_hc_sce = present(in_smat)
    
    ij = 0
    do i = 1, n
       do j = 1, i
          ij = ij + 1
          res % hmat(ij) = in_hmat(i, j)
          if(present(in_smat)) then
             res % smat(ij) = in_smat(i, j)
          else
             res % smat(ij) = (0.0d0, 0.0d0)
          end if
       end do
    end do
    res % is_done = .false.
!    write (*,*) res % hmat
            
  end function CGM_new
  !--------Destructor------------------
  subroutine CGM_delete(cgm)
    type(CGM_Object), intent(inout) :: cgm
    deallocate(cgm % hmat)
    deallocate(cgm % smat)
    deallocate(cgm % eigenvalue_array)
    deallocate(cgm % cmat)
  end subroutine CGM_delete
  !--------Getter----------------------
  function CGM_get_eigenvalues(cgm) result(res)
    type(CGM_Object), intent(in) :: cgm
    complex*16, allocatable      :: res(:)
    allocate(res(cgm % num))
    if(cgm % is_done) then
       res(:) = cgm % eigenvalue_array(:)
    else
       write(*,*) "this object must calculate. in CGM_get_eigenvalues"
    end if
  end function CGM_get_eigenvalues
  function CGM_get_coefficients(cgm) result(res)
    type(CGM_Object), intent(in) :: cgm
    complex*16, allocatable      :: res(:,:)
    integer                      :: n
    n = cgm % num
    allocate(res(n, n))
    if(cgm % is_done) then
       res(:,:) = cgm % cmat(:,:)
    else
       write(*,*) "this object must calculate. in CGM_get_coefficients"
    end if
  end function CGM_get_coefficients
  !--------Solve------------------
  subroutine CGM_solve_eigenvalue(cgm)
    type(CGM_Object), intent(inout) :: cgm
    complex*16              :: xmat(max_num,max_num)
    complex*16              :: umat(max_num,max_num)
    complex*16              :: tmpmat(max_num, max_num)
    complex*16              :: scratch(max_num, max_num)
    complex*16              :: rot_hmat(max_num2)
    complex*16              :: ctmp, eig
    integer                 :: i, j, k, num, ijsub

    if(.not. cgm % is_hc_sce) then
       write (*,*) "no smat. in CGM_solve_eigenvalue"
    end if

    num = cgm % num
    call GIVENS(num, num, max_num, &
          cgm % smat, scratch, cgm % eigenvalue_array, umat, num)
    call ORDABS(cgm % eigenvalue_array, umat, num, max_num, -1)
     
    do i = 1, num
       do j = 1, num
          eig = cgm % eigenvalue_array(j)
          xmat(i, j) = umat(i, j) / sqrt(eig)
       end do
    end do

    do i = 1, num
       do k = 1, num
          ctmp = (0.0q0, 0.0q0)
          do j = 1, num
             ijsub = i * (i - 1) / 2 + j
             if(i .lt. j) then
                ijsub = j * (j - 1) / 2 + i
             end if
             ctmp = ctmp + cgm % hmat(ijsub) * xmat(j, k)
          end do
          tmpmat(i, k) = ctmp
       end do
    end do
    ijsub = 0
    do i = 1, num
       do j = 1, i
          ijsub = ijsub + 1
          ctmp = (0.0q0, 0.0q0)
          do k = 1, num
             ctmp = ctmp + xmat(k, i) * tmpmat(k, j)
          end do
          rot_hmat(ijsub) = ctmp
       end do
    end do
    call GIVENS(num, num, max_num, &
         rot_hmat, scratch, cgm % eigenvalue_array, umat, num)
    call ORDABS(cgm % eigenvalue_array, umat, num, max_num, -1)


    do i = 1, num
       do j = 1, num
          ctmp = (0.0q0, 0.0q0)
          do k = 1, num
             ctmp = ctmp + xmat(i,k) * umat(k,j)
          end do
          cgm % cmat(i,j) = ctmp
       end do
    end do
    cgm % is_done = .true.
  end subroutine CGM_solve_eigenvalue
  subroutine CGM_solve_eigenvalue_hc_ec(cgm)
    type(CGM_Object), intent(inout) :: cgm
    complex*16              :: umat(max_num,max_num)
    complex*16              :: scratch(max_num, max_num)
    complex*16              :: eigs(max_num)
    integer                 :: num
    num = cgm % num
    call GIVENS(num, num, max_num, &
          cgm % hmat, scratch, eigs, umat, num)
    call ORDABS(eigs, umat, num, max_num, -1)

    cgm % cmat(:,:) = umat(1:num,1:num)
    cgm % eigenvalue_array(1:num) = eigs(1:num)
    cgm % is_done = .true.
  end subroutine CGM_solve_eigenvalue_hc_ec
  !--------Matrix-----------------
  function CGM_matrix_from_array(array, num) result(res)
    complex*16, intent(in) :: array(:)
    integer, intent(in)    :: num
    complex*16             :: res(num, num)
    integer                :: i, j, ij
    do i = 1, num
       do j = 1, num
          ij = i * (i - 1) / 2 + j
          res(i,j) = array(ij)
          res(j,i) = array(ij)
       end do
    end do
  end function CGM_matrix_from_array
  !--------Print------------------
  subroutine CGM_print(cgm)
    type(CGM_Object), intent(in) :: cgm
    integer                      :: i
    complex*16                   :: tmpmat(cgm % num, cgm % num)

    write (*,*) "num:", cgm % num

    write(*,*) "hmarix"
    tmpmat = CGM_matrix_from_array(cgm % hmat, cgm % num)
    do i = 1, cgm % num
       write(*,*) tmpmat(i,:)
    end do
    write (*,*) "smatrix"
    tmpmat = CGM_matrix_from_array(cgm % smat, cgm % num)
    do i = 1, cgm % num
       write(*,*) tmpmat(i,:)
    end do
    write(*,*) "eigen value"
    do i = 1, cgm % num
       write(*,*) cgm % eigenvalue_array(i)
    end do
    write (*,*) "unitary matrix"
    do i = 1, cgm % num
       write(*,*) cgm % cmat(i,:)
    end do
  end subroutine CGM_print
  !--------Test case----------------
  subroutine CGM_testcase_heh()
    type(CGM_Object)  :: cgm
    complex*16        :: hmat(2,2), smat(2,2)
    complex*16        :: left(2,2), right(2,2)
    complex*16        :: eigmat(2,2), cmat(2,2)
    integer i

    !----set test data----------
    smat(1,1) = (1.0q0, 0.0q0)
    smat(1,2) = (0.4508q0, 0.1q0)
    smat(2,1) = (0.4508q0, 0.1q0)
    smat(2,2) = (1.0q0, -0.2q0)
    hmat(1,1) = (-2.6527q0, 0.0q0)
    hmat(1,2) = (-1.3472q0, 0.1q0)
    hmat(2,1) = (-1.3472q0, 0.1q0)
    hmat(2,2) = (-1.7318q0, 0.4q0)
    cgm = CGM_new(hmat, smat)

    !----calc and print----------
    call CGM_solve_eigenvalue(cgm)
    call CGM_print(cgm)

    !----get result-------------
    eigmat(1,1) = cgm % eigenvalue_array(1)
    eigmat(2,2) = cgm % eigenvalue_array(2)
    eigmat(1,2) = (0.0d0, 0.0d0)
    eigmat(2,1) = (0.0d0, 0.0d0)
    cmat(:,:) = CGM_get_coefficients(cgm)

    !-----check the eigen value probrem---
    left(:,:) = matmul(hmat, cmat)
    right(:,:) = matmul(matmul(smat, cmat), eigmat)
    write (*,*) "HC"
    do i = 1, 2
       write (*,*) left(i,:)
    end do
    write (*,*) "SCL"
    do i = 1, 2
       write (*,*) right(i,:)
    end do
    
  end subroutine CGM_testcase_heh
  subroutine CGM_testcase_unit_s()
    type(CGM_Object)  :: cgm
    complex*16        :: hmat(2,2), smat(2,2)
    complex*16        :: left(2,2), right(2,2)
    complex*16        :: eigmat(2,2), cmat(2,2)
    integer i
    smat(1,1) = (1.0q0, 0.0q0)
    smat(1,2) = (0.0q0, 0.0q0)
    smat(2,1) = (0.0q0, 0.0q0)
    smat(2,2) = (1.0q0, 0.0q0)
    hmat(1,1) = (0.0q0, 0.0q0)
    hmat(1,2) = (3.0q0, 0.0q0)
    hmat(2,1) = (3.0q0, 0.0q0)
    hmat(2,2) = (4.0q0, 0.0q0)
    cgm = CGM_new(hmat, smat)
    call CGM_solve_eigenvalue(cgm)
    call CGM_print(cgm)

    eigmat(1,1) = cgm % eigenvalue_array(1)
    eigmat(2,2) = cgm % eigenvalue_array(2)
    eigmat(1,2) = (0.0d0, 0.0d0)
    eigmat(2,1) = (0.0d0, 0.0d0)
    left(:,:) = matmul(hmat, cmat)
    right(:,:) = matmul(matmul(eigmat, smat), cmat)
    write (*,*) "HC"
    do i = 1, 2
       write (*,*) left(i,:)
    end do
    write (*,*) "LSC"
    do i = 1, 2
       write (*,*) right(i,:)
    end do
  end subroutine CGM_testcase_unit_s
  subroutine CGM_testcase_3dim()
    type(CGM_Object)  :: cgm
    complex*16        :: hmat(3,3), smat(3,3)
    complex*16        :: left(3,3), right(3,3)
    complex*16        :: eigmat(3,3), cmat(3,3)
    integer i
    smat(1,1) = (1.0q0, 0.0q0)
    smat(2,2) = (1.0q0, 0.0q0)
    smat(3,3) = (-2.0q0, 0.0q0)
    smat(1,2) = (0.0q0, 0.0q0)
    smat(2,1) = smat(1,2)
    smat(1,3) = (0.0q0, 2.0q0)
    smat(3,1) = smat(1,3)
    smat(2,3) = (2.0q0, 2.0q0)
    smat(3,2) = smat(2,3)

    hmat(1,1) = (1.0q0, 0.0q0)
    hmat(2,2) = (4.0q0, 0.0q0)
    hmat(3,3) = (-2.0q0, 2.0q0)
    hmat(1,2) = (0.2q0, 0.0q0)
    hmat(2,1) = hmat(1,2)
    hmat(1,3) = (0.0q0, 0.0q0)
    hmat(3,1) = hmat(1,3)
    hmat(2,3) = (2.5q0, -2.0q0)
    hmat(3,2) = hmat(2,3)
    cgm = CGM_new(hmat, smat)
    call CGM_solve_eigenvalue(cgm)
    call CGM_print(cgm)

    eigmat(:,:) = (0.0d0, 0.0d0)
    eigmat(1,1) = cgm % eigenvalue_array(1)
    eigmat(2,2) = cgm % eigenvalue_array(2)
    eigmat(3,3) = cgm % eigenvalue_array(3)
    cmat(:,:) = CGM_get_coefficients(cgm)

    left(:,:) = matmul(hmat, cmat)
    right(:,:) = matmul(matmul(smat, cmat), eigmat)
    write (*,*) "HC"
    do i = 1, 3
       write (*,*) left(i,:)
    end do
    write (*,*) "LSC"
    do i = 1, 3
       write (*,*) right(i,:)
    end do
  end subroutine CGM_testcase_3dim
  subroutine CGM_testcase_hc_ec()
    type(CGM_Object)  :: cgm
    complex*16        :: hmat(2,2)
    complex*16        :: left(2,2), right(2,2)
    complex*16        :: eigmat(2,2), cmat(2,2)
    integer i
    hmat(1,1) = (1.0d0, 0.0d0)
    hmat(2,2) = (2.0d0, 0.4d0)
    hmat(1,2) = (0.0d0, 0.2d0)
    hmat(2,1) = hmat(1,2)

    cgm = CGM_new(hmat)
    !    call CGM_solve_eigenvalue(cgm)
    call CGM_solve_eigenvalue_hc_ec(cgm)

!    call CGM_print(cgm)

    eigmat(:,:) = (0.0d0, 0.0d0)
    eigmat(1,1) = cgm % eigenvalue_array(1)

    eigmat(2,2) = cgm % eigenvalue_array(2)

    cmat(:,:) = CGM_get_coefficients(cgm)
    write (*,*) "eigs"
    do i = 1, 2
       write (*,*) cgm % eigenvalue_array(i)
    end do
    left(:,:) = matmul(hmat, cmat)
    right(:,:) = matmul(cmat, eigmat)
    write (*,*) "HC"
    do i = 1, 2
       write (*,*) left(i,:)
    end do
    write (*,*) "LSC"
    do i = 1, 2
       write (*,*) right(i,:)
    end do
  end subroutine CGM_testcase_hc_ec
    
end module Class_CGM_complex_givens_method

      SUBROUTINE GIVENS (NX,NROOTX,NJX,A,B,ROOT,VECT,NVEC)
      IMPLICIT COMPLEX*16 (A-H,O-Z)
      REAL *8 ETA,THETA,DELTA,SMALL,DELBIG,THETA1,TOLER, &
            RPOWER,RPOW1,RAND1,FACTOR,RTEMP,RNORM,RABS,DSQRT
!      QCPE PROGRAM 62.3
!      EIGENVALUES AND EIGENVECTORS BY THE GIVENS METHOD.
!      BY FRANKLIN PROSSER, INDIANA UNIVERSITY.
!      SEPTEMBER, 1967
!
!      CALCULATES EIGENVALUES AND EIGENVECTORS OF REAL SYMMETRIC MATRIX
!      STORED IN PACKED UPPER TRIANGULAR FORM.
!
!      THANKS ARE DUE TO F. E. HARRIS (STANFORD UNIVERSITY) AND H. H.
!      MICHELS (UNITED AIRCRAFT RESEARCH LABORATORIES) FOR EXCELLENT
!      WORK ON NUMERICAL DIFFICULTIES WITH EARLIER VERSIONS OF THIS
!      PROGRAM.
!
!      THE PARAMETERS FOR THE ROUTINE ARE...
!          NX     ORDER OF MATRIX
!          NROOTX NUMBER OF ROOTS WANTED.  THE NROOTX SMALLEST (MOST
!                  NEGATIVE) ROOTS WILL BE CALCULATED.  IF NO VECTORS
!                  ARE WANTED, MAKE THIS NUMBER NEGATIVE.
!          NJX    ROW DIMENSION OF VECT ARRAY.  SEE  VECT  BELOW.
!                  NJX MUST BE NOT LESS THAN NX.
!          A      MATRIX STORED BY COLUMNS IN PACKED UPPER TRIANGULAR
!                 FORM, I.E. OCCUPYING NX*(NX+1)/2 CONSECUTIVE
!                 LOCATIONS.
!          B      SCRATCH ARRAY USED BY GIVENS.  MUST BE AT LEAST
!                  NX*5 CELLS.
!          ROOT   ARRAY TO HOLD THE EIGENVALUES.  MUST BE AT LEAST
!                 NROOTX CELLS LONG.  THE NROOTX SMALLEST ROOTS ARE
!                  ORDERED LARGEST FIRST IN THIS ARRAY.
!          VECT   EIGENVECTOR ARRAY.  EACH COLUMN WILL HOLD AN
!                  EIGENVECTOR FOR THE CORRESPONDING ROOT.  MUST BE
!                  DIMENSIONED WITH  NJX  ROWS AND AT LEAST  NROOTX
!                  COLUMNS, UNLESS NO VECTORS
!                  ARE REQUESTED (NEGATIVE NROOTX).  IN THIS LATTER
!                  CASE, THE ARGUMENT VECT IS JUST A DUMMY, AND THE
!                  STORAGE IS NOT USED.
!                  THE EIGENVECTORS ARE NORMALIZED TO UNITY.
!
!      THE ARRAYS A AND B ARE DESTROYED BY THE COMPUTATION. THE RESULTS
!      APPEAR IN ROOT AND VECT.
!      FOR PROPER FUNCTIONING OF THIS ROUTINE, THE RESULT OF A FLOATING
!      POINT UNDERFLOW SHOULD BE A ZERO.
!
!      THE ORIGINAL REFERENCE TO THE GIVENS TECHNIQUE IS IN OAK RIDGE
!      REPORT NUMBER ORNL 1574 (PHYSICS), BY WALLACE GIVENS.
!      THE METHOD AS PRESENTED IN THIS PROGRAM CONSISTS OF FOUR STEPS,
!      ALL MODIFICATIONS OF THE ORIGINAL METHOD...
!      FIRST, THE INPUT MATRIX IS REDUCED TO TRIDIAGONAL FORM BY THE
!      HOUSEHOLDER TECHNIQUE (J. H. WILKINSON, COMP. J. 3, 23 (1960)).
!      THE ROOTS ARE THEN LOCATED BY THE STURM SEQUENCE METHOD (J. M.
!      ORTEGA (SEE REFERENCE BELOW).  THE VECTORS OF THE TRIDIAGONAL
!      FORM ARE THEN EVALUATED (J.H. WILKINSON, COMP. J. 1, 90 (1958)),
!      AND LAST THE TRIDIAGONAL VECTORS ARE ROTATED TO VECTORS OF THE
!      ORIGINAL ARRAY (FIRST REFERENCE).
!      VECTORS FOR DEGENERATE (OR NEAR-DEGENERATE) ROOTS ARE FORCED
!      TO BE ORTHOGONAL, USING A METHOD SUGGESTED BY B. GARBOW, ARGONNE
!      NATIONAL LABS (PRIVATE COMMUNICATION, 1964).  THE GRAM-SCHMIDT
!      PROCESS IS USED FOR THE ORTHOGONALIZATION.
!
!      AN EXCELLENT PRESENTATION OF THE GIVENS TECHNIQUE IS FOUND IN
!      J. M. ORTEGA S ARTICLE IN  MATHEMATICS FOR DIGITAL COMPUTERS,
!      VOLUME 2, ED. BY RALSTON AND WILF, WILEY (1967), PAGE 94.
!
       DIMENSION B(NX,5),A(*),ROOT(*),VECT(NJX,NVEC)
!
! ** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
! **   USERS PLEASE NOTE...
! **   THE FOLLOWING TWO PARAMETERS, ETA AND THETA, SHOULD BE ADJUSTED
! **   BY THE USER FOR HIS PARTICULAR MACHINE.
! **   ETA IS AN INDICATION OF THE PRECISION OF THE FLOATING POINT
! **   REPRESENTATION ON THE COMPUTER BEING USED (ROUGHLY 10**(-M),
! **   WHERE M IS THE NUMBER OF DECIMALS OF PRECISION ).
! **   THETA IS AN INDICATION OF THE RANGE OF NUMBERS THAT CAN BE
! **   EXPRESSED IN THE FLOATING POINT REPRESENTATION (ROUGHLY THE
! **   LARGEST NUMBER).
! **   SOME RECOMMENDED VALUES FOLLOW.
! **   FOR IBM 7094, UNIVAC 1108, ETC. (27-BIT BINARY FRACTION, 8-BIT
! **   BINARY EXPONENT), ETA=1.E-8, THETA=1.E37.
! **   FOR CONTROL DATA 3600 (36-BIT BINARY FRACTION, 11-BIT BINARY
! **   EXPONENT), ETA=1.E-11, THETA=1.E307.
! **   FOR CONTROL DATA 6600 (48-BIT BINARY FRACTION, 11-BIT BINARY
! **   EXPONENT), ETA=1.E-14, THETA=1.E307.
! **   FOR IBM 360/50 AND 360/65 DOUBLE PRECISION (56-BIT HEXADECIMAL
! **   FRACTION, 7-BIT HEXADECIMAL EXPONENT), ETA=1.E-16, THETA=1.E75.
! **   FOR TELEFUNKEN TR440, ETA=1.E-11, THETA=1.E152.
! **
!DEC20 DATA ETA,THETA /1.D-14,1.D+37/
       DATA ETA,THETA /1.D-16,1.D+60/
!
!  CDABS(Z) WAS REPLACED BY STATEMENT FUNCTION RABS TO SPEED UP  S.Y.
      RABS(Z)=DABS(DREAL(Z))+DABS(DIMAG(Z))
!
! ** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
!
!SY    DEL1 = ETA*1.D-02
       DELTA = ETA**2*1.D+02
       SMALL = ETA**2*1.D-02
       DELBIG = THETA*DELTA*1.D-03
       THETA1 = 1.0D3/THETA
!      TOLER  IS A FACTOR USED TO DETERMINE IF TWO ROOTS ARE CLOSE
!      ENOUGH TO BE CONSIDERED DEGENERATE FOR PURPOSES OF ORTHOGONALI-
!      ZING THEIR VECTORS.  FOR THE MATRIX NORMED TO UNITY, IF THE
!      DIFFERENCE BETWEEN TWO ROOTS IS LESS THAN TOLER, THEN
!      ORTHOGONALIZATION WILL OCCUR.
       TOLER = ETA*1.D2
!
!      INITIAL VALUE FOR PSEUDORANDOM NUMBER GENERATOR... (2**23)-3
       RPOWER = 8388608.D0
       RPOW1 = RPOWER*0.5D0
       RAND1 = RPOWER - 3.D0
!
      N = NX
      NROOT = IABS(NROOTX)
      IF (NROOT.EQ.0) GO TO 1001
      IF (N-1) 1001,1003,105
!
!     SPECIAL SECTION FOR N=1.
1003  ROOT(1) = A(1)
      IF (NVEC.GT.0) VECT(1,1) = (1.0D0,0.0D0)
      GO TO 1001
!
  105 IF (N.GT.2) GOTO 106
!     SPECIAL SECTION FOR N=2
      IF(DREAL(A(2)).NE.0.D0.OR.DIMAG(A(2)).NE.0.D0) GO TO 107
      IF(DREAL(A(1)).GT.DREAL(A(3))) GO TO 112
      ROOT(1) = A(1)
      IF (NROOTX.LT.0.OR.NVEC.LT.1) GOTO 116
      VECT(1,1) = (1.D0,0.D0)
      VECT(2,1) = (0.D0,0.D0)
  116 IF (NROOT.LT.2) GOTO 114
      ROOT(2) = A(3)
      IF(NROOTX.LT.0.OR.NVEC.LT.2)GO TO 114
      VECT(2,2) = (1.D0,0.D0)
      VECT(1,2) = (0.D0,0.D0)
      GO TO 114
  112 ROOT(1) = A(3)
      IF (NROOTX.LT.0.OR.NVEC.LT.1) GOTO 118
      VECT(2,1) = (1.D0,0.D0)
      VECT(1,1) = (0.D0,0.D0)
  118 IF (NROOT.LT.2) GOTO 114
      ROOT(2) = A(1)
      IF(NROOTX.LT.0.OR.NVEC.LT.2)GO TO 114
      VECT(1,2) = (1.D0,0.D0)
      VECT(2,2) = (0.D0,0.D0)
  114 GOTO 1001
  107 ZFACTR = 0.5D0*CDSQRT((A(1)-A(3))**2+4.D0*A(2)**2)
      TEMP  = 0.5D0* (A(1)+A(3))
      ROOT(1) = TEMP - ZFACTR
      IF (NROOTX.LT.0) GOTO 109
      IF(NVEC.LT.1)GO TO 109
      TEMP1 = (1.D0,0.D0)/A(2)
      VECT(2,1) = (ROOT(1)-A(1))*TEMP1
      ANORM = (1.D0,0.D0)/CDSQRT((1.D0,0.D0)+VECT(2,1)**2)
      VECT(2,1) = ANORM*VECT(2,1)
      VECT(1,1) = ANORM
  109 IF (NROOT.LT.2) GOTO 1001
      ROOT(2) = TEMP + ZFACTR
      IF (NROOTX.LT.0) GOTO 1001
      IF(NVEC.LT.2)GO TO 1001
      VECT(1,2) = (ROOT(2)-A(3))*TEMP1
      ANORM = (1.D0,0.D0)/CDSQRT((1.D0,0.D0)+VECT(1,2)**2)
      VECT(1,2) = ANORM*VECT(1,2)
      VECT(2,2) = ANORM
      GOTO 1001
!
!     SECTION FOR  N.GT.2 .
!     NSIZE    NUMBER OF ELEMENTS IN THE PACKED ARRAY
  106 NSIZE = (N*(N+1))/2
      NM1 = N-1
      NM2 = N-2
      DO 20 J=1,5
      DO 20 I=1,NX
   20 B(I,J)=(0.D0,0.D0)
!
!     SCALE MATRIX TO EUCLIDEAN NORM OF 1.  SCALE FACTOR IS ANORM.
      FACTOR = 0.D0
      DO 70 I=1,NSIZE
      RTEMP = RABS(A(I))
70    FACTOR = DMAX1(FACTOR,RTEMP)
      IF (FACTOR.NE.0.D0) GO TO 72
!     NULL MATRIX.  FIX UP ROOTS AND VECTORS, THEN DEXIT.
      DO 78 I=1,NROOT
      IF (NROOTX.LT.0) GO TO 78
      IF(NVEC.LE.0)GO TO 78
      DO 77 J=1,N
77    VECT(J,I) = (0.0D0,0.0D0)
      VECT(I,I) = (1.0D0,0.0D0)
78    ROOT(I) = (0.D0,0.D0)
      GO TO 1001
!
72    RNORM = 0.D0
      J = 1
      K = 1
86    RTEMP = 1.D0/FACTOR
      DO 80 I=1,NSIZE
      IF (I.NE.J) GO TO 81
      RNORM = RNORM + (RABS(A(I))*RTEMP)**2*0.5D0
      K = K+1
      J = J+K
      GO TO 80
81    RNORM = RNORM + (RABS(A(I))*RTEMP)**2
80    CONTINUE
83    RNORM = DSQRT(RNORM*2.D0)*FACTOR
      TEMP1 = 1.D0/RNORM
      DO 91 I=1,NSIZE
91    A(I) = A(I)*TEMP1
!SY    ALIMIT = (1.0D0,0.0D0)
!
!      TRIDIA SECTION.
!      TRIDIAGONALIZATION OF SYMMETRIC MATRIX
       ID = 0
       IA = 1
!      IF (NM2.EQ.0) GO TO 201
       DO 200  J=1,NM2
!      J       COUNTS ROW  OF A-MATRIX TO BE DIAGONALIZED
!      IA      START OF NON-CODIAGONAL ELEMENTS IN THE ROW
!      ID      INDEX OF CODIAGONAL ELEMENT ON ROW BEING CODIAGONALIZED.
       IA = IA+J+2
       ID = ID + J + 1
       JP2 = J+2
!      SUM SQUARES OF NON-CODIAGONAL ELEMENTS IN ROW J
       II = IA
       SUM = (0.0D0,0.0D0)
       DO 100 I=JP2,N
       SUM=SUM+A(II)*A(II)
100    II = II + I
       TEMP = A(ID)
       IF (RABS(SUM).GT.SMALL) GO TO 110
!      NO TRANSFORMATION NECESSARY IF ALL THE NON-CODIAGONAL
!      ELEMENTS ARE TINY.
       WRITE(6,111) J,SUM
  111  FORMAT(1H ,'** THIS CALCULATION MAY BE COMPLETELY NONSENCE **' &
            /         ' BE CAREFUL   ', I4,2D12.6)
120    B(J,1) = TEMP
       A(ID) = (0.D0,0.D0)
       GO TO 200
!      NOW COMPLETE THE SUM OF OFF-DIAGONAL SQUARES
110    SUM = CDSQRT(SUM + TEMP**2)
      IF(DREAL(SUM*DCONJG(TEMP)).LT.0.D0) SUM=-SUM
!      NEW CODIAGONAL ELEMENT
!      B(J,1) = -DSIGN(SUM,TEMP)
       B(J,1) = -SUM
!      FIRST NON-ZERO ELEMENT OF THIS W-VECTOR
       B(J+1,2) = CDSQRT(((1.D0,0.D0) + TEMP/SUM)*0.5D0)
!      FORM REST OF THE W-VECTOR ELEMENTS
       TEMP = 0.5D0/(B(J+1,2)*SUM)
       II = IA
       DO 130 I=JP2,N
       B(I,2) = A(II)*TEMP
130    II = II + I
!      FORM P-VECTOR AND SCALAR.  P-VECTOR = A-MATRIX*W-VECTOR.
!     SCALAR = W-VECTOR*P-VECTOR
       AK = (0.0D0,0.0D0)
!      IC      LOCATION OF NEXT DIAGONAL ELEMENT
       IC = ID + 1
       J1 = J + 1
       DO 190  I=J1,N
       JJ = IC
       TEMP = (0.D0,0.D0)
       DO 180  II=J1,N
!      I       RUNS OVER THE NON-ZERO P-ELEMENTS
!      II      RUNS OVER ELEMENTS OF W-VECTOR
       TEMP = TEMP + B(II,2)*A(JJ)
!      CHANGE INCREMENTING MODE AT THE DIAGONAL ELEMENTS.
       IF (II.LT.I) GO TO 210
140    JJ = JJ + II
       GO TO 180
210    JJ = JJ + 1
180    CONTINUE
!      BUILD UP THE K-SCALAR (AK)
       AK = AK + TEMP*B(I,2)
       B(I,1) = TEMP
!      MOVE IC TO TOP OF NEXT A-MATRIX  ROW
190    IC = IC + I
!      FORM THE Q-VECTOR
       DO 150  I=J1,N
150    B(I,1) = B(I,1) - AK*B(I,2)
!      TRANSFORM THE REST OF THE A-MATRIX
!      JJ      START-1 OF THE REST OF THE A-MATRIX
       JJ = ID
!      MOVE W-VECTOR INTO THE OLD A-MATRIX LOCATIONS TO SAVE SPACE
!      I       RUNS OVER THE SIGNIFICANT ELEMENTS OF THE W-VECTOR
       DO 160  I=J1,N
       A(JJ) = B(I,2)
       DO 170  II=J1,I
       JJ = JJ + 1
       QWWQ = B(I,1)*B(II,2) + B(I,2)*B(II,1)
170    A(JJ) = A(JJ) - (QWWQ+QWWQ)
160    JJ = JJ + J
200    CONTINUE
!      MOVE LAST CODIAGONAL ELEMENT OUT INTO ITS PROPER PLACE
201    CONTINUE
       B(NM1,1) = A(NSIZE-1)
       A(NSIZE-1) = (0.D0,0.D0)
!
!     STURM SECTION.
!     STURM SEQUENCE ITERATION TO OBTAIN ROOTS OF TRIDIAGONAL FORM.
!     MOVE DIAGONAL ELEMENTS INTO SECOND N ELEMENTS OF B-VECTOR.
!     THIS IS A MORE CONVENIENT INDEXING POSITION.
!     ALSO, PUT SQUARE OF CODIAGONAL ELEMENTS IN THIRD N ELEMENTS.
      JUMP=1
      DO 320 J=1,N
      B(J,2)=A(JUMP)
      IF(J.EQ.N) GO TO 320
      B(J+1,3)=B(J,1)
320   JUMP = JUMP+J+1
      CALL ZQL1(N,B(1,2),B(1,3),IERR)
      IF(IERR.NE.0) WRITE(6,999) IERR
  999 FORMAT('0** IERR = ',I4)
      DO 998 I=1,NROOT
  998 ROOT(I)=B(I,2)
!
!     TRIVEC SECTION.
!     EIGENVECTORS OF CODIAGONAL FORM
807   CONTINUE
!     QUIT NOW IF NO VECTORS WERE REQUESTED.
      IF (NROOTX.LT.0) GO TO 1002
      IF(NVEC.LT.1)GO TO 1002
!     INITIALIZE VECTOR ARRAY.
      DO 15 I=1,N
      DO 15 J=1,NVEC
15    VECT(I,J) = (1.0D0,0.0D0)
      DO 700 I=1,NVEC
      AROOT = ROOT(I)
!     ORTHOGONALIZE IF ROOTS ARE CLOSE.
      IF (I.EQ.1) GO TO 710
!     THE ABSOLUTE VALUE IN THE NEXT TEST IS TO ASSURE THAT THE TRIVEC
!     SECTION IS INDEPENDENT OF THE ORDER OF THE EIGENVALUES.
715   IF (RABS(ROOT(I-1)-AROOT).LT.TOLER) GO TO 720
710   IA = -1
720   IA = IA + 1
      ELIM1 = A(1) - AROOT
      ELIM2 = B(1,1)
      JUMP = 1
      DO 750  J=1,NM1
      JUMP = JUMP+J+1
!     GET THE CORRECT PIVOT EQUATION FOR THIS STEP.
      IF (RABS(ELIM1).LE.RABS(B(J,1))) GOTO 760
!     FIRST (ELIM1) EQUATION IS THE PIVOT THIS TIME.  CASE 1.
      B(J,2) = ELIM1
      B(J,3) = ELIM2
      B(J,4) = (0.D0,0.D0)
      TEMP = B(J,1)/ELIM1
      ELIM1 = A(JUMP) - AROOT - TEMP*ELIM2
      ELIM2 = B(J+1,1)
      GO TO 755
!     SECOND EQUATION IS THE PIVOT THIS TIME.  CASE 2.
760   B(J,2) = B(J,1)
      B(J,3) = A(JUMP) - AROOT
      B(J,4) = B(J+1,1)
      TEMP = (1.0D0,0.0D0)
      IF (RABS(B(J,1)).GT.THETA1) TEMP = ELIM1/B(J,1)
      ELIM1 = ELIM2 - TEMP*B(J,3)
      ELIM2 = -TEMP*B(J+1,1)
!     SAVE FACTOR FOR THE SECOND ITERATION.
755   B(J,5) = TEMP
750   CONTINUE
      B(N,2) = ELIM1
      B(N,3) = (0.D0,0.D0)
      B(N,4) = (0.D0,0.D0)
      B(NM1,4) = (0.D0,0.D0)
      ITER = 1
      IF (IA.NE.0) GO TO 801
!     BACK SUBSTITUTE TO GET THIS VECTOR.
790   L = N + 1
      DO 780 J=1,N
      L = L - 1
786   CONTINUE
      LP1 = L+1
      LP2 = L+2
      ELIM1=VECT(L,I)
      IF (LP1.LE.N) ELIM1=ELIM1-VECT(LP1,I)*B(L,3)
      IF (LP2.LE.N) ELIM1=ELIM1-VECT(LP2,I)*B(L,4)
!     IF OVERFLOW IS CONCEIVABLE, SCALE THE VECTOR DOWN.
!     THIS APPROACH IS USED TO AVOID MACHINE-DEPENDENT AND SYSTEM-
!     DEPENDENT CALLS TO OVERFLOW ROUTINES.
      IF (RABS(ELIM1).GT.DELBIG) GO TO 782
      TEMP = B(L,2)
      IF (RABS(B(L,2)).LT.DELTA) TEMP = DELTA
      VECT(L,I) = ELIM1/TEMP
      GO TO 780
!     VECTOR IS TOO BIG.  SCALE IT DOWN.
782   TEMP1 = (1.D0,0.D0)/DELBIG
      DO 784 K=1,N
784   VECT(K,I) = VECT(K,I)*TEMP1
      GO TO 786
780   CONTINUE
      GO TO (820,800), ITER
!     SECOND ITERATION.  (BOTH ITERATIONS FOR REPEATED-ROOT VECTORS).
820   ITER = ITER + 1
890   ELIM1 = VECT(1,I)
      DO 830 J=1,NM1
      IF (DREAL(B(J,2)).EQ.DREAL(B(J,1)).AND. &
           DIMAG(B(J,2)).EQ.DIMAG(B(J,1))) GO TO 840
!     CASE ONE.
850   VECT(J,I) = ELIM1
      ELIM1 = VECT(J+1,I) - ELIM1*B(J,5)
      GO TO 830
!     CASE TWO.
840   VECT(J,I) = VECT(J+1,I)
      ELIM1 = ELIM1 - VECT(J+1,I)*TEMP
830   CONTINUE
      VECT(N,I) = ELIM1
      GO TO 790
!     PRODUCE A RANDOM VECTOR
801   CONTINUE
      TEMP1 = (1.D0,0.D0)/RPOW1
      DO 802 J=1,N
!     GENERATE PSEUDORANDOM NUMBERS WITH UNIFORM DISTRIBUTION IN (-1,1).
!     THIS RANDOM NUMBER SCHEME IS OF THE FORM...
!     RAND1 = DMOD((2**12+3)*RAND1,2**23)
!     IT HAS A PERIOD OF 2**21 NUMBERS.
      RAND1 = DMOD(4099.D0*RAND1,RPOWER)
802   VECT(J,I) = RAND1*TEMP1 - (1.D0,0.D0)
      GO TO 790
!
!     ORTHOGONALIZE THIS REPEATED-ROOT VECTOR TO OTHERS WITH THIS ROOT.
800   IF (IA.EQ.0) GO TO 885
      DO 860 J1=1,IA
      K = I - J1
      TEMP = (0.D0,0.D0)
      DO 870 J=1,N
870   TEMP = TEMP + VECT(J,I)*VECT(J,K)
      DO 880 J=1,N
880   VECT(J,I) = VECT(J,I) - TEMP*VECT(J,K)
860   CONTINUE
885   GO TO (890,900), ITER
!     NORMALIZE THE VECTOR
900   TEMP = (0.D0,0.D0)
      DO 910 J=1,N
      TEMP=TEMP+VECT(J,I)**2
  910 CONTINUE
      TEMP=(1.0D0,0.0D0)/CDSQRT(TEMP)
      DO 920 J=1,N
      VECT(J,I) = VECT(J,I)*TEMP
920   CONTINUE
700   CONTINUE
!
!      SIMVEC SECTION.
!      ROTATE CODIAGONAL VECTORS INTO VECTORS OF ORIGINAL ARRAY
!      LOOP OVER ALL THE TRANSFORMATION VECTORS
       IF (NM2.EQ.0) GO TO 1002
       JUMP = NSIZE - (N+1)
       IM = NM1
       DO 950  I=1,NM2
       J1 = JUMP
!      MOVE A TRANSFORMATION VECTOR OUT INTO BETTER INDEXING POSITION.
       DO 955  J=IM,N
       B(J,2) = A(J1)
955    J1 = J1 + J
!      MODIFY ALL REQUESTED VECTORS.
       DO 960  K=1,NVEC
       TEMP = (0.D0,0.D0)
!      FORM SCALAR PRODUCT OF TRANSFORMATION VECTOR WITH EIGENVECTOR
       DO 970  J=IM,N
970    TEMP = TEMP + B(J,2)*VECT(J,K)
       TEMP = TEMP + TEMP
       DO 980  J=IM,N
980    VECT(J,K) = VECT(J,K) - TEMP*B(J,2)
960    CONTINUE
       JUMP = JUMP - IM
950    IM = IM - 1
1002   CONTINUE
!      RESTORE ROOTS TO THEIR PROPER SIZE.
       DO 95 I=1,NROOT
95     ROOT(I) = ROOT(I)*RNORM
1001   RETURN
       END

            SUBROUTINE ZQL1(N,D,E,IERR)
!
      INTEGER          I,           IERR,        II,          J
      INTEGER          K,           L,           L1,          M
      INTEGER          MML,         N
      COMPLEX *16      Z,           C,           D(N)
      COMPLEX *16      CDSQRT,                                E(N)
      COMPLEX *16      F,           G,           H
      COMPLEX *16      P,           R,           S
      DOUBLE PRECISION B,           MACHEP,      RABS
      DOUBLE PRECISION DREAL,       DIMAG
!
!     THIS SUBROUTINE IS A TRANSLATION OF THE ALGOL PROCEDURE TQL1,
!     NUM. MATH. 11, 293-306(1968) BY BOWDLER, MARTIN, REINSCH, AND
!     WILKINSON.
!     HANDBOOK FOR AUTO. COMP., VOL.II-LINEAR ALGEBRA, 227-240(1971).
!
!     THIS SUBROUTINE FINDS THE EIGENVALUES AND EIGENVECTORS
!     OF A SYMMETRIC TRIDIAGONAL MATRIX BY THE QL METHOD.
!     THE EIGENVECTORS OF A FULL SYMMETRIC MATRIX CAN ALSO
!     BE FOUND IF  TRED2  HAS BEEN USED TO REDUCE THIS
!     FULL MATRIX TO TRIDIAGONAL FORM.
!
!     ON INPUT
!
!        N IS THE ORDER OF THE MATRIX;
!
!        D CONTAINS THE DIAGONAL ELEMENTS OF THE INPUT MATRIX;
!
!        E CONTAINS THE SUBDIAGONAL ELEMENTS OF THE INPUT MATRIX
!          IN ITS LAST N-1 POSITIONS.  E(1) IS ARBITRARY;
!
!      ON OUTPUT
!
!        D CONTAINS THE EIGENVALUES IN ASCENDING ORDER.  IF AN
!          ERROR EXIT IS MADE, THE EIGENVALUES ARE CORRECT BUT
!          UNORDERED FOR INDICES 1,2,...,IERR-1;
!
!        E HAS BEEN DESTROYED;
!
!        IERR IS SET TO
!          ZERO       FOR NORMAL RETURN,
!          J          IF THE J-TH EIGENVALUE HAS NOT BEEN
!                     DETERMINED AFTER 30 ITERATIONS.
!
!     QUESTIONS AND COMMENTS SHOULD BE DIRECTED TO B. S. GARBOW,
!     APPLIED MATHEMATICS DIVISION, ARGONNE NATIONAL LABORATORY
!
!     ------------------------------------------------------------------
!
!                MACHEP IS A MACHINE DEPENDENT PARAMETER SPECIFYING
!                THE RELATIVE PRECISION OF FLOATING POINT ARITHMETIC.
!                MACHEP = 16.0D0**(-13) FOR LONG FORM ARITHMETIC
!                ON S360
!      DATA MACHEP/Z3410000000000000/
!
!
!
!     SUPPLY THE MACHINE EPSILON FROM THE PORT LIBRARY FRAMEWORK.
!
!     MACHEP = 1.0D-14
!
      RABS(Z)=DABS(DREAL(Z))+DABS(DIMAG(Z))
!
!     SUPPLY THE MACHINE EPSILON FROM THE PORT LIBRARY FRAMEWORK.
!
      MACHEP = 1.0D-14
!
      IERR = 0
      IF (N .EQ. 1) GO TO 1001
!
      DO 100 I = 2, N
  100 E(I-1) = E(I)
!
      F = (0.0D0,0.0D0)
      B = 0.0D0
      E(N) = (0.0D0,0.0D0)
!
      DO 240 L = 1, N
         J = 0
         H = MACHEP * (RABS(D(L)) + RABS(E(L)))
         IF (B .LT. DREAL(H)) B = DREAL(H)
!                LOOK FOR SMALL SUB-DIAGONAL ELEMENT
         DO 110 M = L, N
            IF (RABS(E(M)) .LE. B) GO TO 120
!                E(N) IS ALWAYS ZERO, SO THERE IS NO EXIT
!                THROUGH THE BOTTOM OF THE LOOP
  110    CONTINUE
!
  120    IF (M .EQ. L) GO TO 220
  130    IF (J .EQ. 30) GO TO 1000
         J = J + 1
!                FORM SHIFT
         L1 = L + 1
         G = D(L)
         P = (D(L1) - G) / (2.0D0 * E(L))
         R = CDSQRT(P*P+1.0D0)
         IF(DREAL(R*DCONJG(P)).LT.0.0D0) R=-R
         D(L) = E(L) / (P + R)
         H = G - D(L)
!
         DO 140 I = L1, N
  140    D(I) = D(I) - H
!
         F = F + H
!                QL TRANSFORMATION
         P = D(M)
         C = 1.0D0
         S = (0.0D0,0.0D0)
         MML = M - L
!                FOR I=M-1 STEP -1 UNTIL L DO --
         DO 200 II = 1, MML
            I = M - II
            G = C * E(I)
            H = C * P
            IF (RABS(P) .LT. RABS(E(I))) GO TO 150
            C = E(I) / P
            R = CDSQRT(C*C+1.0D0)
            E(I+1) = S * P * R
            S = C / R
            C = 1.0D0 / R
            GO TO 160
  150       C = P / E(I)
            R = CDSQRT(C*C+1.0D0)
            E(I+1) = S * E(I) * R
            S = 1.0D0 / R
            C = C * S
  160       P = C * D(I) - S * G
            D(I+1) = H + S * (C * G + S * D(I))
!
  200    CONTINUE
!
         E(L) = S * P
         D(L) = C * P
         IF (RABS(E(L)) .GT. B) GO TO 130
  220    D(L) = D(L) + F
  240 CONTINUE
!                ORDER EIGENVALUES AND EIGENVECTORS
      DO 300 II = 2, N
         I = II - 1
         K = I
         P = D(I)
!
         DO 260 J = II, N
            IF (DREAL(D(J)) .GE. DREAL(P)) GO TO 260
            K = J
            P = D(J)
  260    CONTINUE
!
         IF (K .EQ. I) GO TO 300
         D(K) = D(I)
         D(I) = P
!
!
  300 CONTINUE
!
      GO TO 1001
!                SET ERROR -- NO CONVERGENCE TO AN
!                EIGENVALUE AFTER 30 ITERATIONS
 1000 IERR = L
      WRITE(6,999) IERR
  999 FORMAT(' IERR= ',I4)
 1001 RETURN
!                LAST CARD OF TQL1
      END

            SUBROUTINE ORDABS(E,U,N,NN,KEY)
      IMPLICIT COMPLEX*16 (A-H,O-Z)
      REAL *8 EMOST,EJ
      LOGICAL ASCEND
      DIMENSION E(NN),U(NN,NN)
!
!  KEY.GE.0 -->  REAL(EIG(1)) < REAL(EIG(2)) < REAL(EIG(3)) ...
!  KEY.LT.0 -->  CABS(EIG(1)) > CABS(EIG(2)) > CABS(EIG(3)) ...
!
      IF(N.LE.1) RETURN
      ASCEND=KEY.GE.0
      NM1=N-1
      DO 10 I=1,NM1
      MOST=I
      EMOST=CDABS(E(MOST))
      IF(ASCEND) EMOST=DREAL(E(MOST))
      IP1=I+1
      DO 20 J=IP1,N
      EJ=CDABS(E(J))
      IF(ASCEND) EJ=DREAL(E(J))
      IF(ASCEND) GO TO 30
!  DESCENDING ORDER (KEY.LT.0)
      IF(EMOST.GE.EJ) GO TO 20
      GO TO 40
!  ASCENDING ORDER (KEY.GE.0)
   30 IF(EMOST.LE.EJ) GO TO 20
   40 MOST=J
      EMOST=EJ
   20 CONTINUE
      IF(MOST.EQ.I) GO TO 10
      EM=E(MOST)
      E(MOST)=E(I)
      E(I)=EM
      DO 50 K=1,N
      UM=U(K,MOST)
      U(K,MOST)=U(K,I)
   50 U(K,I)=UM
   10 CONTINUE
      RETURN
      END
  
     
